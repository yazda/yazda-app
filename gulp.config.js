module.exports = function() {
    var name = 'Yazda';
    var client = './www/';
    //var server = './src/server/';
    var clientApp = client + 'app/';
    var report = './report/';
    var root = './';
    var deploy = './deploy/';
    var specRunnerFile = 'specs.html';
    var temp = './.tmp/';
    var styles = './www/css/';
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({devDependencies: true})['js'];
    var bower = {
        json: require('./bower.json'),
        directory: './www/lib/',
        ignorePath: '../..'
    };
    var nodeModules = 'node_modules';

    var config = {
        /**
         * File paths
         */
        // all javascript that we want to vet
        alljs: [
            './www/**/*.js',
            './*.js'
        ],
        build: './build/',
        client: client,
        css: styles + '*.css',
        deploy: deploy,
        deployConf: {
            default: {
                privateKey: '',
                host: '',
                username: '',
                dir: '',
                port: 0
            },
            test: {
                privateKey: '',
                host: '',
                username: '',
                dir: '',
                port: 0
            }
        },
        deployProject: deploy + name + '/',
        fonts: [
            client + 'fonts/**/*.*'
        ],
        html: client + '**/*.html',
        htmltemplates: client + 'templates/**/*.html',
        images: client + 'img/**/*.*',
        index: client + 'index.html',
        // app js, with no specs
        js: [
            clientApp + '**/*.module.js',
            clientApp + '**/*.js',
            '!' + clientApp + '**/*.spec.js',
            '!' + client + 'test-helpers/' + '**/*.js'
        ],
        jsOrder: [
            '**/app.module.js',
            '**/*.module.js',
            '**/*.js'
        ],
        less: client + 'styles/styles.less',
        name: name,
        report: report,
        root: root,
        //server: server,
        source: 'src/',
        stubsjs: [
            bower.directory + 'angular-mocks/angular-mocks.js',
            client + 'stubs/**/*.js'
        ],
        temp: temp,

        /**
         * optimized files
         */
        optimized: {
            app: 'app.js',
            lib: 'lib.js'
        },

        /**
         * plato
         */
        plato: {js: clientApp + '**/*.js'},

        /**
         * browser sync
         */
        browserReloadDelay: 1000,

        /**
         * template cache
         */
        templateCache: {
            file: 'templates.js',
            options: {
                module: 'templates',
                root: 'templates/',
                standAlone: false
            }
        },

        /**
         * Bower and NPM files
         */
        bower: bower,
        packages: [
            './package.json',
            './bower.json'
        ],

        /**
         * specs.html, our HTML spec runner
         */
        specRunner: client + specRunnerFile,
        specRunnerFile: specRunnerFile,

        /**
         * The sequence of the injections into specs.html:
         *  1 testlibraries
         *      mocha setup
         *  2 bower
         *  3 js
         *  4 spechelpers
         *  5 specs
         *  6 templates
         */
        testlibraries: [
            nodeModules + '/mocha/mocha.js',
            nodeModules + '/chai/chai.js',
            nodeModules + '/mocha-clean/index.js',
            nodeModules + '/sinon-chai/lib/sinon-chai.js'
        ],
        specHelpers: ['./www/test-helpers/*.js'],
        specs: ['./www/**/*.spec.js'],
        serverIntegrationSpecs: ['./testing/tests/server-integration/**/*.spec.js'],

        /**
         * Node settings
         */
        //nodeServer: './src/server/app.js',
        defaultPort: '8001'
    };

    /**
     * wiredep and bower settings
     */
    config.getWiredepDefaultOptions = function() {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };

    /**
     * karma settings
     */
    config.karma = getKarmaOptions();

    return config;

    ////////////////

    function getKarmaOptions() {
        var options = {
            files: [].concat(
                './node_modules/es6-promise/dist/es6-promise.js',
                './www/lib/ionic/js/ionic.bundle.js',
                bowerFiles,
                './www/lib/ngCordovaMocks/dist/ngCordovaMocks.js',
                config.specHelpers,
                clientApp + '**/*.module.js',
                clientApp + config.templateCache.file,
                clientApp + '**/*.js',
                config.serverIntegrationSpecs
            ),
            exclude: [],
            coverage: {
                dir: report + 'coverage',
                reporters: [
                    // reporters not supporting the `file` property
                    {type: 'html', subdir: 'report-html'},
                    {type: 'lcov', subdir: 'report-lcov'},
                    {type: 'text-summary'} //, subdir: '.', file: 'text-summary.txt'}
                ]
            },
            preprocessors: {}
        };
        // test coverage
        options.preprocessors[clientApp + 'components/**/!(*.spec|*.module)+(.js)'] = ['coverage'];
        options.preprocessors[clientApp + 'controllers/**/!(*.spec|*.module)+(.js)'] = ['coverage'];
        options.preprocessors[clientApp + 'features/**/!(*.spec|*.module)+(.js)'] = ['coverage'];
        options.preprocessors[clientApp + 'filters/**/!(*.spec|*.module)+(.js)'] = ['coverage'];
        options.preprocessors[clientApp + 'yazda-api/**/!(*.spec|*.module)+(.js)'] = ['coverage'];
        options.preprocessors[clientApp + 'core/core.route.js'] = ['coverage'];
        return options;
    }
};
