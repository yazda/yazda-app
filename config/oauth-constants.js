(function() {
    angular
        .module('yazda.yazda-api')
        .constant('oauthConstants', {
            'clientId':           '@@clientId',
            'clientSecret':       '@@clientSecret',
            'accessTokenUri':     '@@accessTokenUri',
            'authorizationUri':   '@@authorizationUri',
            'version':            '@@version',
            'baseURL':            '@@baseURL',
            'facebookAppId':      '@@facebookAppId',
            'stravaClientId':     '@@stravaClientId',
            'stravaClientSecret': '@@stravaClientSecret',
            'branchKey':          '@@branchKey',
            'googleAnalyticsID':  '@@googleAnalyticsID',
            "websocketURL":       "@@websocketURL"
        });
})();
