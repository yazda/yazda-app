var args = require('yargs').argv;
var config = require('./gulp.config.js')();
var gulp = require('gulp');
var bower = require('bower');
var sh = require('shelljs');
var args = require('yargs').argv;
var fs = require('fs');
var $ = require('gulp-load-plugins')({lazy: true});

var paths = {
    sass: ['./scss/**/*.scss', './www/fonts/**/*.scss'],
    js:   [
        './www/app/app.module.js',
        './www/app/**/*.module.js',
        './www/app/core/**/*.js',
        './www/app/blocks/**/*.js',
        './www/app/controllers/*.js',
        './www/app/components/**/*.directive.js',
        './www/app/features/**/*.route.js',
        './www/app/features/**/*.controller.js',
        './www/app/yazda-api/*.js',
        './www/app/open-url/*.js',
        './www/app/patch.js',
        './www/app/filters/**/*.js',
        '!./www/app/**/*.spec.js',
        '!./www/app/test-helpers/**/*.js']
};

gulp.task('jshint', function() {
    return gulp
        .src(paths.js)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
});

gulp.task('replace', function() {
    // Get the environment from the command line
    var env = args.env || 'localdev';

    // Read the settings from the right file
    var filename = env + '.json';
    var path = './config/' + filename;
    var settings = JSON.parse(fs.readFileSync(path, 'utf8'));

// Replace each placeholder with the correct value for the variable.
    gulp.src('config/oauth-constants.js')
        .pipe($.replaceTask({
            patterns: [
                {
                    match:       'clientId',
                    replacement: settings.clientId
                },
                {
                    match:       'clientSecret',
                    replacement: settings.clientSecret
                },
                {
                    match:       'accessTokenUri',
                    replacement: settings.accessTokenUri
                },
                {
                    match:       'authorizationUri',
                    replacement: settings.authorizationUri
                },
                {
                    match:       'version',
                    replacement: settings.version
                },
                {
                    match:       'baseURL',
                    replacement: settings.baseURL
                },
                {
                    match:       'facebookAppId',
                    replacement: settings.facebookAppId
                },
                {
                    match:       'stravaClientId',
                    replacement: settings.stravaClientId
                },
                {
                    match:       'stravaClientSecret',
                    replacement: settings.stravaClientSecret
                },
                {
                    match:       'branchKey',
                    replacement: settings.branchKey
                },
                {
                    match:       'googleAnalyticsID',
                    replacement: settings.googleAnalyticsID
                },
                {
                    match:       'websocketURL',
                    replacement: settings.websocketURL
                }
            ]
        }))
        .pipe(gulp.dest('www/app/yazda-api'));
});

gulp.task('default', ['sass', 'js']);

gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
        .pipe($.sass()
            .on('error', function (err) {
                console.log(err);
                this.emit('end');
            }))
        .pipe(gulp.dest('./www/css/'))
        .pipe($.concat('yazda-app.css'))
        .pipe(gulp.dest('./www/dist/'))
        .pipe($.minifyCss({
            keepSpecialComments: 0
        }))
        .pipe($.rename({extname: '.min.css'}))
        .pipe(gulp.dest('./www/dist/'));
});

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.js, ['js', 'jshint', 'default']);
});

gulp.task('install', ['git-check'], function() {
    return bower.commands.install()
        .on('log', function(data) {
            $.util.log('bower', $.util.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function(done) {
    if(!sh.which('git')) {
        console.log(
            '  ' + $.util.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', $.util.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + $.util.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});

gulp.task('js', ['templatecache'], function() {
    return gulp.src(paths.js)
        .pipe($.concat('yazda-app.js'))
        .pipe(gulp.dest('www/dist/'))
        .pipe($.rename('yazda-app.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('www/dist'));
});

gulp.task('templatecache', function() {
    console.log('Creating an AngularJS $templateCache');

    return gulp
        .src(config.htmltemplates)
        .pipe($.angularTemplatecache('templates.js', {
            module:     'templates',
            standalone: true,
            root:       ''
        }))
        .pipe(gulp.dest('./www/app/'))

    //.pipe($.if(args.verbose, $.bytediff.start()))
    //.pipe($.minifyHtml({empty: true}))
    //.pipe($.if(args.verbose, $.bytediff.stop(bytediffFormatter)))
    //.pipe($.angularTemplatecache())
    //.pipe(gulp.dest(config.temp));
});

gulp.task('test', ['templatecache'], function(done) {
    startTests(true, done);
});

///-----------------------------------------///

function startTests(singleRun, done) {
    console.log($.util.colors.cyan('///KARMA_START///'));
    var excludeFiles = config.karma.exclude;
    var karma = require('karma').server;
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        exclude:    excludeFiles,
        singleRun:  !!singleRun
    }, karmaComplete);

    function karmaComplete(karmaResult) {
        console.log($.util.colors.cyan('///KARMA_RESULT///\n', karmaResult));
    }
}

function bytediffFormatter(data) {
    var difference = (data.savings > 0) ? ' smaller.' : ' larger.';
    return data.fileName + ' went from ' +
        (data.startSize / 1000).toFixed(2) + ' kB to ' +
        (data.endSize / 1000).toFixed(2) + ' kB and is ' +
        formatPercent(1 - data.percent, 2) + '%' + difference;
}
