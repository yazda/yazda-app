(function() {
  angular.module('yazda', [
      'yazda.core',
      'yazda.yazda-api',
      'yazda.openUrl',
      'yazda.components',
      'yazda.controllers',
      'yazda.features',
      'ngCordovaOauth',
      'ngIOS9UIWebViewPatch'
    ]);
})();
