(function() {
    'use strict';

    angular
        .module('blocks.logger')
        .factory('logger', logger);

    //TODO: Toaster
    logger.$inject = ['$log' /*, 'toastr'*/];

    //TODO: Toaster
    /* @ngInject */
    function logger($log /*, toastr*/) {
        var service = {
            showToasts: true,
            error: error,
            info: info,
            success: success,
            warning: warning,

            // straight to console; bypass toastr
            log: $log.log
        };

        return service;
        /////////////////////

        //TODO: Toasters
        function error(message, data, title) {
            //toastr.error(message, title);
            console.log('Error: ' + message, data);
        }

        function info(message, data, title) {
            //toastr.info(message, title);
            console.log('Info: ' + message, data);
        }

        function success(message, data, title) {
            //toastr.success(message, title);
            console.log('Success: ' + message, data);
        }

        function warning(message, data, title) {
            //toastr.warning(message, title);
            console.log('Warning: ' + message, data);
        }
    }
}());
