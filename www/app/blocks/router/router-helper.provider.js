/* Help configure the state-base ui.router */
(function() {
    'use strict';

    angular
        .module('blocks.router')
        .provider('routerHelper', routerHelperProvider);

    routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];
    /* @ngInject */
    function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
        /* jshint validthis:true */
        var config = {
            docTitle:      undefined,
            resolveAlways: {}
        };

        $locationProvider.html5Mode(false);

        this.configure = function(cfg) {
            angular.extend(config, cfg);
        };

        this.$get = RouterHelper;
        RouterHelper.$inject = ['$location', '$rootScope', '$state', 'oauthClient', 'NotificationManager', '$ionicPlatform'];
        /* @ngInject */
        function RouterHelper($location, $rootScope, $state, oauthClient, NotificationManager, $ionicPlatform) {
            var handlingStateChangeError = false;
            var hasOtherwise = false;
            var stateCounts = {
                errors:  0,
                changes: 0
            };

            var service = {
                configureStates: configureStates,
                getStates:       getStates,
                stateCounts:     stateCounts,
                goPrevious:      goPrevious
            };

            var previousState = {};

            init();

            return service;

            ///////////////

            function configureStates(states, otherwisePath) {
                states.forEach(function(state) {
                    state.config.resolve =
                        angular.extend(state.config.resolve || {}, config.resolveAlways);
                    $stateProvider.state(state.state, state.config);
                });
                if(otherwisePath && !hasOtherwise) {
                    hasOtherwise = true;
                    $urlRouterProvider.otherwise(otherwisePath);
                    $urlRouterProvider.when('', '/adventures');
                }
            }

            function handleRoutingErrors() {
                // Route cancellation:
                // On routing error, go to the login screen.
                // Provide an exit clause if it tries to do it twice.
                $rootScope.$on('$stateChangeError',
                    function(event, toState, toParams, fromState, fromParams, error) {
                        if(handlingStateChangeError) {
                            return;
                        }
                        stateCounts.errors++;
                        handlingStateChangeError = true;
                        var destination = (toState &&
                            (toState.title || toState.name || toState.loadedTemplateUrl)) ||
                            'unknown target';
                        var msg = 'Error routing to ' + destination + '. ' +
                            (error.data || '') + '. <br/>' + (error.statusText || '') +
                            ': ' + (error.status || '');
                        NotificationManager.showError(msg, [toState]);
                        $location.path('/');
                    }
                );
            }

            function init() {
                checkOnChange();
                handleRoutingErrors();
                updateDocTitle();
            }

            function getStates() {
                return $state.get();
            }

            function updateDocTitle() {
                $rootScope.$on('$stateChangeSuccess',
                    function(event, toState, toParams, fromState, fromParams) {
                        stateCounts.changes++;
                        handlingStateChangeError = false;
                        var title = config.docTitle + ' ' + (toState.title || '');
                        $rootScope.title = title; // data bind to <title>
                    }
                );
            }

            function checkOnChange() {
                $rootScope.$on('$stateChangeStart',
                    function(event, toState, toParams, fromState, fromParams) {
                        if(toState && toState.savePrevious && fromState && fromState.name !== toState.name && !fromState.savePrevious) {
                            previousState.name = fromState.name;
                            previousState.params = fromParams;
                        }

                        //TODO: Uncomment when authorization is implemented.
                        if(toState.isProtected) {

                            $ionicPlatform.ready(function() {
                                oauthClient.isLoggedIn()
                                    .then(function() {
                                    }, function() {
                                        // Force logout and get new client
                                        // credentials token
                                        oauthClient.logout();
                                    });
                            });
                        }
                    });
            }

            function goPrevious() {
                if(previousState.name) {
                    $state.go(previousState.name, previousState.params);
                }
            }
        }
    }
})();
