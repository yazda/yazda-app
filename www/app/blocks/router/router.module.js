(function() {
    'use strict';

    angular.module('blocks.router', [
        'ui.router',
        'blocks.logger',
        'yazda.yazda-api'
    ]);
})();
