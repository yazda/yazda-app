(function () {
    'use strict';

    describe('Service: followersService', function () {
        var baseMock,
            TARGET_URL = 'adventures',
            paramObj = {
                data: 'TESTDATA'
            },
            id = "TESTID";

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'followers',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
        });

        it('should load followers', function () {
            expect(followers).to.be.ok();
        });

        describe('Function getIndex', function() {
            var id = 'TESTID',
            TARGET_URL = 'users/' + id + '/followers';

            it('Should call oauthClient.get', function() {
                followers.index(id);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL)).to.be.true();
            });
        });
    });
})();
