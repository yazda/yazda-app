(function() {
    angular
        .module('yazda.yazda-api')
        .constant('oauthConstants', {
            'clientId':           '8051cb8e2c71c63f781d5c2ddb8ab3f241cabbd30a91c7a4a8160e2f6ac1c648',
            'clientSecret':       'bc93eac07c3e7529787f980ef427d790b002bd3fc1364b4241af25f39844fa8c',
            'accessTokenUri':     'http://yazda-server.dev/oauth/token',
            'authorizationUri':   'http://yazda-server.dev/oauth/authorize',
            'version':            'v0.1',
            'baseURL':            'http://yazda-server.dev',
            'facebookAppId':      '980385968648785',
            'stravaClientId':     '',
            'stravaClientSecret': '',
            'branchKey':          'key_test_bmlIK0C3ahGT5n7Xm5jpypkbwAacXSma',
            'googleAnalyticsID':  '',
            "websocketURL":       "ws://localhost:8080"
        });
})();
