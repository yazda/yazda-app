(function() {
    angular
        .module('yazda.yazda-api')
        .factory('oauthClient', oauthClient);

    oauthClient.$inject = ['$rootScope', 'oauthConstants', '$q', 'NotificationManager', 'token'];
    function oauthClient($rootScope, oauthConstants, $q, NotificationManager, token) {

        var PASSWORD = 'password';
        var CLIENT_CREDENTIAL = 'credentials';

        function changeForgottenPassword(token, password, passwordConfirmation) {
            return put('/reset_password',
                {
                    reset_password_token:  token,
                    password:              password,
                    password_confirmation: passwordConfirmation
                });
        }

        function forgotPassword(email) {
            return post('/reset_password', {email: email});
        }

        function register(obj) {
            return post('/register', obj)
                .then(function() {
                    return token.login(obj.email, obj.password, true);
                });
        }

        function loginWithStrava(result, broadcast) {
            var access = {access_token: result.access_token};

            return get('/oauth/strava', access)
                .then(function(result) {
                    return token.setValue(result.body, true)
                        .then(function() {
                            if(broadcast) {
                                return $rootScope.$broadcast(broadcast);
                            }
                            return;
                        });
                });
        }

        function loginWithFacebook(result, broadcast) {
            return get('/oauth/facebook', result)
                .then(function(result) {
                    return token.setValue(result.body, true)
                        .then(function() {
                            if(broadcast) {
                                return $rootScope.$broadcast(broadcast);
                            }
                        });
                });
        }

        function get(path, params, cache) {
            return token
                .getValue()
                .then(function(oauthToken) {
                    return oauthToken.request({
                        method: 'get',
                        url:    oauthConstants.baseURL + '/' + path,
                        body:   params,
                        cache:  cache
                    });
                }, function() {
                    return token.logout();
                });
        }

        function post(path, params) {
            return token
                .getValue()
                .then(function(oauthToken) {
                    return oauthToken.request({
                        method: 'post',
                        url:    oauthConstants.baseURL + '/' + path,
                        data:   params
                    });
                }, function() {
                    return token.logout();
                });
        }

        function patch(path, params) {
            return token
                .getValue()
                .then(function(oauthToken) {
                    return oauthToken.request({
                        method: 'patch',
                        url:    oauthConstants.baseURL + '/' + path,
                        data:   params
                    });
                }, function() {
                    return token.logout();
                });
        }

        function put(path, params) {
            return token
                .getValue()
                .then(function(oauthToken) {
                    return oauthToken.request({
                        method: 'put',
                        url:    oauthConstants.baseURL + '/' + path,
                        data:   params
                    });
                }, function() {
                    return token.logout();
                });
        }

        function destroy(path, params) {
            return token
                .getValue()
                .then(function(oauthToken) {
                    oauthToken.request({
                        method: 'delete',
                        url:    oauthConstants.baseURL + '/' + path,
                        data:   params
                    });
                }, function() {
                    return token.logout();
                });
        }

        return {
            logout:                  token.logout,
            login:                   token.login,
            loginWithFacebook:       loginWithFacebook,
            loginWithStrava:         loginWithStrava,
            isLoggedIn:              token.isLoggedIn,
            register:                register,
            forgotPassword:          forgotPassword,
            changeForgottenPassword: changeForgottenPassword,
            get:                     get,
            post:                    post,
            patch:                   patch,
            destroy:                 destroy,
            token:                   token.getAccessToken
        };
    }
})();
