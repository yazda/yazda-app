(function() {
    angular
        .module('yazda.yazda-api')
        .factory('invites', invites);

    invites.$inject = ['oauthClient'];
    function invites(oauth) {
        var BASE_URL = 'invites';

        function getPending(options) {
            return oauth.get(BASE_URL + '/pending', options);
        }

        function accept(id) {
            return oauth.patch(BASE_URL + '/accept?adventure_id=' + id);
        }

        function reject(id) {
            return oauth.patch(BASE_URL + '/reject?adventure_id=' + id);
        }

        return {
            pending: getPending,
            accept:  accept,
            reject:  reject
        };
    }
})();
