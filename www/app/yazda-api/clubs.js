(function() {
    angular
        .module('yazda.yazda-api')
        .factory('clubs', clubs);

    clubs.$inject = ['oauthClient', '$cacheFactory'];
    function clubs(oauth, $cacheFactory) {
        var BASE_URL = 'clubs/';
        var cache = $cacheFactory('clubs');

        function searchClubs(options) {
            return oauth.get(BASE_URL, options);
        }

        function createClub(obj) {
            return oauth.post(BASE_URL, obj);
        }

        function updateClub(obj) {
            return oauth
                .patch(BASE_URL + obj.id, obj)
                .then(function() {
                    cache.removeAll();
                });
        }

        function getMembers(id, options) {
            return oauth.get(BASE_URL + id + '/members', options);
        }

        function showClub(id) {
            return oauth.get(BASE_URL + id, null, cache);
        }

        function join(id) {
            cache.removeAll();

            return oauth
                .post(BASE_URL + id + '/join');
        }

        function updateNotification(id, obj) {
            cache.removeAll();
            return oauth.patch(BASE_URL + id + '/update_notification', obj);
        }

        function leave(id) {
            cache.removeAll();
            return oauth.post(BASE_URL + id + '/leave');
        }

        function updateMember(id, obj) {
            return oauth.patch(BASE_URL + id + '/update_member', obj);
        }

        //function destroyAdventure(id) {
        //    return oauth.destroy(BASE_URL + '/' + id);
        //}

        function updateProfileImage(id, obj) {
            return oauth.post(BASE_URL + id + '/update_profile_image',
                {avatar: 'data:image/png;base64,' + obj});
        }

        function updateBannerImage(id, obj) {
            return oauth.post(BASE_URL + id + '/update_banner_image',
                {banner: 'data:image/png;base64,' + obj});
        }

        return {
            index:              searchClubs,
            members:            getMembers,
            update:             updateClub,
            show:               showClub,
            join:               join,
            leave:              leave,
            create:             createClub,
            updateProfileImage: updateProfileImage,
            updateBannerImage:  updateBannerImage,
            updateMember:       updateMember,
            updateNotification: updateNotification
            //update:  updateAdventure,
            //destroy: destroyAdventure
        };
    }
})();
