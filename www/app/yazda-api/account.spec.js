(function () {
    'use strict';

    describe('Service: AccountService', function () {
        var baseMock;

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'account',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
            sinon.spy(oauthClient, 'patch');
            sinon.spy(oauthClient, 'post');
        });

        it('should load account', function () {
            expect(account).to.be.ok();
        });

        describe('Function getSettings', function() {
            var TARGET_URL = 'account/settings';

            it('Should call oauthClient.get', function() {
                account.getSettings();
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL)).to.be.true();
            });
        });

        describe('Function getMe', function() {
            var TARGET_URL = 'account/me';

            it('Should call oauthClient.get', function() {
                account.me();
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL)).to.be.true();
            });
        });

        describe('Function updateSettings', function() {
            var TARGET_URL = 'account/settings',
                paramObj = {
                    data: 'TESTDATA'
                };

            it('Should call oauthClient.patch', function() {
                account.updateSettings(paramObj)
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function updateProfileImage', function() {
            var TARGET_URL = 'account/update_profile_image';
            it('Should call oauthClient.post', function() {
                var paramObj = {
                    data: 'TESTDATA'
                };
                account.updateProfileImage(paramObj);
                $rootScope.$apply();
                expect(oauthClient.post.calledWith(
                    TARGET_URL,
                    {avatar: 'data:image/png;base64,' + paramObj}
                )).to.be.true();
            });
        });
    });
})();
