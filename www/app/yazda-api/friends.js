(function() {
    angular
        .module('yazda.yazda-api')
        .factory('friends', friends);

    friends.$inject = ['oauthClient'];
    function friends(oauth) {
        var BASE_URL = 'friends';

        function getIndex(id, params) {
            return oauth.get('users/' + id + '/' + BASE_URL, params);
        }

        return {
            index: getIndex
        };
    }
})();
