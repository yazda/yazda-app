(function() {
    angular
        .module('yazda.yazda-api')
        .factory('account', account);

    account.$inject = ['oauthClient', '$cacheFactory', '$q'];
    function account(oauth, $cacheFactory, $q) {
        var BASE_URL = 'account';
        var cache = $cacheFactory('account');
        var ME_KEY = 'me';

        function getSettings() {
            return oauth.get(BASE_URL + '/settings');
        }

        function getMe(force) {
            if(cache.get(ME_KEY) && !force) {
                return $q.when(cache.get(ME_KEY));
            } else {
                return oauth.get(BASE_URL + '/me')
                    .then(function(data) {
                        cache.put(ME_KEY, data);
                        return data;
                    });
            }
        }

        function updateSettings(obj) {
            return oauth.patch(BASE_URL + '/settings', obj);
        }

        function updateProfileImage(obj) {
            return oauth.post(BASE_URL + '/update_profile_image',
                {avatar: 'data:image/png;base64,' + obj});
        }

        function updateBannerImage(obj) {
            return oauth.post(BASE_URL + '/update_banner_image',
                {banner: 'data:image/png;base64,' + obj});
        }

        return {
            getSettings:        getSettings,
            updateSettings:     updateSettings,
            me:                 getMe,
            updateProfileImage: updateProfileImage,
            updateBannerImage:  updateBannerImage
        };
    }
})();
