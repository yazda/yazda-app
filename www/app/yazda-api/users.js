(function() {
    angular
        .module('yazda.yazda-api')
        .factory('users', users);

    users.$inject = ['oauthClient', '$cacheFactory'];
    function users(oauth, $cacheFactory) {
        var BASE_URL = 'users';
        var cache = $cacheFactory('users');

        function showUser(id) {
            return oauth.get(BASE_URL + '/' + id, null, cache);
        }

        function searchUsers(options) {
            return oauth.get(BASE_URL + '/search', options);
        }

        function inviteFriendsWithEmail(emails) {
            return oauth.post(BASE_URL + '/invite_friends_by_email', {emails: emails});
        }

        function findFriendWithEmail(emails) {
            return oauth.post(BASE_URL + '/find_friends_with_email', {emails: emails});
        }

        function findFriendsWithStrava() {
            return oauth.get(BASE_URL + '/find_strava_users');
        }

        function findFriendsWithFacebook() {
            return oauth.get(BASE_URL + '/find_facebook_users');
        }

        return {
            show:                   showUser,
            search:                 searchUsers,
            findFriendWithEmail:    findFriendWithEmail,
            inviteFriendsWithEmail: inviteFriendsWithEmail,
            findFriendsWithStrava: findFriendsWithStrava,
            findFriendsWithFacebook: findFriendsWithFacebook
        };
    }
})();
