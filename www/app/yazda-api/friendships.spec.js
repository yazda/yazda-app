(function () {
    'use strict';

    describe('Service: friendshipsService', function () {
        var baseMock,
            TARGET_URL = 'adventures',
            paramObj = {
                data: 'TESTDATA'
            },
            id = "TESTID";

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'friendships',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
            sinon.spy(oauthClient, 'patch');
            sinon.spy(oauthClient, 'post');
        });

        it('should load friendships', function () {
            expect(friendships).to.be.ok();
        });

        describe('Function getIncoming', function() {
            var TARGET_URL = 'friendships/incoming',
                paramObj = {
                    data: 'TEST_data'
                };

            it('Should call oauthClient.get', function() {
                friendships.incoming(paramObj);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function getOutgoing', function() {
            var TARGET_URL = 'friendships/outgoing',
                paramObj = {
                    data: 'TEST_data'
                };

            it('Should call oauthClient.get', function() {
                friendships.outgoing(paramObj);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function createFriendship', function() {
            var TARGET_URL = 'friendships',
                userId = 'TESTID';

            it('Should call oauthClient.post', function() {
                friendships.create(userId);
                $rootScope.$apply();
                expect(oauthClient.post.calledWith(
                    TARGET_URL,
                    {user_id: userId}
                )).to.be.true();
            });
        });

        describe('Function acceptFriendship', function() {
            var id ='TESTID',
                TARGET_URL = 'friendships/' + id + '/accept';

            it('Should call oauthClient.patch', function() {
                friendships.accept(id);
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(TARGET_URL)).to.be.true();
            });
        });

        describe('Function acceptFriendship', function() {
            var id ='TESTID',
                TARGET_URL = 'friendships/' + id + '/reject';

            it('Should call oauthClient.patch', function() {
                friendships.reject(id);
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(TARGET_URL)).to.be.true();
            });
        });
    });
})();
