(function () {
    'use strict';

    describe('Service: friendsService', function () {
        var baseMock,
            TARGET_URL = 'adventures',
            paramObj = {
                data: 'TESTDATA'
            },
            id = "TESTID";

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'friends',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
        });

        it('should load friends', function () {
            expect(friends).to.be.ok();
        });

        describe('Function getIndex', function() {
            var id = 'TESTID',
                TARGET_URL = 'users/' + id + '/friends';

            it('Should call oauthClient.get', function() {
                friends.index(id);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL)).to.be.true();
            });
        });
    });
})();
