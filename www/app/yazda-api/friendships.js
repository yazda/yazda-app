(function() {
    angular
        .module('yazda.yazda-api')
        .factory('friendships', friendships);

    friendships.$inject = ['oauthClient'];
    function friendships(oauth) {
        var BASE_URL = 'friendships';

        function getIncoming(options) {
            return oauth.get(BASE_URL + '/incoming', options);
        }

        function getOutgoing(obj) {
            return oauth.get(BASE_URL + '/outgoing', obj);
        }

        function createFriendship(userId) {
            return oauth.post(BASE_URL, {user_id: userId});
        }

        function acceptFriendship(id) {
            return oauth.patch(BASE_URL + '/' + id + '/accept');
        }

        function rejectFriendship(id) {
            return oauth.patch(BASE_URL + '/' + id + '/reject');
        }

        return {
            incoming: getIncoming,
            outgoing: getOutgoing,
            create:   createFriendship,
            accept:   acceptFriendship,
            reject:   rejectFriendship
        };
    }
})();
