(function() {
    angular
        .module('yazda.yazda-api')
        .factory('devices', account);

    account.$inject = ['oauthClient'];
    function account(oauth) {
        var BASE_URL = 'devices';

        function createDevice(obj) {
            return oauth.post(BASE_URL, obj);
        }

        return {
            create: createDevice
        };
    }
})();
