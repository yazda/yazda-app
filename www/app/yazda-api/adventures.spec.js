(function () {
    'use strict';

    describe('Service: AdventureService', function () {
        var baseMock,
            TARGET_URL = 'adventures',
            paramObj = {
                data: 'TESTDATA'
            },
            id = "TESTID";

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'adventures',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
            sinon.spy(oauthClient, 'post');
            sinon.spy(oauthClient, 'patch');
            sinon.spy(oauthClient, 'destroy');
        });

        it('should load adventures', function () {
            expect(adventures).to.be.ok();
        });

        describe('Function getSettings', function() {
            it('Should call oauthClient.get', function() {
                adventures.index(paramObj);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function createAdventure', function() {
            it('Should call oauthClient.post', function() {
                adventures.create(paramObj);
                $rootScope.$apply();
                expect(oauthClient.post.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function showAdventure', function() {
            it('Should call oauthClient.get', function() {
                adventures.show(id);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL + '/' + id)).to.be.true();
            });
        });

        describe('Function updateAdventure', function() {
            it('Should call oauthClient.patch', function() {
                adventures.update(id, paramObj);
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(
                    TARGET_URL + '/' + id,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function destroyAdventure', function() {
            it('Should call oauthClient.destroy', function() {
                adventures.destroy(id);
                $rootScope.$apply();
                expect(oauthClient.destroy.calledWith(TARGET_URL + '/' + id)).to.be.true();
            });
        });
    });
})();
