(function () {
    'use strict';

    describe('Service: usersService', function () {
        var baseMock;

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'users',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
        });

        it('should load users', function () {
            expect(users).to.be.ok();
        });

        describe('Function showUser', function() {
            var id = 'TESTID',
                TARGET_URL = 'users/' + id;

            it('Should call oauthClient.get', function() {
                users.show(id);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(TARGET_URL)).to.be.true();
            });
        });

        describe('Function searchUsers', function() {
            var TARGET_URL = 'users/search',
                paramObj = {data: 'TEST_data'};

            it('Should call oauthClient.get', function() {
                users.search(paramObj);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });
    });
})();
