(function() {
  angular
    .module('yazda.yazda-api')
    .factory('followers', followers);

    followers.$inject = ['oauthClient'];
  function followers(oauth) {
    var BASE_URL = 'followers';

    function getIndex(id) {
      return oauth.get('users/' + id + '/' + BASE_URL);
    }

    return {
      index: getIndex
    };
  }
})();
