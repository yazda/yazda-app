(function() {
    angular
        .module('yazda.yazda-api')
        .factory('token', token);

    token.$inject = ['$rootScope', '$q', '$ionicLoading', '$ionicPopup', 'oauthConstants', '$http', '$cordovaSQLite', '$ionicPlatform', '$cacheFactory', 'NotificationManager'];
    function token($rootScope, $q, $ionicLoading, $ionicPopup, oauthConstants, $http, $cordovaSQLite, $ionicPlatform, $cacheFactory, NotificationManager) {
        var cache = $cacheFactory('oauthToken');
        var PASSWORD = 'password';
        var CLIENT_CREDENTIAL = 'credentials';
        var db;

        $ionicPlatform.ready(function() {
            var query = 'CREATE TABLE IF NOT EXISTS creds (id INTEGER PRIMARY KEY, token TEXT);';

            db = $cordovaSQLite.openDB({name: "yazda.db", location: 'default'});
            $cordovaSQLite.execute(db, query);
        });

        var client = new ClientOAuth2({
            clientId:            oauthConstants.clientId,
            clientSecret:        oauthConstants.clientSecret,
            accessTokenUri:      oauthConstants.accessTokenUri,
            authorizationUri:    oauthConstants.authorizationUri,
            redirectUri:         oauthConstants.redirectUri,
            authorizationGrants: ['client_credentials', 'password', 'refresh_token']
        });

        client.request = function(opts) {
            var options = {
                method:  opts.method.toUpperCase(),
                url:     opts.url,
                params:  opts.body ? opts.body : opts.params,
                data:    opts.data,
                headers: opts.headers
            };

            $ionicLoading.show({
                template: '<ion-spinner class="spinner-energized"></ion-spinner>',
                delay:    125
            });

            return $http(options)
                .then(null, function(error) {
                    console.log(error.TEST);
                    if(error.status === 401) {
                        logout();
                        return $q.reject(error);
                    } else {
                        console.log(error);
                        console.log(error.status);

                        var msg = '';

                        if(error.data && error.data.errors) {
                            _.each(error.data.errors, function(val) {
                                msg += '<p>' + val + '</p>';
                            });
                        } else {
                            switch(error.status) {
                                case 401:
                                    msg = 'Invalid email/password. Please' +
                                        ' try again.';
                                    break;
                                case 0:
                                    msg = 'Error connecting to server. Please' +
                                        ' try again.';
                                    break;
                            }
                        }

                        $ionicPopup.alert({
                            title:    'Error',
                            template: msg,
                            cssClass: 'error'
                        });

                        return $q.reject(error);
                    }
                })
                .finally(function() {
                    $ionicLoading.hide();
                });
        };

        function setValue(token, isPassword) {
            var obj;
            var query = "INSERT OR REPLACE INTO creds (id, token) VALUES (1, ?)";

            if(token) {
                obj = {
                    accessToken:  token.accessToken,
                    expires:      token.expires,
                    grantType:    isPassword ? PASSWORD : CLIENT_CREDENTIAL,
                    refreshToken: token.refreshToken,
                    tokenType:    token.tokenType
                };

                obj = JSON.stringify(obj);
            }

            return $cordovaSQLite.execute(db, query, [obj])
                .then(function(res) {
                    cache.remove('token');
                });
        }

        function getValue() {
            var obj = window.localStorage.getItem('token');
            var query = "SELECT * FROM creds;";
            var deferred = $q.defer();

            if(obj) {
                var token = JSON.parse(obj);
                var newClient = client.createToken(token.accessToken, token.refreshToken, token.tokenType);

                newClient.grantType = token.grantType;
                window.localStorage.removeItem('token');
                setValue(token, token.grantType === PASSWORD);

                deferred.resolve(newClient);

                return deferred.promise;
            } else if(cache.get('token')) {
                deferred.resolve(cache.get('token'));

                return deferred.promise;
            } else {
                return $cordovaSQLite.execute(db, query)
                    .then(function(res) {
                        if(res.rows.length > 0) {
                            var token = JSON.parse(res.rows.item(0).token);

                            var newClient = client.createToken(token.accessToken, token.refreshToken, token.tokenType);
                            newClient.grantType = token.grantType;

                            cache.put(newClient);
                            return newClient;
                        } else {
                            return $q.reject('no token');
                        }
                    });
            }
        }

        function getAccessToken() {
            return getValue()
                .then(function(token) {
                    return token.accessToken;
                });
        }

        function isLoggedIn() {
            return getValue()
                .then(function(oauthToken) {
                    if(oauthToken.grantType === PASSWORD) {
                        return $q.resolve();
                    } else {
                        return $q.reject();
                    }
                });
        }

        function logout() {
            setValue(undefined)
                .then(function() {
                    $rootScope.$broadcast('user.logout');
                    return login();
                });
        }

        function login() {
            if(arguments.length > 0) {
                var username = arguments[0];
                var password = arguments[1];
                var register = arguments[2];

                return client
                    .owner
                    .getToken(username, password)
                    .then(function(user) {
                        console.log('!!!!! client.owner.getToken.then -- > user --> ' + user + '!!!!!');
                        return setValue(user, true)
                            .then(function() {
                                if(register) {
                                    $rootScope.$broadcast('user.wizard');
                                } else {
                                    $rootScope.$broadcast('user.login');
                                }
                            });
                    }, function() {
                        NotificationManager.showError({code: 'AUTH'});
                        return $q.reject();
                    });
            } else {
                return client.credentials
                    .getToken()
                    .then(function(user) {
                        return setValue(user)
                            .then(function() {
                                return $q.resolve();
                            });
                    });
            }
        }

        return {
            client:         client,
            logout:         logout,
            login:          login,
            isLoggedIn:     isLoggedIn,
            setValue:       setValue,
            getValue:       getValue,
            getAccessToken: getAccessToken,
            isAuthorized:   getValue
        };
    }
})();
