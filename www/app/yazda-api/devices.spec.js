(function () {
    'use strict';

    describe('Service: devicesService', function () {
        var baseMock,
            TARGET_URL = 'adventures',
            paramObj = {
                data: 'TESTDATA'
            },
            id = "TESTID";

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'devices',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'post');
        });

        it('should load devices', function () {
            expect(devices).to.be.ok();
        });

        describe('Function createDevice', function() {
            var TARGET_URL = 'devices',
                paramObj = {data: "TESTDATA"};

            it('Should call oauthClient.post', function() {
                devices.create(paramObj);
                $rootScope.$apply();
                expect(oauthClient.post.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });
    });
})();
