(function() {
    angular
        .module('yazda.yazda-api')
        .factory('adventures', adventures);

    adventures.$inject = ['oauthClient', '$cacheFactory'];
    function adventures(oauth, $cacheFactory) {
        var BASE_URL = 'adventures';
        var cache = $cacheFactory('adventures');

        function getAdventures(options) {
            return oauth.get(BASE_URL, options);
        }

        function createAdventure(obj) {
            return oauth.post(BASE_URL, obj);
        }

        function showAdventure(id) {
            return oauth.get(BASE_URL + '/' + id, null, cache);
        }

        function updateAdventure(id, obj) {
            cache.removeAll();
            return oauth.patch(BASE_URL + '/' + id, obj);
        }

        function destroyAdventure(id) {
            return oauth.destroy(BASE_URL + '/' + id);
        }

        return {
            index:   getAdventures,
            create:  createAdventure,
            show:    showAdventure,
            update:  updateAdventure,
            destroy: destroyAdventure
        };
    }
})();
