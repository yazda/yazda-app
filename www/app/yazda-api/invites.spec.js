(function () {
    'use strict';

    describe('Service: invitesService', function () {
        var baseMock;

        beforeEach(function () {
            bard.appModule('yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                'oauthClient',
                'invites',
                '$q',
                '$rootScope'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            baseMock = mocks.Base($q);

            sinon.spy(oauthClient, 'get');
            sinon.spy(oauthClient, 'patch');
        });

        it('should load invites', function () {
            expect(invites).to.be.ok();
        });

        describe('Function getPending', function() {
            var TARGET_URL = 'invites/pending',
                paramObj = {data: 'TEST_data'};

            it('Should call oauthClient.get', function() {
                invites.pending(paramObj);
                $rootScope.$apply();
                expect(oauthClient.get.calledWith(
                    TARGET_URL,
                    paramObj
                )).to.be.true();
            });
        });

        describe('Function accept', function() {
            var id = 'TESTID',
                TARGET_URL = 'invites/accept?adventure_id=' + id;

            it('Should call oauthClient.patch', function() {
                invites.accept(id);
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(TARGET_URL)).to.be.true();
            });
        });

        describe('Function reject', function() {
            var id = 'TESTID',
                TARGET_URL = 'invites/reject?adventure_id=' + id;

            it('Should call oauthClient.patch', function() {
                invites.reject(id);
                $rootScope.$apply();
                expect(oauthClient.patch.calledWith(TARGET_URL)).to.be.true();
            });
        });
    });
})();
