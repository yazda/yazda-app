(function() {
    'use strict';

    angular
        .module('yazda.controllers', [
            'yazda.core'
        ]);
})();
