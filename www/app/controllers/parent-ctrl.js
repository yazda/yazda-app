(function() {
    'use strict';
    angular
        .module('yazda.controllers')
        .controller('ParentCtrl', ParentCtrl);

    /* @ngInject */
    function ParentCtrl($scope, $rootScope, $state, $cordovaDevice, $cordovaBadge,
                        $window, oauthClient, account, devices, AnalyticsFactory, WebSockets, $ionicPlatform) {
        $rootScope.logout = logout;

        $scope.$on('user.wizard', redirectToWizard);
        $scope.$on('user.login', redirectToAdventures);
        $scope.$on('user.logout', redirectToLogin);

        $ionicPlatform.ready(function() {
            oauthClient.isLoggedIn()
                .then(function() {
                    registerDevice();
                }, function() {
                    redirectToLogin();
                });
        });

        function registerDevice() {
            var push;
            //TODO: MOCK $window.PushNotification
            //TODO: see -->  https://github.com/phonegap/phonegap-plugin-push
            if($window.PushNotification) {
                push = $window.PushNotification.init({
                    android: {senderID: '3269359761'},
                    ios:     {badge: true, sound: true, alert: true}
                });

                push.on('notification', function(data) {
                    $cordovaBadge
                        .hasPermission()
                        .then(function() {
                            $cordovaBadge
                                .increase()
                                .then(function() {
                                    $cordovaBadge
                                        .get()
                                        .then(function() {
                                            var badge = data.additionalData.notification_type + '_badge';
                                            var notBadge = $rootScope[badge];

                                            $rootScope[badge] = notBadge + 1;

                                            window.localStorage.setItem(badge, notBadge + 1);

                                            if(data.additionalData.url) {
                                                handleOpenURL(data.additionalData.url);
                                            }

                                        });
                                });
                        })
                        .finally(function() {
                            if(!data.additionalData.foreground) {
                                push.finish();
                            }
                        });
                });

                push.on('registration', function(data) {
                    var deviceToken = data.registrationId;
                    var name = $cordovaDevice.getModel();

                    devices
                        .create({
                            name:        name,
                            device_type: ionic.Platform.isAndroid() ? 'android' : 'ios',
                            identifier:  deviceToken
                        });
                });
            }
        }

        function logout() {
            WebSockets.disconnect();
            oauthClient.logout();
            AnalyticsFactory.logout();
        }

        function redirectToLogin() {
            console.log('caught broadcast user.logout');

            $state.go('login');
        }

        function redirectToWizard() {
            console.log('caught broadcast user.wizard');

            $state.go('wizard');
        }

        function redirectToAdventures() {
            console.log('caught broadcast user.login');

            oauthClient.isLoggedIn()
                .then(function() {
                    registerDevice();

                    account
                        .me(true)
                        .then(function(response) {
                            window.localStorage.setItem('current_user_id', response.user.id);

                            AnalyticsFactory.trackUserId(Number(response.user.id));

                            $state.go('loggedIn.adventures-index');
                        });
                }, function() {
                    $rootScope.$broadcast('user.logout');
                });
        }
    }
})();
