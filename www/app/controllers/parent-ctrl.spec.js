(function() {
    'use strict';

    describe('Controller: ParentCtrl', function() {
        var vm,
            $scope;

        beforeEach(function () {
            bard.appModule('yazda.controllers', 'ngCordovaMocks', 'yazda.yazda-api', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function () {});
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$state',
                '$cordovaDevice',
                '$cordovaBadge',
                '$window',
                'oauthClient',
                'account',
                'devices'
            );
            bard.mockService($state, mocks.StateMock($q));
            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            bard.mockService(account, mocks.AccountMock($q));
            bard.mockService(devices, mocks.DevicesMock($q));
            //bard.mockService($window, mocks.PushNotificationMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();
            vm = $controller('ParentCtrl', {$scope: $scope});
            $rootScope.$apply();

            sinon.spy($state, 'go');
            sinon.spy(account, 'me');
            sinon.spy(oauthClient, 'isLoggedIn');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open ParentCtrl', function() {
            expect(vm).to.be.ok();
        });

        describe('Function redirectToLogin', function() {
            it('Should change states to login', function() {
                $scope.$emit('user.logout');
                $rootScope.$apply();
                expect($state.go.calledWith('login')).to.be.true();
            });
        });

        describe('Function redirectToAdventures', function() {
            beforeEach(function() {
                $scope.$emit('user.login');
                $rootScope.$apply();
            });
            it('Should call account.me', function() {
                expect(account.me.called).to.be.true();
            });
            it('Should change states to loggedIn.adventures-index', function() {
                expect($state.go.calledWith('loggedIn.adventures-index')).to.be.true();
            });
        });

        describe('Event deviceready', function() {
            var event;
            beforeEach(function() {
                event = document.createEvent('Event');
                event.initEvent('deviceready', true, true);
            });

            it('Should oauthClient.isLoggedIn', function() {
                document.dispatchEvent(event);
                $rootScope.$apply();
                expect(oauthClient.isLoggedIn.called).to.be.true();
            });

            describe('If oauthCLient.isLoggedIn returns false', function() {
               it('Should re-route to the "login" state', function() {
                   oauthClient.setLoggedIn(false);
                   document.dispatchEvent(event);
                   $rootScope.$apply();
                   expect($state.is('login')).to.be.true();
               });
            });

            describe('If oauthCLient.isLoggedIn returns true', function() {
                it('Should re-route to the "loggedIn.adventures-index" state', function() {
                    oauthClient.setLoggedIn(true);

                    document.dispatchEvent(event);
                    $rootScope.$apply();

                    //TODO: uncomment when $window is being succesfully mocked
                   /* expect($window.PushNotification.init.calledWith({
                        android: {senderID: '3269359761'},
                        ios: {badge: true, sound: true, alert: true}
                    })).to.be.true();*/
                });
            });
        });

        describe('Function logout', function() {
            it('Should call oauthClient.logout', function() {
                $rootScope.logout();
                $rootScope.$apply();
                expect(oauthClient.logout.called).to.be.true();
            });
        });
    });
})();
