(function() {
    'use strict';

    describe('Controller: UserCtrl', function() {
        var vm,
            $scope;

        beforeEach(function () {
            bard.appModule('yazda.controllers', 'yazda.yazda-api', function ($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function () {
                });
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                '$stateParams',
                '$state',
                '$ionicHistory',
                'users',
                'friendships',
                'account'
            );
            bard.mockService($stateParams, mocks.StateParamsMock());
            bard.mockService($state, mocks.StateMock($q));
            bard.mockService($ionicHistory, mocks.IonicHistoryMock());
            bard.mockService(users, mocks.UsersMock($q));
            bard.mockService(friendships, mocks.FriendShipsMock($q));
            bard.mockService(account, mocks.AccountMock($q));
        });

        beforeEach(function () {
            $scope = $rootScope.$new();
            vm = $controller('UserCtrl', {$scope: $scope});
            $rootScope.$apply();

            sinon.spy(friendships, 'create');
        });

        bard.verifyNoOutstandingHttpRequests();

        describe('For: activate()', function () {
            it('Should: call getUser()', function () {
                var user = {
                    id: 'TestID'
                };

                //setup data
                vm.profile = 'profile';

                vm.activate();
                $rootScope.$apply();

                expect(vm.data).to.be.eql(user);
            });

            it('if no profile then Should: call users.show()', function () {
                var user = {
                    id: '123'
                };

                vm.activate();
                $rootScope.$apply();

                expect(vm.data).to.be.eql(user);
            });
        });
        describe('For: viewFriends()', function () {

            describe('If:$ionicHistory.currentStateName() === \'loggedIn.adventure-users-show\'', function () {

                it('Should: set $state.go(\'loggedIn.adventure-friends-index\', {id: vm.data.id});', function () {

                    $ionicHistory.currentStateName = function () {
                        return 'loggedIn.adventure-users-show';
                    };

                    //setup local data
                    vm.data = {id: 123};

                    vm.viewFriends();
                    $rootScope.$apply();

                    expect($state.current()).to.eq('loggedIn.adventure-friends-index');
                    expect($state.lastParams()).to.eql({id: 123});
                });
            });

            describe('Else if:$ionicHistory.currentStateName() === \'loggedIn.profile-show\'', function () {

                it('Should: set $state.go(\'loggedIn.adventure-friends-index\', {id: vm.data.id});', function () {

                    $ionicHistory.currentStateName = function () {
                        return 'loggedIn.profile-show';
                    };

                    //setup local data
                    vm.data = {id: 123};

                    vm.viewFriends();
                    $rootScope.$apply();

                    expect($state.current()).to.eq('loggedIn.profile-friends-index');
                    expect($state.lastParams()).to.eql({id: 123});
                });
            });
            describe('Else if:$ionicHistory.currentStateName() === \'loggedIn.invite-user-show\'', function () {

                it('Should: set $state.go(\'loggedIn.adventure-friends-index\', {id: vm.data.id});', function () {

                    $ionicHistory.currentStateName = function () {
                        return 'loggedIn.invite-user-show';
                    };

                    //setup local data
                    vm.data = {id: 123};

                    vm.viewFriends();
                    $rootScope.$apply();

                    expect($state.current()).to.eq('loggedIn.invite-friends-index');
                    expect($state.lastParams()).to.eql({id: 123});
                });
            });
        });

        describe('Function addFriend', function() {
            var id = 'TESTID';
            it('Should call friendships.create', function() {
                vm.data.request_sent = false;
                vm.addFriend(id);
                $rootScope.$apply();
                expect(friendships.create.calledWith(id)).to.be.true();
                expect(vm.data.request_sent).to.be.true();
            });
        });
    });
})();
