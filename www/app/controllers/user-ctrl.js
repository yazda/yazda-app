(function() {
    'use strict';

    angular
        .module('yazda.controllers')
        .controller('UserCtrl', UserController);

    /* @ngInject */
    function UserController($stateParams, $state, $ionicHistory, users,
                            friendships, account, BranchFactory, $ionicPlatform) {
        var vm = this;

        vm.getUser = getUser;
        vm.addFriend = addFriend;
        vm.viewFriends = viewFriends;
        vm.background = background;

        function background() {
            if(vm.data && vm.data.banner_image_thumb_url) {
                return 'url( ' + vm.data.banner_image_thumb_url + ')';
            }
        }

        if($stateParams.id || vm.profile) {
            $ionicPlatform.ready(activate);
        }

        function activate() {
            return getUser();
        }

        function viewFriends() {
            if($ionicHistory.currentStateName() === 'loggedIn.adventure-users-show') {
                $state.go('loggedIn.adventure-friends-index', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.profile-show' ||
                $ionicHistory.currentStateName() === 'loggedIn.profile-users-show') {
                $state.go('loggedIn.profile-friends-index', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.invite-user-show') {
                $state.go('loggedIn.invite-friends-index', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.club-users-show') {
                $state.go('loggedIn.club-friends-index', {id: vm.data.id});
            }
        }

        function addFriend(id) {
            friendships.create(id)
                .then(function(response) {
                    vm.data.request_sent = true;
                });
        }

        function getUser() {
            if(vm.profile) {
                return account
                    .me(true)
                    .then(setUserData);
            } else {
                return users
                    .show($stateParams.id)
                    .then(setUserData);
            }
        }

        function setUserData(data) {
            var user = data.user;

            vm.data = user;

            BranchFactory.createObj('user_' + user.id,
                user.name,
                user.description,
                user.profile_image_url,
                false, {
                    'url':             'yazda://users/' + user.id,
                    '$og_title':       user.name,
                    '$og_description': user.description,
                    '$og_image_url':   user.profile_image_url
                }).then(function() {
                BranchFactory.registerView();
            });

            return vm.data;
        }
    }
})();
