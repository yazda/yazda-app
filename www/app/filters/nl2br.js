(function() {
  angular
    .module('yazda')
    .filter('nl2br', ['$filter', nl2br]);

    function nl2br($filter) {
        return function(data) {
            if (!data) return data;
            return data.replace(/\n\r?/g, '<br />');
        };
    }

})();
