(function() {
    angular
        .module('yazda')
        .filter('groupByDayMonthYear', ['$parse', groupByDayMonthYear]);

    function groupByDayMonthYear($parse) {
        var dividers = {};

        return function(input) {
            if(!input || !input.length) {
                return;
            }

            var output = [],
                previousDate,
                currentDate;

            for(var i = 0, ii = input.length; i < ii && (item = input[i]); i++) {
                if(item.start_time) {
                    currentDate = moment(item.start_time);
                } else {
                    currentDate = moment(item.adventure.start_time);
                }
                if(!previousDate ||
                    currentDate.month() !== previousDate.month() ||
                    currentDate.day() !== previousDate.day() ||
                    currentDate.year() !== previousDate.year()) {

                    var dividerId = currentDate.format('MMMM Do YYYY');

                    if(!dividers[dividerId]) {
                        dividers[dividerId] = {
                            isDivider: true,
                            divider:   currentDate.format('dddd, MMMM Do')
                        };
                    }

                    output.push(dividers[dividerId]);
                }

                output.push(item);
                previousDate = currentDate;
            }

            return output;
        };
    }

})();
