(function() {
    'use strict';

    angular
        .module('yazda.openUrl', ['yazda.yazda-api'])
        .factory('openUrlService', ['$location', '$rootScope', '$ionicHistory', 'oauthClient', openUrlService])
        .run(['openUrlService', run]);

    function openUrlService($location, $rootScope, $ionicHistory, oauthClient) {
        return {
            handleOpenUrl: handleOpenUrl,
            onResume:      onResume
        };

        function openUrl(url) {
            $ionicHistory.nextViewOptions({
                historyRoot:      true,
                disableBack:      true,
                disableAnimation: true
            });

            oauthClient.isLoggedIn()
                .then(function() {
                    if(url.indexOf('reset_password') === -1) {
                        window.location.hash = url.replace('yazda://', '');
                        $rootScope.$broadcast('handleopenurl', url);
                    }
                }, function() {
                    if(url.indexOf('reset_password') !== -1) {
                        window.location.hash = url.replace('yazda://', '');
                        $rootScope.$broadcast('handleopenurl', url);
                    }
                }).finally(function() {
                window.cordova.removeDocumentEventHandler('handleopenurl');
                window.cordova.addStickyDocumentEventHandler('handleopenurl');
                document.removeEventListener('handleopenurl', handleOpenUrl);
            });
        }

        function handleOpenUrl(e) {
            openUrl(e.url);
        }

        function onResume() {
            document.addEventListener('handleopenurl', handleOpenUrl, false);
        }
    }

    function run(openUrlService) {
        if(openUrlService) {
            document.addEventListener('handleopenurl', openUrlService.handleOpenUrl, false);
            document.addEventListener('resume', openUrlService.onResume, false);
        }
    }

})();
