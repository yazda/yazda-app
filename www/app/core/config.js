(function() {
    'use strict';

    var core = angular.module('yazda.core');

    var config = {
        appErrorPrefix: '[yazda Error] ',
        appTitle:       'yazda'
    };

    core.value('config', config);

    core.run(CoreRun)
        .config(CoreConfig);

    CoreRun.$inject = ['$ionicPlatform', 'AnalyticsFactory', '$cordovaBadge'];

    function CoreRun($ionicPlatform, AnalyticsFactory, $cordovaBadge) {
        $ionicPlatform.ready(function() {
            AnalyticsFactory.init();
            //TODO: Figure out why jshint ignore:start/end isn't working here.

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false); // jshint ignore:line
                cordova.plugins.Keyboard.disableScroll(true); // jshint ignore:line
            }

            if(window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault(); // jshint ignore:line
            }

            $cordovaBadge.clear();
        });

        $ionicPlatform.on('resume', function(event) {
            AnalyticsFactory.init();
            $cordovaBadge.clear();
        });
    }

    CoreConfig.$inject = ['$ionicConfigProvider', 'routerHelperProvider', 'exceptionHandlerProvider'];

    function CoreConfig($ionicConfigProvider, routerHelperProvider, exceptionHandlerProvider) {
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({docTitle: config.appTitle + ': '});
        $ionicConfigProvider.views.transition('platform');
    }
})();

// Set Location of opening location
function handleOpenURL(url) {
    if(url.indexOf('yazda://') !== -1) {
        cordova.fireDocumentEvent('handleopenurl', {url: url});
    }
}

if('cordova' in window) {
    cordova.addStickyDocumentEventHandler('handleopenurl');
}

function DeepLinkHandler(link) {
    if(link.url) {
      if(link.url === 'yazda://') {
          link.url = 'yazda://adventures';
      }

        handleOpenURL(link.url);
    }
}

function NonBranchLinkHandler(link) {
    if(link.url) {
      if(link.url === 'yazda://') {
          link.url = 'yazda://adventures';
      }

        handleOpenURL(link.url);
    }
}
