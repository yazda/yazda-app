(function() {
    'use strict';

    angular.module('yazda.core')
        .constant('yazdaPlugin', window.yazdaPlugin);
})();
