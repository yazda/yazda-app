(function() {
    angular
        .module('yazda.core')
        .config(['$httpProvider', interceptor]);

    function interceptor($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    }
})();
