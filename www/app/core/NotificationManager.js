(function() {
    'use strict';
    angular
        .module('yazda.core')
        .service('NotificationManager', NotificationManager);

    NotificationManager.$inject = ['$ionicPlatform', '$cordovaToast', '$window'];

    function NotificationManager($ionicPlatform, $cordovaToast, $window) {
        var self = this;

        //ngCordova functions must not be called until the ionic platform is ready.
        $ionicPlatform.ready(function() {

            //If running on a computer, Cordova will not be active, so
            //the following block allows us to see toast messages in the console.
            if (!$window.plugins) {
                $cordovaToast.showShortBottom = function(message) {
                    console.log('DEBUG TOAST: ' + message);
                };
            }

            self.showError = function(err) {
                if (!err || (err.code !== 'AUTH' && !err.result)) {
                    showGeneralErrorToast();
                } else if (err.code === 'AUTH') {
                    showNotAuthorizedToast();
                } else {
                    switch (err.result.status) {
                        case 400:
                            showBadRequestToast('save');
                            break;
                        case 404:
                            showMissingDataToast();
                            break;
                        default:
                            showGeneralErrorToast();
                            break;
                    }
                }
            };

            // ----- Toasts ----- \\
            self.showInvalidFormToast = function() {
                $cordovaToast.showShortBottom('Invalid Information: ' +
                    'Please provide valid information for requested fields below.');
            };

            self.showSuccess = function(message) {
                $cordovaToast.showShortBottom(message);
            };

            self.showErrorCustom = function(message) {
                $cordovaToast.showShortBottom(message);
            };

            var showBadRequestToast = function(action) {
                $cordovaToast.showShortBottom('Error: An unexpected error occurred while ' + action +
                    ' your information. Please contact customer support.');
            };

            var showMissingDataToast = function() {
                $cordovaToast.showShortBottom('Error: ' +
                    'Required information was missing or not found. Please contact customer support.');
                //console.log('DEBUG TOAST: ' + message);
            };

            var showNotAuthorizedToast = function() {
                $cordovaToast.showShortBottom('Wrong username or password.' +
                    ' Please check your credentials and try again.');
            };

            var showGeneralErrorToast = function() {
                $cordovaToast.showShortBottom('Error: An unexpected error occurred. Please contact customer support.');
            };
        });
    }
})();
