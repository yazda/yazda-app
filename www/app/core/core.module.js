(function() {
    'use strict';

    angular.module('yazda.core', [
        'ionic',
        'blocks.exception',
        'blocks.logger',
        'blocks.router',
        'ui.router',
        'ngCordova',
        'angular.filter',
        'yazda.yazda-api',
        'monospaced.elastic'
    ]);
})();
