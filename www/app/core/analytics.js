(function() {
    angular
        .module('yazda.core')
        .factory('AnalyticsFactory', AnalyticsFactory);

    AnalyticsFactory.$inject = ['oauthConstants', '$cordovaGoogleAnalytics'];
    function AnalyticsFactory(oauthConstants, $cordovaGoogleAnalytics) {
        function init() {
            try {
                // Init GA
                $cordovaGoogleAnalytics.startTrackerWithId(oauthConstants.googleAnalyticsID);
            } catch(e) {
                console.log(e);
            }

            // Init Branch
            Branch.setDebug(true);
            Branch.initSession();
        }

        function track(category, action, obj) {
            var opts = obj || {};

            opts.category = category;

            try {
                Branch.userCompletedAction(action, opts);
                facebookConnectPlugin.logEvent(action, opts);
                $cordovaGoogleAnalytics.trackEvent(category, action);
            } catch(e) {
                console.log(e);
            }
        }

        function trackUserId(userId) {
            try {
                $cordovaGoogleAnalytics.setUserId(userId);
                Branch.setIdentity(String(userId));
            } catch(e) {
                console.log(e);
            }
        }

        function logout() {
            Branch.logout();
        }

        return {
            init:        init,
            track:       track,
            trackUserId: trackUserId,
            logout:      logout
        };
    }
})();
