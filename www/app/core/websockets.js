(function() {
    angular
        .module('yazda.websockets',
            ['yazda.core',
                'btford.socket-io'])
        .factory('WebSockets', WebSockets);

    WebSockets.$inject = ['oauthConstants', 'socketFactory', 'oauthClient'];
    function WebSockets(oauthConstants, socketFactory, oauthClient) {
        var socket;
        var channels = [];
        var connected;
        var cachedCalls = [];

        function connectToAdventure(id) {
            var obj = {adventureId: id};
            var channel = 'adventures:' + id;
            var pagedChannel = 'adventures:' + id + ':paged';

            socket.forward(channel);
            socket.forward(pagedChannel);

            socket.emit('join', obj);
            channels.push(obj);
        }

        function sendMessageToUsers(message) {
            var obj = {
                conversationId: message.conversationId,
                sentTo:         message.sentTo,
                msg:            message.msg
            };

            if(connected) {
                socket.emit('chat', obj);
            } else {
                //Push when we're connected again
                cachedCalls.push({'chat': obj});
            }
        }

        function sendMessageToAdventurers(adventureId, message) {
            var obj = {
                adventureId: adventureId,
                msg:         message
            };

            if(connected) {
                socket.emit('chat', obj);
            } else {
                //Push when we're connected again
                cachedCalls.push({'chat': obj});
            }
        }

        function getConversationMessages(conversationId) {
            var obj = {
                conversationId: conversationId,
                action:         'list'
            };

            if(connected) {
                socket.emit('conversations', obj);
            } else {
                //Push when we're connected again
                cachedCalls.push({'conversations': obj});
            }
        }

        function markConversationRead(conversationId) {
            var obj = {
                conversationId: conversationId,
                action:         'mark_read'
            };

            if(connected) {
                socket.emit('conversations', obj);
            } else {
                //Push when we're connected again
                cachedCalls.push({'conversations': obj});
            }
        }

        function markAdventureConvRead(adventureId) {
            var obj = {
                adventureId: adventureId,
                action:      'mark_read'
            };

            if(connected) {
                socket.emit('conversations', obj);
            } else {
                //Push when we're connected again
                cachedCalls.push({'conversations': obj});
            }
        }

        function disconnectFromAdventure(id) {
            var channel = 'adventures:' + id;
            var pagedChannel = 'adventures:' + id + ':paged';

            socket.removeListener(channel);
            socket.removeListener(pagedChannel);

            socket.emit('leave', {adventureId: id});
            console.log('Disconnecting from ' + channel);
            console.log('Disconnecting from ' + pagedChannel);

            channels = _.without(channels, _.findWhere(channels, {adventureId: id}));
        }

        function disconnect() {
            socket.disconnect();
        }

        function connect() {
            if(socket) {
                socket.connect();
            } else {
                socket = socketFactory({
                    ioSocket: io.connect(oauthConstants.websocketURL, {
                        reconnection:         true,
                        reconnectionDelay:    1000,
                        reconnectionDelayMax: 10000,
                        reconnectionAttempts: 10,
                        transports:           ['websocket', 'polling']
                    })
                });

                socket.forward('conversations');
                socket.forward('conversations:add');
                socket.forward('direct_chat');

                socket.on('connect', function() {
                    oauthClient.token()
                        .then(function(token) {
                            socket.emit('authentication', {token: token});

                            connected = true;

                            socket.on('ping', function() {
                                socket.emit('pong', {beat: 1});
                            });         
                        });
                });

                socket.on('connecting', function() {
                    console.log('connecting');
                });

                socket.on('disconnect', function() {
                    console.log('disconnect');
                    connected = false;
                });
                socket.on('connect_failed', function() {
                    console.log('connect_failed');
                });
                socket.on('error', function(err) {
                    console.log('error: ' + err);
                });
                socket.on('reconnect_failed', function() {
                    console.log('reconnect_failed');
                });
                socket.on('reconnect', function() {
                    console.log('reconnected');
                });
                socket.on('reconnecting', function() {
                    console.log('reconnecting');
                });
                socket.on('authenticated', function() {
                    _.each(channels, function(obj) {
                        socket.emit('join', obj);
                    });

                    while(cachedCalls.length !== 0) {
                        var call = cachedCalls.pop();
                        var channel = Object.keys(call)[0];

                        socket.emit(channel, call[channel]);
                    }
                });
            }
        }

        return {
            disconnect:               disconnect,
            connect:                  connect,
            connectToAdventure:       connectToAdventure,
            sendMessageToAdventurers: sendMessageToAdventurers,
            markAdventureConvRead:    markAdventureConvRead,
            disconnectFromAdventure:  disconnectFromAdventure,
            getConversationMessages:  getConversationMessages,
            markConversationRead:     markConversationRead,
            sendMessageToUsers:       sendMessageToUsers
        };
    }
})();
