(function() {
    angular
        .module('yazda.core')
        .factory('httpInterceptor', httpInterceptor);

    function httpInterceptor() {

        return {
            request: request,
            response: response
        };

        function request(config) {
            if(config.url.indexOf('yazda-server.dev') !== -1) {
                console.log('requesting: ' + config.url);
            }

            return config;
        }

        function response(resp) {
            if(resp.config.url.indexOf('yazda-server.dev') !== -1) {
                console.log('received: ' + resp.status + ': ' + resp.config.url);
            }

            if(resp.config.headers.Authorization) {
                var resp1 = resp.data;
                resp1.body = resp.data;

                return resp1;
            } else {
                return resp;
            }
        }
    }
})();
