(function() {
  'use strict';

  angular
    .module('yazda.features.loggedIn')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'loggedIn',
        config: {
          url: '',
          abstract: true,
          templateUrl: 'app/features/navViews/loggedIn/loggedIn.html',
          controller: 'LoggedInCtrl'
        }
      }
    ];
  }
})();
