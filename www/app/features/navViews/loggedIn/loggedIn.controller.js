(function() {
    'use strict';

    angular
        .module('yazda.features.loggedIn')
        .controller('LoggedInCtrl', LoggedInCtrl);

    /* @ngInject */
    function LoggedInCtrl($scope, $rootScope, $cordovaBadge, WebSockets, $ionicPlatform) {
        var vm = this;

        $rootScope.adventure_badge = null;
        $rootScope.friend_request_badge = null;

        $ionicPlatform.ready(activate);

        function activate() {
            $scope.onInvites = onInvites;
            $scope.onProfile = onProfile;
            
            WebSockets.connect();

            $rootScope.adventure_badge = window.localStorage.getItem('adventure_badge');
            $rootScope.friend_request_badge = window.localStorage.getItem('friend_request_badge');
        }

        function onProfile() {
            if(typeof cordova !== 'undefined') {
                $cordovaBadge.hasPermission()
                    .then(function() {
                        $cordovaBadge.get()
                            .then(function(badge) {
                                if(Number(badge) === Number($rootScope.friend_request_badge) || !badge) {
                                    $cordovaBadge.clear();
                                }
                                $rootScope.friend_request_badge = null;
                                window.localStorage.removeItem('friend_request_badge');
                            });
                    });
            } else {
                $rootScope.friend_request_badge = null;
                window.localStorage.removeItem('friend_request_badge');
            }
        }

        function onInvites() {
            if(typeof cordova !== 'undefined') {
                $cordovaBadge.hasPermission()
                    .then(function() {
                        $cordovaBadge.get()
                            .then(function(badge) {
                                if(Number(badge) === Number($rootScope.adventure_badge) || !badge) {
                                    $cordovaBadge.clear();
                                }
                                $rootScope.adventure_badge = null;
                                window.localStorage.removeItem('adventure_badge');
                            });
                    });
            } else {
                $rootScope.adventure_badge = null;
                window.localStorage.removeItem('adventure_badge');
            }
        }
    }
})();
