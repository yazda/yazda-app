/* jshint -W117, -W030 */
(function() {
    describe.skip('loggedIn routes', function() {
        describe('state', function() {
            var view = 'app/features/navViews/loggedIn/loggedIn.html';

            beforeEach(function() {
                module('yazda.features.loggedIn');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for loggedIn route', function() {
                it('should navigate to \'', function() {
                    expect($state.href('loggedIn'))
                        .to.equal('#');
                });

                it('should map /loggedIn route to loggedIn template', function() {
                    expect($state.get('loggedIn').templateUrl).to.equal(view);
                });

                it('login should work with $state.go', function() {
                    $state.go('loggedIn');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn');
                });
            });
        });
    });
})();
