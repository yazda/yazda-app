(function() {
    'use strict';

    angular.module('yazda.features.loggedIn', [
        'yazda.core',
        'ngCordova'
    ]);

})();
