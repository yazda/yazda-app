(function() {
    'use strict';

    describe('loggedIn Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.loggedIn', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$ionicHistory',
                '$rootScope',
                '$stateParams',
                '$state',
                '$cordovaBadge'
            );

            bard.mockService($stateParams, mocks.StateParamsMock());
            bard.mockService($state, mocks.StateMock($q));
            bard.mockService($cordovaBadge, mocks.CordovaBadgeMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('LoggedInCtrl', {$scope: $scope});

            $rootScope.$apply();

            window.localStorage.setItem('adventure_badge', '1000');
            window.localStorage.setItem('friend_request_badge', '1000');

            sinon.spy($cordovaBadge, 'hasPermission');
            sinon.spy($cordovaBadge, 'get');
        });

        it('Should open the LoggedInCtrl', function() {
            expect($scope).to.be.ok();
        });

        describe('Function activate', function() {
            it('Should set adventure_badge on the $rootScope', function() {
                expect($rootScope.adventure_badge).to.eq('1000');
            });
            it('Should set friend_request_badge on the $rootScope', function() {
                expect($rootScope.friend_request_badge).to.eq('1000');
            });
        });
        describe('Function onProfile', function() {
            describe('If badge === $rootScope.friend_request_badge', function() {
                beforeEach(function() {
                    $rootScope.friend_request_badge = '1000';
                    $scope.onProfile();
                    $rootScope.$apply();
                });
                it('Should call $cordovaBadge.clear ', function() {
                    $scope.onProfile();
                    $rootScope.$apply();
                    expect($cordovaBadge.clear.called).to.be.true();
                });
            });
            describe('If badge !== $rootSCope.friend_request_badge', function() {
                beforeEach(function() {
                    $rootScope.friend_request_badge = '1111';
                    $scope.onProfile();
                    $rootScope.$apply();
                });
                it('Should call $cordovaBadge.decrease', function() {
                    expect($cordovaBadge.decrease.calledWith(1111)).to.be.true();
                });
            });

            it('Should remove freind_request_badge regardless', function() {
                $scope.onProfile();
                $rootScope.$apply();
                console.log($rootScope.friend_request_badge);
                expect($rootScope.friend_request_badge).to.eq(null);
            });
        });

        describe('Function onInvites', function() {
            it('Should call $cordovaBadge.hasPermission', function() {
                $scope.onInvites();
                $rootScope.$apply();
                expect($cordovaBadge.hasPermission.called).to.be.true();
                expect($cordovaBadge.get.called).to.be.true();
            });
            describe('if badge === $rootScope.adventure_badge', function() {
               it('Should call $cordovaBadge.clear', function() {
                   $rootScope.adventure_badge = 1000;
                   $scope.onInvites();
                   $rootScope.$apply();
                   expect($cordovaBadge.clear.called).to.be.true();
               });
            });
            describe('if badge !== $rootScope.adventure_badge', function() {
                it('Should call $cordovaBadge.decrease', function() {
                    $rootScope.adventure_badge = 1111;//badge === 1000
                    $scope.onInvites();
                    $rootScope.$apply();
                    expect($cordovaBadge.decrease.calledWith(1111)).to.be.true();
                });
            });
        });
    });
})();
