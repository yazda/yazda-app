(function() {
    'use strict';

    angular.module('yazda.features.branch', [
            'yazda.core'
        ])
        .factory('BranchFactory', BranchFactory);

    function BranchFactory($ionicPlatform) {
        var universalObject;

        function createObj(id, title, description, image, isPrivate, meta) {
            return Branch.createBranchUniversalObject({
                canonicalIdentifier: id,
                title:               title,
                contentDescription:  description,
                contentImageUrl:     image,
                contentIndexingMode: isPrivate ? 'private' : 'public',
                contentMetadata:     meta
            }).then(function(obj) {
                universalObject = obj;
            }, function(err) {
                console.error(err);
            });
        }

        function registerView() {
            if(universalObject) {
                $ionicPlatform.ready(function() {
                    universalObject.registerView();
                    universalObject.listOnSpotlight();
                });
            }
        }

        function createLink(feature, channel) {
            return universalObject.generateShortUrl({
                feature: feature,
                channel: channel
            }, {});
        }

        function share(feature, channel) {
            return universalObject.showShareSheet({
                feature: feature,
                channel: channel
            }, {});
        }

        return {
            createObj:    createObj,
            registerView: registerView,
            createLink:   createLink,
            share:        share
        };
    }
})();
