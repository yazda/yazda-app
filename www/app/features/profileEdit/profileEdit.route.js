(function() {
  'use strict';

  angular
    .module('yazda.features.profileEdit')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'loggedIn.profile-edit',
        config: {
          url: '/profile/edit',
          views: {
            'profile-view': {
              templateUrl: 'app/features/profileEdit/profileEdit.html'
            }
          }
        }
      }
    ];
  }
})();
