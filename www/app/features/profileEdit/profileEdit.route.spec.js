/* jshint -W117, -W030 */
(function() {
    describe('profileEdit routes', function() {
        describe('state', function() {
            var view = 'app/features/profileEdit/profileEdit.html';

            beforeEach(function() {
                module('yazda.features.profileEdit');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for loggedIn.profile-edit route', function() {
                it('should navigate to /profile/edit', function() {
                    expect($state.href('loggedIn.profile-edit'))
                        .to.equal('/profile/edit');
                });

                it('should map loggedIn.profile-edit route to Profile Edit template', function() {
                    expect($state.get('loggedIn.profile-edit').templateUrl).to.equal(view);
                });

                it('loggedIn.profile-edit should work with $state.go', function() {
                    $state.go('loggedIn.profile-edit');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.profile-edit');
                });
            });
        });
    });
}());
