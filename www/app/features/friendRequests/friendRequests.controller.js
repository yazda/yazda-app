(function() {
    'use strict';

    angular
        .module('yazda.features.friendRequests')
        .controller('FriendRequestsCtrl', FriendRequestsCtrl);

    /* @ngInject */
    function FriendRequestsCtrl($scope, friendships, $ionicPlatform) {
        var vm = this;
        var DEFAULT_PAGE_SIZE_STEP = 25;

        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.data = [];
        vm.getFriends = getFriends;
        vm.refresh = refresh;

        $ionicPlatform.ready(activate);

        function refresh() {
            vm.currentPage = 1;
            vm.data = [];
            getFriends();
        }

        function loadNextPage() {
            vm.currentPage++;
            getFriends();
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function activate() {
            return getFriends();
        }

        function getFriends() {
            var config = {
                page: vm.currentPage
            };

            return friendships.incoming(config)
                .then(function(data) {
                    var now = moment();
                    var friends = data.friendships;

                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');

                    vm.page_count = data.page_count;

                    vm.data.push.apply(vm.data, friends);

                    return vm.data;
                });
        }
    }
})();
