(function() {
    'use strict';

  describe('FriendRequests Controller', function() {
    var vm,
      $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.friendRequests', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });

            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                'friendships'
            );

            bard.mockService(friendships, mocks.FriendShipsMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('FriendRequestsCtrl', {$scope: $scope});

            $rootScope.$apply();

            sinon.spy(friendships, 'incoming');
            sinon.spy($scope, '$broadcast');
        });

        it('should open FriendRequestsCtrl',function() {
            expect(vm).to.be.ok();
        });

        describe('Function activate', function() {
           it('Should call friendships.incoming', function() {
               expect(friendships.incoming.calledWith({page: 1}));
           })
            it('Should call $broadcast', function() {
                expect($scope.$broadcast.calledWith('scroll.refreshComplete'));
                expect($scope.$broadcast.calledWith('scroll.infiniteScrollComplete'));
                expect(vm.data).to.eql([
                    {user: 'one'},
                    {user: 'two'},
                    {user: 'three'}
                ]);
            });
        });

        describe('refresh()', function() {

            it('resets currentPage to 1', function() {
                vm.currentPage = 2;
                vm.refresh();
                expect(vm.currentPage).to.equal(1);
            });

            it('resets users to []', function() {
                vm.data = [1, 2, 3, 4, 5, 6];
                vm.refresh();
                expect(vm.data.length).to.eql(0);
            });

            it('Calls getFollowers -> followers.index', function() {
                vm.refresh();
                $rootScope.$apply();
                expect(friendships.incoming.calledWith({page: 1})).to.be.true();
            });
        });

        describe('For: moreData()', function() {

            it('Should: return false if: vm.currentPage < vm.page_count', function() {
                vm.currentPage = 3;
                vm.page_count = 2;

                vm.moreData();

                expect(vm.moreData()).to.be.false();
            });
            it('Should: return true if: vm.currentPage > vm.page_count', function() {
                vm.currentPage = 2;
                vm.page_count = 3;

                vm.moreData();

                expect(vm.moreData()).to.be.true();
            });
        });

        describe('For: loadNextPage()', function() {
            it('Should: increment currentPage', function() {
                vm.currentPage = 3;
                vm.loadNextPage();
                expect(vm.currentPage).to.eql(4);
            });
        });

        describe('For: getFriends()', function() {

            it('Should: call $scope.$broadcast && return 3 friends', function() {
                expect($scope.$broadcast.calledWith('scroll.refreshComplete'));
                expect($scope.$broadcast.calledWith('scroll.infiniteScrollComplete'));
                expect(vm.data).to.eql([
                    {user: 'one'},
                    {user: 'two'},
                    {user: 'three'}
                ]);
            });
        });
    });
})();
