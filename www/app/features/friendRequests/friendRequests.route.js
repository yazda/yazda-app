(function() {
  'use strict';

  angular
    .module('yazda.features.friendRequests')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'loggedIn.friend-requests',
        config: {
          url:   '/friend-requests/incoming',
          views: {
            'profile-view': {
              templateUrl: 'app/features/friendRequests/friendRequests.html',
              controller:  'FriendRequestsCtrl as friendship'
            }
          }
        }
      }
    ];
  }
})();
