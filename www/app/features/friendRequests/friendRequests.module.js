(function () {
    'use strict';

    angular.module('yazda.features.friendRequests', [
        'yazda.core'
    ]);

})();
