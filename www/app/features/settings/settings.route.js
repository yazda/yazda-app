(function() {
    'use strict';

    angular
        .module('yazda.features.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.settings',
                config: {
                    url:   '/settings',
                    views: {
                        'more-view': {
                            templateUrl: 'app/features/settings/settings.html',
                            controller:  'SettingsCtrl as settings'
                        }
                    }
                }
            }
        ];
    }
})();
