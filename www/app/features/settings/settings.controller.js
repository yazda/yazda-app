(function() {
    'use strict';

    angular
        .module('yazda.features.settings')
        .controller('SettingsCtrl', SettingsCtrl);

    SettingsCtrl.$inject = ['account', '$ionicHistory','$ionicPlatform'];

    function SettingsCtrl(account, $ionicHistory,$ionicPlatform) {
        var vm = this;
        vm.updateSettings = updateSettings;

        $ionicPlatform.ready(activate);

        function activate() {
            return getMe();
        }

        function updateSettings() {
            return account
                .updateSettings(vm.data)
                .then(function() {
                    $ionicHistory.clearCache().then(function() {
                        $ionicHistory.goBack();
                    });
                });
        }

        function getMe() {
            return account
                .me()
                .then(setUserData);
        }

        function setUserData(data) {
            var user = data.user;

            vm.data = user;

            return vm.data;
        }
    }
})();
