(function () {
    'use strict';

    angular.module('yazda.features.settings', [
        'yazda.core'
    ]);

})();
