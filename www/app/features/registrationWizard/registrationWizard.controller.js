(function() {
    'use strict';

    angular
        .module('yazda.features.wizard')
        .controller('WizardCtrl', WizardCtrl);

    WizardCtrl.$inject = ['$scope', '$cordovaCamera', '$cordovaActionSheet', 'account', 'users', 'friendships', '$cordovaContacts', 'AnalyticsFactory','$ionicPlatform'];

    function WizardCtrl($scope, $cordovaCamera, $cordovaActionSheet, account, users, friendships, $cordovaContacts, AnalyticsFactory,$ionicPlatform) {
        var vm = this;
        var friendRequests = [];
        var emailFriendRequests = [];

        vm.appUsers = [];
        vm.toggle = toggle;
        vm.interestedIn = interestedIn;
        vm.updateProfile = updateProfile;
        vm.disableTap = disableTap;
        vm.chooseWayToUploadPhoto = chooseWayToUploadPhoto;
        vm.inviteFacebookFriends = inviteFacebookFriends;
        vm.requestedFriend = requestedFriend;
        vm.requestFriend = requestFriend;
        vm.requestEmailFriend = requestEmailFriend;
        vm.emailPotentialFriends = emailPotentialFriends;
        vm.upload_icon = ionic.Platform.isIOS() ? 'ion-ios-cloud-upload' : 'ion-android-upload';

        $ionicPlatform.ready(activate);

        function emailPotentialFriends() {
            if(emailFriendRequests.length > 0) {
                users
                    .inviteFriendsWithEmail(emailFriendRequests)
                    .then(function() {
                        $scope.$broadcast('wizard:Next');
                    });
            } else {
                $scope.$broadcast('wizard:Next');
            }
        }

        function requestEmailFriend(email) {
            var hasFRQ = _.findWhere(friendRequests, email);
            var hasEFRQ = _.findWhere(emailFriendRequests, email);

            if(hasFRQ) {
                _.pull(friendRequests, email);
            } else {
                friendRequests.push(email);
            }

            if(hasFRQ) {
                _.pull(emailFriendRequests, email);
            } else {
                emailFriendRequests.push(email);
            }
        }

        function requestFriend(id) {
            friendships
                .create(id)
                .then(function() {
                    friendRequests.push(id);
                });
        }

        function requestedFriend(id) {
            return _.contains(friendRequests, id);
        }

        function updateProfile() {
            return account
                .updateSettings(vm.data)
                .then(function() {
                    $scope.$broadcast('wizard:Next');

                    if(typeof cordova !== 'undefined') {

                        $cordovaContacts
                            .find({fields: ['email', 'name', 'display']})
                            .then(function(data) {
                                vm.contacts = _.map(data,
                                    function(contact) {
                                        var photo = contact.photo ? contact.photo[0] : '';

                                        return {
                                            emails: _.pluck(contact.emails, 'value'),
                                            name:   (contact.displayName || contact.name.formatted),
                                            photo:  photo
                                        };
                                    });

                                vm.emails = _.flatten(_.pluck(vm.contacts, 'emails'));
                            })
                            .finally(function() {
                                users
                                    .findFriendWithEmail(vm.emails)
                                    .then(setFriendsFromEmail);
                            });
                    } else {
                        users
                            .findFriendWithEmail(vm.emails)
                            .then(setFriendsFromEmail);
                    }

                    users.findFriendsWithStrava()
                        .then(function(data) {
                            vm.appUsers.push.apply(vm.appUsers, data.users);
                        });

                    users.findFriendsWithFacebook()
                        .then(function(data) {
                            vm.appUsers.push.apply(vm.appUsers, data.users);
                        });
                });
        }

        function setFriendsFromEmail(data) {
            var foundEmails = _.pluck(data.users, 'email');

            vm.appUsers.push.apply(vm.appUsers, data.users);
            vm.inviteToAppUsers = _.reject(vm.contacts, function(user) {
                var intersectedEmails = _.intersection(user.emails, foundEmails);
                var show = (intersectedEmails.length && (user.emails.length > 0));

                return show;
            });
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');

            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');

            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('places-wizard').blur();
            });
        }

        function toggle(type) {
            vm.data[type] = !vm.data[type];
        }

        function interestedIn(type) {
            if(!vm.data) {
                return '';
            }

            if(ionic.Platform.isIOS()) {
                return vm.data[type] ? 'ion-ios-checkmark' : 'ion-ios-circle-outline';
            } else {
                return vm.data[type] ? 'ion-android-checkbox' : 'ion-android-checkbox-outline-blank';
            }
        }

        function chooseWayToUploadPhoto() {
            var config = {
                quality:            100,
                destinationType:    Camera.DestinationType.DATA_URL,
                allowEdit:          true,
                encodingType:       Camera.EncodingType.PNG,
                targetWidth:        275,
                targetHeight:       275,
                popoverOptions:     CameraPopoverOptions,
                saveToPhotoAlbum:   true,
                correctOrientation: true
            };

            $cordovaActionSheet.show({
                    title:                     'How do you want to pick your picture',
                    buttonLabels:              ['Camera', 'Photo Library'],
                    addCancelButtonWithLabel:  'Cancel',
                    androidEnableCancelButton: true
                })
                .then(function(index) {
                    console.log('index:' + index);
                    if(index === 1) {
                        config.sourceType = Camera.PictureSourceType.CAMERA;
                    } else if(index === 2) {
                        config.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
                    }

                    if(index !== 3) {
                        $cordovaCamera.getPicture(config)
                            .then(function(data) {
                                account.updateProfileImage(data)
                                    .then(function() {
                                        $scope.$broadcast('wizard:Next');
                                    });
                            });
                    }
                });
        }

        function inviteFacebookFriends() {
            var options = {
                url:     'https://bnc.lt/mobile-download-yazda',
                // TODO better sharing image
                picture: "https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png"
            };

            facebookConnectPlugin
                .appInvite(options, function(success) {
                    if(success.completionGesture !== 'cancel') {
                        AnalyticsFactory
                            .track('social-growth', 'facebook-invite');
                    }
                }, function(error) {
                });

        }

        function activate() {
            return getMe();
        }

        function getMe() {
            return account
                .me(true)
                .then(setUserData);
        }

        function setUserData(data) {
            var user = data.user;

            vm.data = user;

            window.localStorage.setItem('current_user_id', user.id);
            AnalyticsFactory.trackUserId(Number(user.id));

            return vm.data;
        }
    }
})();
