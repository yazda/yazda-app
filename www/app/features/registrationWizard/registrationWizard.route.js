(function() {
    'use strict';

    angular
        .module('yazda.features.wizard')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state:  'wizard',
                config: {
                    url:         '/wizard',
                    templateUrl: 'app/features/registrationWizard/registrationWizard.html',
                    controller: 'WizardCtrl as ctrl'
                }
            }
        ];
    }
})();
