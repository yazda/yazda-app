(function () {
    'use strict';

    angular.module('yazda.features.wizard', [
        'yazda.core',
        'ionic.wizard'
    ]);

})();
