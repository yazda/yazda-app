/* jshint -W117, -W030 */
(function() {
    describe('register routes', function() {
        describe('state', function() {
            var view = 'app/features/register/register.html';

            beforeEach(function() {
                module('yazda.features.register');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for register route', function() {
                it('should navigate to /register', function() {
                    expect($state.href('register'))
                        .to.equal('#/register');
                });

                it('should map register route to Register template', function() {
                    expect($state.get('register').templateUrl).to.equal(view);
                });

                it('register should work with $state.go', function() {
                    $state.go('register');

                    $rootScope.$apply();

                    expect($state.current).to.be('register');
                });
            });
        });
    });
}());
