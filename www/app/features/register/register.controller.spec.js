(function() {
    'use strict';

    describe.skip('Controller: Register Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.register', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                '$cordovaGeolocation',
                '$ionicPlatform',
                'adventures',
                'oauth'
            );

            bard.mockService(oauth, mocks.OauthClientMock($q));
            bard.mockService($cordovaGeolocation, mocks.CordovaGeoLocationMock($q));
            bard.mockService($ionicPlatform, mocks.IonicPlatformMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('RegisterCtrl', {$scope: $scope});
            $rootScope.$apply();
        });

        describe('For: RegisterCtrl()', function() {

            it('Opens the RegisterCtrl', function() {
                expect(vm).to.be.ok();
            });
        });

        describe('For: register()', function() {

            it('Should: call register(user)', function() {
               var oauth = {};
                vm.register();
                expect(oauth.register).to.be.calledWith(users);
            });
        });

    });
})();
