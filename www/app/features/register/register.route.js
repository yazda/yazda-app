(function() {
  'use strict';

  angular
    .module('yazda.features.register')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'register',
        config: {
          url: '/register',
          templateUrl: 'app/features/register/register.html'
        }
      }
    ];
  }
})();
