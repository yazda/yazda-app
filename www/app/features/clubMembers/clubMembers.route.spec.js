/* jshint -W117, -W030 */
(function() {
    describe('friends routes', function() {
        describe('state', function() {
            var view = 'app/features/friends/friends.html';

            beforeEach(function() {
                module('yazda.features.friends', 'yazda.yazda-api');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
                $rootScope.$apply();
            });

            bard.verifyNoOutstandingHttpRequests();

            describe('for friends.route', function() {
                it('should navigate to ', function() {
                    expect($state.href('', {}))
                        .to.equal('');
                });

                it('should map /adventures route to  template', function() {
                    expect($state.get('').templateUrl).to.equal(view);
                });

                it(' should work with $state.go', function() {
                    $state.go('');

                    expect($state.is('')).to.be.true();
                });

            });
        });
    });
}());
