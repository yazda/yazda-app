(function() {
    'use strict';

    angular
        .module('yazda.features.clubMembers')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        function sharedConfig(viewName) {
            var returnObject = {url: '/users/:id/friends'};

            returnObject.views = {};
            returnObject.views[viewName] = {
                templateUrl: 'app/features/clubMembers/clubMembers.html',
                controller:  'ClubMembersCtrl as users'
            };

            return returnObject;
        }

        return [
            {
                state:  'loggedIn.profile-club-members-index',
                config: sharedConfig('profile-view')
            },
            {
                state:  'loggedIn.adventure-club-members-index',
                config: sharedConfig('adventure-view')
            },
            {
                state:  'loggedIn.club-club-members-index',
                config: sharedConfig('club-view')
            },
            {
                state:  'loggedIn.invite-club-members-index',
                config: sharedConfig('invite-view')
            }
        ];
    }
})();
