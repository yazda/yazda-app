(function() {
    'use strict';

    angular
        .module('yazda.features.clubMembers')
        .controller('ClubMembersCtrl', ClubMembersCtrl);

    ClubMembersCtrl.$inject = ['$scope', 'clubs', '$stateParams', '$ionicHistory', '$state', '$ionicListDelegate'];
    function ClubMembersCtrl($scope, clubs, $stateParams, $ionicHistory, $state, $ionicListDelegate) {
        var DEFAULT_PAGE_SIZE_STEP = 25;
        var vm = this;

        vm.query = '';
        vm.getFriends = getFriends;
        vm.users = [];
        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.refresh = refresh;
        vm.viewUser = viewUser;
        vm.clear = clear;

        $scope.setRole = setRole;
        $scope.canChangeRole = canChangeRole;

        $ionicPlatform.ready(activate);

        function activate() {
            getClub();

            $scope.$watch(function() {
                return vm.query;
            }, function(value) {
                vm.currentPage = 1;
                vm.users = [];

                getFriends();
            });
        }

        function canChangeRole(user, role) {
            return vm.data.role === 'admin' && user.role !== role;
        }

        function setRole(user, role) {
            clubs
                .updateMember($stateParams.id,
                    {user_id: user.id, role: role})
                .then(function() {
                    var updateUser = _.find(vm.users, function(obj) {
                        return obj.id === user.id;
                    });

                    updateUser.role = role;
                    $ionicListDelegate.closeOptionButtons();
                });
        }

        function clear() {
            vm.currentPage = 1;
            vm.users = [];
            vm.query = '';
        }

        function viewUser(id) {

            if($ionicHistory.currentStateName() === 'loggedIn.adventure-club-members-index') {
                $state.go('loggedIn.adventure-users-show', {id: id});

            } else if($ionicHistory.currentStateName() === 'loggedIn.profile-club-members-index') {
                $state.go('loggedIn.profile-users-show', {id: id});

            } else if($ionicHistory.currentStateName() === 'loggedIn.invite-club-members-index') {
                $state.go('loggedIn.invite-users-show', {id: id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.club-club-members-index') {
                $state.go('loggedIn.club-users-show', {id: id});
            }
        }

        function refresh() {
            vm.currentPage = 1;
            vm.users = [];

            getFriends();
        }

        function moreData() {
            return vm.page_count >= vm.currentPage;
        }

        function loadNextPage() {
            vm.currentPage++;
            getFriends();
        }

        function getFriends() {
            return clubs.members($stateParams.id, {
                    page: vm.currentPage,
                    q:    vm.query
                })
                .then(setFriendData)
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }

        function setFriendData(data) {
            var user = data.users;

            vm.users.push.apply(vm.users, user);
            vm.page_count = data.page_count;

            return vm.users;
        }

        function getClub() {
            return clubs
                .show($stateParams.id)
                .then(setClubData);
        }

        function setClubData(data) {
            var club = data.club;

            vm.data = club;

            BranchFactory.createObj('club_' + club.id,
                club.name,
                club.description,
                club.avatar_url,
                false, {
                    'url':             'yazda://clubs/' + club.id,
                    '$og_title':       club.name,
                    '$og_description': club.description,
                    '$og_image_url':   club.avatar_url
                }).then(function() {
                BranchFactory.registerView();
            });

            return vm.data;
        }
    }
})();
