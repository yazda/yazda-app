(function() {
  'use strict';

  angular
    .module('yazda.features.forgotPassword')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'forgot-password',
        config: {
          url: '/reset_password?reset_password_token',
          templateUrl: 'app/features/forgotPassword/forgotPassword.html'
        }
      }
    ];
  }
})();
