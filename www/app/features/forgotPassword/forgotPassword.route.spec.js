/* jshint -W117, -W030 */
(function() {
    describe('forgotPassword routes', function() {
        describe('state', function() {
            var view = 'app/features/forgotPassword/forgotPassword.html';

            beforeEach(function() {
                module('yazda.features.forgotPassword', 'yazda.yazda-api');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for forgot-password route', function() {
                it('should navigate to /forgotPassword', function() {
                    expect($state.href('forgot-password')).to.equal('#/forgotPassword');
                });

                it('should map /forgotPassword route to Forgot Password template', function() {
                    expect($state.get('forgot-password').templateUrl).to.equal(view);

                });

                it('loggedIn.adventures-index should work with $state.go', function() {
                    $state.go('forgot-password');

                    $rootScope.$apply();

                    expect($state.is('forgot-password')).to.be.true();
                });
            });
        });
    });
}());
