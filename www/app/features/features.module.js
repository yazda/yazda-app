(function () {
    'use strict';

    angular.module('yazda.features', [
        'yazda.features.adventuresEdit',
        'yazda.features.adventuresIndex',
        'yazda.features.adventuresNew',
        'yazda.features.adventuresShow',
        'yazda.features.branch',
        'yazda.features.clubsEdit',
        'yazda.features.clubsIndex',
        'yazda.features.clubMembers',
        'yazda.features.clubsCreate',
        'yazda.features.clubsShow',
        'yazda.features.forgotPassword',
        'yazda.features.friendRequests',
        'yazda.features.friends',
        'yazda.features.inviteFollowersIndex',
        'yazda.features.invitesIndex',
        'yazda.features.login',
        'yazda.features.moreIndex',
        'yazda.features.loggedIn',
        'yazda.features.profileEdit',
        'yazda.features.profileUsersShow',
        'yazda.features.register',
        'yazda.features.settings',
        'yazda.features.usersSearch',
        'yazda.features.usersShow',
        'yazda.features.wizard'
    ]);

})();
