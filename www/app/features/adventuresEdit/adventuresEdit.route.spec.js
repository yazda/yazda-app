/*
 /!* jshint -W117, -W030 *!/
 (function() {
 describe('adventuresEdit routes', function() {
 describe('state', function() {
 var view = 'app/features/adventuresEdit/adventuresEdit.html';

 beforeEach(function() {
 module('yazda.features.adventuresEdit', function($provide, $urlRouterProvider){
 $provide.value('$ionicTemplateCache', function(){} );
 $urlRouterProvider.deferIntercept();
 });
 bard.inject(
 '$state',
 '$templateCache',
 '$rootScope',
 '$q'
 );
 });

 bard.verifyNoOutstandingHttpRequests();

 describe('for adventuresEdit.route', function() {
 it('should navigate to /adventures/new', function() {
 expect($state.href('adventuresEdit')).to.equal('/adventures/new');
 });
 it('should map /adventuresEdit route to Adventures New View template', function() {
 expect($state.get('adventuresEdit').templateUrl).to.equal(view);
 });
 it('of adventuresEdit should work with $state.go', function() {
 $state.go('adventuresEdit');
 $rootScope.$apply();
 expect($state.is('adventuresEdit')).to.be.true();
 });
 });
 });
 });
 }());
 */
