(function () {
    'use strict';

    angular.module('yazda.features.adventuresEdit', [
        'yazda.core',
        'ngMap'
    ]);
})();
