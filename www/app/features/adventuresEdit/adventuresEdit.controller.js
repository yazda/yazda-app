(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresEdit')
        .controller('AdventureEditCtrl', AdventureEditCtrl);

    AdventureEditCtrl.$inject = ['$scope', 'adventures', '$stateParams', '$ionicHistory', 'AnalyticsFactory'];
    function AdventureEditCtrl($scope, adventures, $stateParams, $ionicHistory,
                               AnalyticsFactory, $ionicPlatform) {
        $scope.updateAdventure = updateAdventure;

        function updateAdventure() {
            var adventure = _.clone($scope.data, true);

            adventure.private = !adventure.public;

            delete adventure.attendings;
            delete adventure.created_at;
            delete adventure.created_at_epoch;
            delete adventure.updated_at;
            delete adventure.updated_at_epoch;
            delete adventure.public;
            delete adventure.has_responded;
            delete adventure.invite;
            delete adventure.is_attending;
            delete adventure.is_invited;
            delete adventure.is_owner;
            delete adventure.owner;
            delete adventure.pendings;
            delete adventure.map;

            return adventures.update(adventure.id, adventure)
                .then(function() {
                    AnalyticsFactory
                        .track('adventures', 'update-adventure', adventure);

                    $ionicHistory.clearCache().then(function() {
                        $ionicHistory.goBack();
                    });
                });
        }

        $ionicPlatform.ready(activate);

        function activate() {
            return getAdventure();
        }

        function getAdventure() {
            console.log($stateParams.id);
            return adventures.show($stateParams.id).then(function(data) {
                var adventure = data.adventure;

                adventure.public = !adventure.private;

                $scope.data = adventure;
                $scope.data.start_time = new Date(adventure.start_time);
                $scope.data.end_time = new Date(adventure.end_time);
                $scope.title = adventure.name;

                return $scope.data;
            });
        }
    }
})();
