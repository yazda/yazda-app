(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresEdit')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state:  'loggedIn.adventures-edit',
                config: {
                    url:         '/adventures/:id/edit',
                    views:       {
                        'adventure-view': {
                            templateUrl: 'app/features/adventuresEdit/adventuresEdit.html',
                            controller: 'AdventureEditCtrl'
                        }
                    },
                    isProtected: true
                }
            }
        ];
    }
})();
