(function() {
  'use strict';

  angular
    .module('yazda.features.moreIndex')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'loggedIn.more-index',
        config: {
          url:   '/more',
          views: {
            'more-view': {
              templateUrl: 'app/features/moreIndex/moreIndex.html',
              controller:  'MoreCtrl as more'
            }
          }
        }
      }
    ];
  }
})();
