(function() {
  'use strict';

  describe('More Controller', function() {
    var vm,
      $scope;

    beforeEach(function() {
      bard.appModule('yazda.features.moreIndex', function($provide, $urlRouterProvider){
          $provide.value('$ionicTemplateCache', function(){} );
          $urlRouterProvider.deferIntercept();
      });
      bard.inject(
        '$q',
        '$controller',
        '$rootScope',
        '$httpBackend'
      );
    });

    beforeEach(function() {
      $scope = $rootScope.$new();

      vm = $controller('MoreCtrl', {$scope: $scope});

      $rootScope.$apply();
    });

    it('should',function() {
      console.log(vm);
    });
  });
})();
