(function() {
    'use strict';

    angular
        .module('yazda.features.moreIndex')
        .controller('MoreCtrl', MoreCtrl);

    MoreCtrl.$inject = ['$cordovaOauth', 'oauthConstants', 'oauthClient', 'account', '$ionicLoading', 'AnalyticsFactory', '$ionicPlatform'];

    function MoreCtrl($cordovaOauth, oauthConstants, oauthClient, account, $ionicLoading, AnalyticsFactory, $ionicPlatform) {
        var vm = this;

        vm.openTwitter = openTwitter;
        vm.openFacebook = openFacebook;
        vm.openYoutube = openYoutube;
        vm.openTrello = openTrello;
        vm.facebook = facebook;
        vm.strava = strava;
        vm.inviteFacebookFriends = inviteFacebookFriends;

        $ionicPlatform.ready(activate);

        function inviteFacebookFriends() {
            $ionicLoading.show({
                template: '<ion-spinner class="spinner-energized"></ion-spinner>',
                delay:    150
            });

            branch.link({
                'tags':    ['social-share'],
                'channel': 'mobile',
                'feature': 'facebook-invite',
                'data':    {
                    '$og_title':     'Discover Adventure on Yazda',
                    '$og_image_url': 'https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png'
                }
            }, function(err, link) {
                $ionicLoading.hide();

                if(!err) {
                    var options = {
                        url:     link,
                        picture: "https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png"
                    };

                    facebookConnectPlugin
                        .appInvite(options, function(success) {
                            if(success.completionGesture !== 'cancel') {
                                AnalyticsFactory
                                    .track('social-growth', 'facebook-invite');
                            }
                        }, function(error) {
                        });
                }
            });

        }

        function openYoutube() {
            openApp('youtube://www.youtube.com/channel/UCcuKd0jf72qlkiC_Jg-Z2wQ',
                'https://www.youtube.com/channel/UCcuKd0jf72qlkiC_Jg-Z2wQ');
        }

        function openTwitter() {
            openApp("twitter://user?screen_name=yazda_app", 'https://twitter.com/yazda_app');
        }

        function openFacebook() {
            openApp("fb://profile/1532609843695322", 'https://www.facebook.com/yazdaapp');
        }

        function openTrello() {
            openApp("trello://x-callback-url/showBoard?x-source=Yazda&shortlink=81QRDHnt",
                'https://trello.com/b/o8zS9Dby');
        }

        function facebook() {
            $cordovaOauth
                .facebook(oauthConstants.facebookAppId, ['email', 'public_profile', 'user_friends'])
                .then(function(result) {
                    oauthClient
                        .loginWithFacebook(result);
                });
        }

        function strava() {
            $cordovaOauth
                .strava(oauthConstants.stravaClientId, oauthConstants.stravaClientSecret, ['public'])
                .then(function(result) {
                    oauthClient
                        .loginWithStrava(result);
                });
        }

        function activate() {
            return getMe();
        }

        function getMe() {
            return account
                .me(true)
                .then(setUserData);
        }

        function setUserData(data) {
            var user = data.user;

            vm.data = user;

            return vm.data;
        }

        function openApp(url, httpUrl) {
            navigator
                .startApp
                .check(url, function() {
                        navigator
                            .startApp
                            .start(url);
                    },
                    function() {
                        window.open(httpUrl, '_system', 'location=yes');
                    });
        }
    }
})();
