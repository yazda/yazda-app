/* jshint -W117, -W030 */
(function() {
    describe('moreIndex routes', function() {
        describe('state', function() {
            var view = 'app/features/moreIndex/moreIndex.html';

            beforeEach(function() {
                module('yazda.features.moreIndex');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for moreIndex route', function() {
                it('should navigate to /more', function() {
                    expect($state.href('loggedIn.more-index'))
                        .to.equal('/more');
                });

                it('should map /more route to moreIndex template', function() {
                    expect($state.get('loggedIn.more-index').templateUrl).to.equal(view);
                });

                it('loggedIn.more-index should work with $state.go', function() {
                    $state.go('loggedIn.more-index');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.more-index');
                });
            });
        });
    });
}());
