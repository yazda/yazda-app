(function() {
  'use strict';

  describe('Invites Controller', function() {
    var vm,
      $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.invitesIndex', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                'invites'
            );

            bard.mockService(invites, mocks.InvitesMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('InvitesCtrl', {$scope: $scope});

            $rootScope.$apply();

            sinon.spy($scope, '$broadcast');
            sinon.spy(invites, 'pending');
        });

        it('should open Invites Index',function() {
            expect(vm).to.be.ok();
            expect(vm.currentPage).to.eq(1);
        });

        describe('function refresh', function () {
            it('resets currentPage to 1, and empties data[]', function () {
                vm.currentPage = 5;
                vm.data = [1, 2, 3];

                vm.refresh();

                expect(vm.currentPage).to.eq(1);
                expect(vm.data).to.eql([]);
            });
        });

        describe('function loadNextPage', function () {
            it('increments currentPage', function () {
                expect(vm.currentPage).to.eq(1);

                vm.loadNextPage();

                expect(vm.currentPage).to.eq(2);
            });
        });

        describe('function moreData()', function () {
            it('returns true if vm.currentPage < vm.page_count', function () {
                vm.currentPage = 1;
                vm.page_count = 2;

                vm.moreData();

                expect(vm.moreData()).to.be.true();
            });

            it('returns false if vm.currentPage > vm.page_count', function () {
                vm.currentPage = 3;
                vm.page_count = 2;

                vm.moreData();

                expect(vm.moreData()).to.be.false();
            });
        });

        describe('function getInvites()', function () {
            beforeEach(function() {
                vm.data = [];
                vm.getInvites();
                $rootScope.$apply();
            });

            it('calls invites.pending', function () {
                expect(invites.pending.called).to.be.true();
            });

            it('Should set values for: vm.page_count', function () {
                expect(vm.page_count).to.eq(5);
            });

            it('Should return data', function() {
                expect(vm.data).to.eql([
                    {id: 1, name: 'inv 1'},
                    {id: 2, name: 'inv 2'},
                    {id: 3, name: 'inv 3'}
                ]);
            });

            it('Should $broadcast', function() {
                expect($scope.$broadcast.calledWith('scroll.refreshComplete')).to.be.true();
                expect($scope.$broadcast.calledWith('scroll.infiniteScrollComplete')).to.be.true();
            });
        });

    });
})();
