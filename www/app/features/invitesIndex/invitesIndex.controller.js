(function() {
    'use strict';

    angular
        .module('yazda.features.invitesIndex')
        .controller('InvitesCtrl', InvitesCtrl);

    InvitesCtrl.$inject = ['$scope', 'invites', '$ionicPlatform'];
    function InvitesCtrl($scope, invites, $ionicPlatform) {
        var vm = this;
        var DEFAULT_PAGE_SIZE_STEP = 25;

        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.data = [];
        vm.getInvites = getInvites;
        vm.refresh = refresh;

        $ionicPlatform.ready(activate);

        function refresh() {
            vm.currentPage = 1;
            vm.data = [];
            getInvites();
        }

        function loadNextPage() {
            vm.currentPage++;
            getInvites();
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function activate() {
            return getInvites();
        }

        function getInvites() {
            var config = {
                page: vm.currentPage
            };

            return invites.pending(config)
                .then(function(data) {
                    var now = moment();
                    var invites = data.invites;

                    vm.page_count = data.page_count;
                    vm.data.push.apply(vm.data, invites);

                    return vm.data;
                })
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }
    }
})();
