/* jshint -W117, -W030 */
(function() {
    describe('invitesIndex routes', function() {
        describe('state', function() {
            var view = 'app/features/invitesIndex/invitesIndex.html';

            beforeEach(function() {
                module('yazda.features.invitesIndex', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                    $provide.value('$ionicTemplateCache', function(){} );
                    $urlRouterProvider.deferIntercept();
                });
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for invites-index route', function() {
                it('should navigate to /invites', function() {
                    expect($state.href('loggedIn.invites-index'))
                        .to.equal('/invites');
                });

                it('should map loggedIn.invites-index route to Invites template', function() {
                    expect($state.get('loggedIn.invites-index').templateUrl).to.equal(view);
                });

                it('loggedIn.invites-index should work with $state.go', function() {
                    $state.go('loggedIn.invites-index');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.invites-index');
                });
            });
        });
    });
}());
