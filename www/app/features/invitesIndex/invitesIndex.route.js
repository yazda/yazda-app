(function() {
    'use strict';

    angular
        .module('yazda.features.invitesIndex')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.invites-index',
                config: {
                    url:   '/invites',
                    views: {
                        'invite-view': {
                            templateUrl: 'app/features/invitesIndex/invitesIndex.html',
                            controller:  'InvitesCtrl as invites'
                        }
                    }
                }
            }
        ];
    }
})();
