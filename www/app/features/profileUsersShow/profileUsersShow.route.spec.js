/* jshint -W117, -W030 */
(function() {
    describe('profileUsersShow routes', function() {
        describe('state', function() {
            var view = 'app/features/profileShow/profileUsersShow.html';

            beforeEach(function() {
                module('yazda.features.profileUsersShow');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for loggedIn.profile-show route', function() {
                it('should navigate to /profile', function() {
                    expect($state.href('loggedIn.profile-show')).to.eq('/profile');
                });

                it('should map loggedIn.profile-show route to Profile Show template', function() {
                    expect($state.get('loggedIn.profile-show').templateUrl).to.eq(view);
                });

                it('loggedIn.profile-show should work with $state.go', function() {
                    $state.go('loggedIn.profile-show');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.profile-show');
                });
            });
        });
    });
}());
