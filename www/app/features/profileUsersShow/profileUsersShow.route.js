(function() {
    'use strict';

    angular
        .module('yazda.features.profileUsersShow')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.profile-show',
                config: {
                    url: '/profile',
                    views: {
                        'profile-view': {
                            templateUrl: 'app/features/profileUsersShow/profileUsersShow.html'
                        }
                    }
                }
            }
        ];
    }
})();
