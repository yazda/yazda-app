(function () {
    'use strict';

    angular.module('yazda.features.clubsCreate', [
        'yazda.core',
        'ngMap'
    ]);
})();
