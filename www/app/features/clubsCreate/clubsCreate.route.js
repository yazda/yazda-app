(function() {
    'use strict';

    angular
        .module('yazda.features.clubsCreate')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state:  'loggedIn.clubs-new',
                config: {
                    url:   '/clubs/new',
                    views: {
                        'club-view': {
                            templateUrl: 'app/features/clubsCreate/clubsCreate.html'
                        }
                    }

                }
            }
        ];
    }
})();
