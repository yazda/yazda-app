(function () {
    'use strict';

    angular.module('yazda.features.adventuresNew', [
        'yazda.core',
        'ngMessages',
        'ngMap'
    ]);
})();
