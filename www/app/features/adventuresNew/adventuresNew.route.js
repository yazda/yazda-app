(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresNew')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.adventures-new',
                config: {
                    url:   '/adventures/new',
                    views: {
                        'adventure-view': {
                            templateUrl: 'app/features/adventuresNew/adventuresNew.html',
                            controller: 'AdventureNewCtrl as adventure'
                        }
                    },
                    isProtected: true
                }
            }
        ];
    }
})();
