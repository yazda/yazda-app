(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresNew')
        .controller('AdventureNewCtrl', AdventureNewCtrl);

    AdventureNewCtrl.$inject = ['$scope', 'friends', 'adventures', 'GeoCoder', '$ionicHistory', '$cordovaGeolocation', 'AnalyticsFactory', '$cordovaSocialSharing', '$state',
        'account', 'BranchFactory','$ionicPlatform'];
    function AdventureNewCtrl($scope, friends, adventures, GeoCoder, $ionicHistory,
                              $cordovaGeolocation, AnalyticsFactory, $cordovaSocialSharing, $state, account, BranchFactory, $ionicPlatform) {
        var vm = this;
        var shareLink;
        var steps = ['choose_type', 'location', 'basic', 'invite_users', 'success'];

        vm.public = true;
        vm.users = [];
        vm.displayMarker = false;
        vm.createAdventure = createAdventure;
        vm.placeChanged = placeChanged;
        vm.$scope = $scope;
        vm.map_lat = null;
        vm.map_lon = null;
        vm.disableTap = disableTap;
        vm.setMarker = setMarker;
        vm.chooseAdventureType = chooseAdventureType;
        vm.adventureTypeIcon = adventureTypeIcon;
        vm.foundUsers = [];
        vm.clear = clear;

        $scope.hasNextStep = hasNextStep;
        $scope.hasPreviousStep = hasPreviousStep;
        $scope.nextStep = nextStep;
        $scope.previousStep = previousStep;
        $scope.$on('mapInitialized', mapInit);
        $scope.shareViaFB = shareViaFB;
        $scope.shareViaTwitter = shareViaTwitter;
        $scope.shareViaEmail = shareViaEmail;
        $scope.shareViaSMS = shareViaSMS;
        $scope.currentPage = 0;
        $scope.getUsers = getUsers;
        $scope.addToUsers = addToUsers;
        $scope.moreData = moreData;
        $scope.loadNextPage = loadNextPage;
        $scope.navigateBackToAdventures = navigateBackToAdventures;
        $scope.checked = checked;
        $scope.step = 'choose_type';
        $scope.background = background;
        $scope.clubs = [];

        $scope.$watch(function() {
            return vm.query;
        }, function(value) {
            vm.foundUsers = [];
            getUsers();
        });

        $ionicPlatform.ready(activate);

        function background() {
            if(vm.adventure_type) {
                return 'background-image: ' +
                    'url(img/new-adventure-background/' + vm.adventure_type + '.jpg)';
            }
        }

        function nextStep() {
            $scope.step = steps[steps.indexOf($scope.step) + 1];
            if($scope.step === 'invite_users') {
                clear();
            }
        }

        function previousStep() {
            $scope.step = steps[steps.indexOf($scope.step) - 1];
        }

        function hasPreviousStep() {
            return !($scope.step === 'choose_type' || $scope.step === 'success');
        }

        function hasNextStep() {
            return !($scope.step === 'choose_type' || $scope.step === 'invite_users' || $scope.step === 'success');
        }

        function clear() {
            $scope.currentPage = 1;
            vm.foundUsers = [];
            vm.query = '';
        }

        function getMe() {
            return account
                .me()
                .then(setUserData);
        }

        function setUserData(data) {
            var user = data.user;

            $scope.me = user;
            $scope.clubs = _.reject($scope.me.clubs, { role: 'leader'});

            return $scope.me;
        }

        function getUsers() {
            var id = window.localStorage.getItem('current_user_id');

            return friends
                .index(id,
                    {
                        page:           $scope.currentPage,
                        adventure_type: vm.adventure_type,
                        q:              vm.query
                    })
                .then(function(response) {
                    $scope.page_count = response.page_count;
                    vm.foundUsers.push.apply(vm.foundUsers, response.users);
                })
                .finally(function() {
                    $scope
                        .$broadcast('scroll.refreshComplete');
                    $scope
                        .$broadcast('scroll.infiniteScrollComplete');
                });
        }

        function moreData() {
            return $scope.currentPage < $scope.page_count;
        }

        function loadNextPage() {
            $scope.currentPage++;
            getUsers();
        }

        function checked(user) {
            if(!user) {
                return false;
            }

            var index = _.findIndex(vm.users, {id: user.id});

            if(index === -1) {
                return false;
            } else {
                return true;
            }
        }

        function addToUsers(user) {
            var index = _.findIndex(vm.users, {id: user.id});

            if(!checked(user)) {
                vm.users.push(user);
            } else {
                _.pullAt(vm.users, index);
            }
        }

        function chooseAdventureType(type) {
            vm.adventure_type = type;
            $scope.$broadcast('wizard:Next');
            nextStep();
        }

        function adventureTypeIcon() {
            if(ionic.Platform.isIOS()) {
                return 'ion-ios-checkmark-outline';
            } else {
                return 'ion-android-checkbox-outline';
            }
        }

        function shareViaFB(id) {
            share(id, 'facebook');
        }

        function shareViaTwitter(id) {
            share(id, 'twitter');
        }

        function shareViaSMS(id) {
            share(id, 'sms');
        }

        function shareViaEmail(id) {
            share(id, 'email');
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');

            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');

            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('places').blur();
            });
        }

        function setMarker(e) {
            GeoCoder.geocode({'location': e.latLng})
                .then(function(result, status) {
                    vm.displayMarker = true;
                    vm.lat = result[0].geometry.location.lat();
                    vm.lon = result[0].geometry.location.lng();
                    vm.address = result[0].formatted_address;

                    if(angular.isDefined(result[0].geometry.viewport)) {
                        vm.map.fitBounds(result[0].geometry.viewport);
                    } else {
                        vm.map.setCenter(result[0].geometry.location);
                        vm.map.setZoom(12);
                    }
                });
        }

        function mapInit(event, map) {
            vm.map = map;
        }

        function activate() {
            var options = {
                timeout:            10000,
                enableHighAccuracy: false
            };

            getMe();

            $cordovaGeolocation.getCurrentPosition(options)
                .then(function(position) {
                    vm.map_lat = position.coords.latitude;
                    vm.map_lon = position.coords.longitude;
                });
        }

        function placeChanged() {
            GeoCoder.geocode({address: vm.address})
                .then(function(result) {
                    vm.displayMarker = true;
                    vm.lat = result[0].geometry.location.lat();
                    vm.lon = result[0].geometry.location.lng();

                    if(angular.isDefined(result[0].geometry.viewport)) {
                        vm.map.fitBounds(result[0].geometry.viewport);
                    } else {
                        vm.map.setCenter(result[0].geometry.location);
                        vm.map.setZoom(12);
                    }
                });
        }

        function share(id, shareType) {
            if(shareLink) {
                socialShare(shareType);
            } else {
                BranchFactory
                    .createLink('adventure-show', 'mobile')
                    .then(function(res) {
                        shareLink = link;

                    }).finally(function() {
                    socialShare(shareType);
                });
            }
        }

        function socialShare(shareType) {
            var adventureType = vm.adventure_type.replace('_', ' ');

            switch(shareType) {
                case 'facebook':
                    $cordovaSocialSharing.shareViaFacebook(null, null, shareLink);
                    break;
                case 'twitter':
                    $cordovaSocialSharing
                        .shareViaTwitter('Come ' + adventureType + ' with me ' +
                            moment(vm.start_time).calendar(), null, shareLink);
                    break;
                case 'sms':
                    $cordovaSocialSharing
                        .shareViaSMS('Come ' + adventureType + ' with me ' +
                            moment(vm.start_time).calendar() + ' -- ' + shareLink, null);
                    break;
                case 'email':
                    $cordovaSocialSharing
                        .shareViaEmail('Find out the details here ' + shareLink,
                            'Come ' + adventureType + ' with me ' +
                            moment(vm.start_time).calendar());
                    break;
            }
        }

        function navigateBackToAdventures() {
            if($ionicHistory.backView() && $ionicHistory.backView().stateName === 'wizard') {
                $ionicHistory.nextViewOptions({
                    historyRoot: true,
                    disableBack: true
                });

                $ionicHistory.clearCache().then(function() {
                    $state.go('loggedIn.adventures-index');
                });
            } else {
                $ionicHistory.clearCache().then(function() {
                    $ionicHistory.goBack();
                });
            }
        }

        function createAdventure() {
            var adventure = _.clone(vm, true);

            adventure.user_ids = _.map(adventure.users, 'id');
            adventure.private = !adventure.public;

            delete adventure.users;
            delete adventure.foundUsers;
            delete adventure.$scope;
            delete adventure.public;
            delete adventure.displayMarker;
            delete adventure.map;
            delete adventure.map_lat;
            delete adventure.map_lon;
            delete adventure.query;

            return adventures
                .create(adventure)
                .then(function(data) {
                    vm.saved = data.adventure;
                    nextStep();

                    AnalyticsFactory
                        .track('adventures', 'create-adventure', adventure);

                    BranchFactory.createObj(
                        vm.saved.id,
                        vm.saved.name,
                        vm.saved.description,
                        'https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png',
                        vm.saved.private,
                        {
                            'url':             'yazda://adventures/' + vm.saved.id,
                            '$og_title':       vm.name,
                            '$og_description': vm.description,
                            '$og_image_url':   'https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png'
                        }
                    ).then(function() {
                        BranchFactory.registerView();
                    });

                    $scope.$broadcast('wizard:Next');
                });
        }
    }
})();
