/*
/!* jshint -W117, -W030 *!/
(function() {
    describe('adventuresNew routes', function() {
        describe('state', function() {
            var view = 'app/features/adventuresNew/adventuresNew.html';

            beforeEach(function() {
                module('yazda.features.adventuresNew', function($provide, $urlRouterProvider){
                    $provide.value('$ionicTemplateCache', function(){} );
                    $urlRouterProvider.deferIntercept();
                });
                bard.inject(
                    '$state',
                    '$templateCache',
                    '$rootScope',
                    '$q'
                );
            });

            bard.verifyNoOutstandingHttpRequests();

            describe('for adventuresNew.route', function() {
                it('should navigate to /adventures/new', function() {
                    expect($state.href('adventuresNew')).to.equal('/adventures/new');
                });
                it('should map /adventuresNew route to Adventures New View template', function() {
                    expect($state.get('adventuresNew').templateUrl).to.equal(view);
                });
                it('of adventuresNew should work with $state.go', function() {
                    $state.go('adventuresNew');
                    $rootScope.$apply();
                    expect($state.is('adventuresNew')).to.be.true();
                });
            });
        });
    });
}());
*/
