(function () {
    'use strict';

    angular.module('yazda.features.adventuresShow', [
        'yazda.core',
        'yazda.websockets'
    ]);

})();
