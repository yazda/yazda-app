(function() {
    'use strict';

    describe('Adventure Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.adventuresShow', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                'adventures',
                '$stateParams'
            );

            bard.mockService(adventures, mocks.AdventuresMock($q));
            bard.mockService($stateParams, mocks.StateParamsMock());
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('AdventureCtrl', {$scope: $scope});

            $rootScope.$apply();

            sinon.spy(adventures, 'show');
        });

        it('Should open AdventureCtrl', function() {
            expect(vm).to.be.ok();
        });

        //TODO: Why is sinon not recording that adventures.show got called?
        describe('Function activate', function() {
            it('Should call adventures.show', function() {
                $stateParams.setId('TESTID');
                $scope.$digest();
                expect(adventures.show.called/*With('TESTID')*/).to.be.true();
            });
        });
    });
})();
