(function() {
  'use strict';

  angular
    .module('yazda.features.adventuresShow')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {

    function sharedConfig(viewName) {
      var returnObject = {url: '/adventures/:id'};

      returnObject.views = {};
      returnObject.views[viewName] = {
        templateUrl: 'app/features/adventuresShow/adventuresShow.html',
        controller:  'AdventureCtrl'
      };

      return returnObject;
    }

    return [
      {
        state: 'loggedIn.adventures-show',
        config: sharedConfig('adventure-view')
      },
      {
        state: 'loggedIn.invite-adventures-show',
        config: sharedConfig('invite-view')
      }
    ];
  }
})();
