(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresShow')
        .controller('AdventureCtrl', AdventureCtrl);

    AdventureCtrl.$inject = ['$scope', 'adventures', '$stateParams',
        'BranchFactory', '$ionicModal', 'WebSockets', '$ionicScrollDelegate', 'account', '$ionicPlatform'];
    function AdventureCtrl($scope, adventures, $stateParams,
                           BranchFactory, $ionicModal, WebSockets, $ionicScrollDelegate, account, $ionicPlatform) {
        var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
        $scope.isAndroid = ionic.Platform.isAndroid;
        $scope.isIOS = ionic.Platform.isIOS;
        $scope.shareAdventure = shareAdventure;
        $scope.viewChat = viewChat;
        $scope.closeModal = closeModal;
        $scope.messages = [];
        $scope.input = {};
        $scope.sendMessage = sendMessage;
        $scope.timeAgo = timeAgo;

        $ionicPlatform.ready(activate);

        $scope.$on('socket:adventures:' + $stateParams.id, function(ev, data) {
            $scope.messages.push(data);
            viewScroll.scrollBottom();
        });

        $scope.$on('socket:adventures:' + $stateParams.id + ':paged', function(ev, json) {
            $scope.messages = json;
            viewScroll.scrollBottom();
        });

        $scope.$on('$destroy', function() {
            console.log('destroying');
            $scope.modal.remove();
            WebSockets.disconnectFromAdventure($stateParams.id);
        });

        function timeAgo(date) {
            return moment(date).fromNow();
        }

        function sendMessage(msg) {
            WebSockets.sendMessageToAdventurers($scope.data.id, msg);
            $scope.input.message = '';
        }

        function closeModal() {
            markRead();

            $scope
                .modal
                .hide();
        }

        function viewChat() {
            markRead();

            $scope
                .modal
                .show()
                .then(function() {
                    viewScroll.scrollBottom();
                });
        }

        function markRead() {
            WebSockets.markAdventureConvRead($scope.data.id);
        }

        function shareAdventure(adventure) {
            BranchFactory.share('adventure-show', 'mobile');
        }

        function activate() {
            account.me().then(function(data) {
                $scope.me = data.user;
            });

            $ionicModal
                .fromTemplateUrl('app/components/chat/view-chat.html', {
                    scope:     $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    var id = $stateParams.id;
                    $scope.modal = modal;
                    WebSockets.connectToAdventure(id);
                }
            );

            return getAdventure();
        }

        function getAdventure() {
            return adventures
                .show($stateParams.id).then(function(data) {
                    var adventure = data.adventure;

                    $scope.data = adventure;
                    $scope.title = adventure.name;

                    BranchFactory.createObj('adventure_' + adventure.id,
                        adventure.name,
                        adventure.description,
                        'https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png',
                        adventure.private, {
                            'url':             'yazda://adventures/' + adventure.id,
                            '$og_title':       adventure.name,
                            '$og_description': adventure.description,
                            '$og_image_url':   'https://yazdaapp.com/wp-content/uploads/2015/05/yazda-180x180.png'
                        }).then(function() {
                        BranchFactory.registerView();
                    });

                    return $scope.data;
                });
        }
    }
})();
