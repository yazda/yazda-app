(function() {
    'use strict';

    describe('Adventures Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.adventuresIndex', 'yazda.yazda-api', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function() {
                });
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                '$cordovaGeolocation',
                '$ionicPlatform',
                'adventures'
            );

            bard.mockService(adventures, mocks.AdventuresMock($q));
            bard.mockService($cordovaGeolocation, mocks.CordovaGeoLocationMock($q));
            bard.mockService($ionicPlatform, mocks.IonicPlatformMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('AdventuresCtrl', {$scope: $scope});
            $rootScope.$apply();

            sinon.spy(adventures, 'index');
            sinon.spy($scope, '$broadcast');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open AdventuresCtrl', function() {
            expect(vm).to.be.ok();
        });

        describe('$ionicPlatform.ready', function() {
            it('Should call $cordovaGeolocation.getCurrentPosition', function() {
                //TODO: $cordovaGeolocation.getCurrentPosition is not being REPORTED as called
                //TODO: Console.log proves otherwise
                //expect(vm.getAdventures.called).to.be.true();
            });
        });
        describe('Function: refresh', function() {
            it('Should set currentPage to 1 && data to []', function() {
                vm.currentPage = 100;
                vm.data = ['data', 'data'];

                expect(vm.currentPage).to.eq(100);
                expect(vm.data.length).to.eq(2);

                vm.refresh();
                $rootScope.$apply();

                expect(vm.currentPage).to.eq(1);
                //vm.data is populated again when getAdventures() is called
                expect(vm.data.length).to.eq(3);
            });
        });

        describe('Function: loadNextPage', function() {
            it('Should increment vm.currentPage by 1', function() {
                vm.currentPage = 0;
                vm.loadNextPage();
                $rootScope.$apply();
                expect(vm.currentPage).to.eq(1);
            });
        });

        describe('Function moreData', function() {
            it('Should return true if currentPage < page_count', function() {
                vm.currentPage = 2;
                vm.page_count = 3;
                expect(vm.moreData()).to.be.true();
            });
            it('Should return false if currentPage > page_count', function() {
                vm.currentPage = 2;
                vm.page_count = 1;
                expect(vm.moreData()).to.be.false();
            });
        });
        //TODO: this fails because it is related to bug in controller
        //describe('setLatAndLon', function() {
        //
        //    it('setLatAndLon() should set lat and lon variables to position', function() {
        //
        //        var pos = {
        //          coords: {
        //              latitude: 500,
        //              longitude: 300
        //          }
        //
        //        };
        //        vm.setLatAndLon(pos);
        //        expect(vm.lat).to.eq(500);
        //        expect(vm.lon).to.eq(300);
        //
        //    });
        //
        //});

        describe('Function getAdventures', function() {
            beforeEach(function() {
                vm.data = [];
                vm.getAdventures();
                $rootScope.$apply();
            });

            it('Should call adventures.index', function() {
                expect(adventures.index.called).to.be.true();
            });

            it('Should $broadcast', function() {
                expect($scope.$broadcast.calledWith('scroll.refreshComplete')).to.be.true();
                expect($scope.$broadcast.calledWith('scroll.infiniteScrollComplete')).to.be.true();
            });

            it('Should set values for: vm.page_count', function() {
                expect(vm.page_count).to.eq(4);
            });

            it('Should return data', function() {
                expect(vm.data).to.eql([
                    {id: 1, name: 'adv 1'},
                    {id: 2, name: 'adv 2'},
                    {id: 3, name: 'adv 3'}
                ]);
            });
        });
    });
})();
