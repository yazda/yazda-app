(function() {
    'use strict';

    angular
        .module('yazda.features.clubsIndex')
        .controller('ClubsIndexCtrl', ClubsIndexCtrl);

    /* @ngInject */
    function ClubsIndexCtrl($scope, clubs, $cordovaGeolocation, $ionicPlatform) {
        var vm = this;
        var DEFAULT_PAGE_SIZE_STEP = 25;
        var lat;
        var lon;

        vm.query = '';
        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.data = [];
        vm.getClubs = getClubs;
        vm.refresh = refresh;
        vm.clear = clear;

        $scope.timeAgo = timeAgo;
        $scope.me = {id: window.localStorage.getItem('current_user_id')};
        $scope.backgroundStyle = backgroundStyle;

        $ionicPlatform.ready(activate);

        function activate() {
            $scope.$watch(function() {
                return vm.query || vm.isFocused;
            }, function(value) {
                vm.data = [];

                getClubs();
            });

            return $cordovaGeolocation.getCurrentPosition({
                    timeout:            2000,
                    enableHighAccuracy: false
                })
                .then(setLatAndLon);
        }

        function setLatAndLon(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
        }

        function clear() {
            vm.currentPage = 1;
            vm.data = [];
            vm.query = '';
            vm.isFocused = false;
        }

        function backgroundStyle(club) {
            if(club) {
                if(club && club.banner_image_thumb_url) {
                    return 'box-shadow: inset 0 0 0 1000px' +
                        ' rgba(39,39,39,0.8) !important;background-position:' +
                        ' center;background-image:' +
                        ' url(' + club.banner_image_thumb_url + ');background-size: cover;';
                }
            }
        }

        function timeAgo(date) {
            return moment(date).fromNow();
        }

        function refresh() {
            vm.currentPage = 1;
            vm.data = [];
            getClubs();
        }

        function loadNextPage() {
            vm.currentPage++;
            getClubs();
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function getClubs() {
            var config = {
                page: vm.currentPage
            };

            if(vm.query.trim().length > 0 || vm.isFocused) {
                config.q = vm.query;
                config.lat = 39.741541;
                config.lon = -105.32889;
            } else {
                config.user_id = $scope.me.id;
            }

            return clubs.index(config)
                .then(function(data) {
                    var clubs = data.clubs;

                    vm.page_count = data.page_count;
                    vm.data.push.apply(vm.data, clubs);

                    return vm.data;
                })
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }
    }
})();
