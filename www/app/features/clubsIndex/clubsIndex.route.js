(function() {
    'use strict';

    angular
        .module('yazda.features.clubsIndex')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.clubs-index',
                config: {
                    url: '/clubs', //TODO wtf why is the router not working
                    // properly
                    isProtected: true,
                    views: {
                        'club-view': {
                            templateUrl: 'app/features/clubsIndex/clubsIndex.html',
                            controller: 'ClubsIndexCtrl as clubs'
                        }
                    }
                }
            }
        ];
    }
})();
