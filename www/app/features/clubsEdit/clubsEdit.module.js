(function () {
    'use strict';

    angular.module('yazda.features.clubsEdit', [
        'yazda.core',
        'ngMap'
    ]);
})();
