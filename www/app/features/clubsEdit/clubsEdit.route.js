(function() {
    'use strict';

    angular
        .module('yazda.features.clubsEdit')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function sharedConfig(viewName) {
        var returnObject = {url: '/clubs/:id/edit'};

        returnObject.views = {};
        returnObject.views[viewName] = {
            templateUrl: 'app/features/clubsEdit/clubsEdit.html'
        };

        return returnObject;
    }

    function getStates() {
        return [
            {
                state:  'loggedIn.adventure-clubs-edit',
                config: {
                    url:         '/adventures/:adventureId/clubs/:id/edit',
                    views: {
                        'adventure-view': {
                            templateUrl: 'app/features/clubsEdit/clubsEdit.html'
                        }
                    }

                }
            },
            {
                state:  'loggedIn.invite-clubs-edit',
                config: sharedConfig('invite-view')
            },
            {
                state:  'loggedIn.club-clubs-edit',
                config: sharedConfig('club-view')
            }
        ];
    }
})();
