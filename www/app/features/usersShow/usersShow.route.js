(function() {
    'use strict';

    angular
        .module('yazda.features.usersShow')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        function sharedConfig(viewName) {
            var returnObject = {url: '/users/:id'};

            returnObject.views = {};
            returnObject.views[viewName] = {templateUrl: 'app/features/usersShow/usersShow.html'};

            return returnObject;
        }

        return [
            {
                state:  'loggedIn.invite-user-show',
                config: sharedConfig('invite-view')
            },
            {
                state:  'loggedIn.adventure-users-show',
                config: sharedConfig('adventure-view')
            },
            {
                state:  'loggedIn.profile-users-show',
                config: sharedConfig('profile-view')
            },
            {
                state:  'loggedIn.club-users-show',
                config: sharedConfig('club-view')
            },
            {
                state:  'loggedIn.invite-users-show',
                config: sharedConfig('invite-view')
            }
        ];
    }
})();
