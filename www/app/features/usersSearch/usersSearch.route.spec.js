/* jshint -W117, -W030 */
(function() {
    describe('usersSearch routes', function() {
        describe('state', function() {
            var view = 'app/features/usersSearch/usersSearch.html';

            beforeEach(function() {
                module('yazda.features.usersSearch', function($provide, $urlRouterProvider){
                    $provide.value('$ionicTemplateCache', function(){} );
                    $urlRouterProvider.deferIntercept();
                });
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for usersSearch route', function() {
                it('should navigate to /users/search', function() {
                    expect($state.href('loggedIn.users-search'))
                        .to.equal('/users/search');
                });

                it('should map /users/search route to login template', function() {
                    expect($state.get('loggedIn.users-search').templateUrl).to.equal(view);
                });

                it('usersSearch should work with $state.go', function() {
                    $state.go('loggedIn.users-search');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.users-search');
                });
            });
        });
    });
}());
