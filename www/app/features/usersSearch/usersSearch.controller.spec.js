(function() {
    'use strict';

    describe('Users Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.usersSearch', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$httpBackend',
                'users'
            );

            bard.mockService(users, mocks.UsersMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('UsersCtrl', {$scope: $scope});

            $rootScope.$apply();
        });

        it('should',function() {
            console.log(vm);
        });
    });
})();
