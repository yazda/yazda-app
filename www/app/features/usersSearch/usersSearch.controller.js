(function() {
    'use strict';

    angular
        .module('yazda.features.usersSearch')
        .controller('UsersCtrl', UsersCtrl);

    /* @ngInject */
    function UsersCtrl($scope, users, $ionicPlatform) {
        var DEFAULT_PAGE_SIZE_STEP = 25;
        var vm = this;

        vm.query = '';
        vm.getUsers = getUsers;
        vm.users = [];
        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.refresh = refresh;
        vm.clear = clear;

        $ionicPlatform.ready(function() {
            $scope.$watch(function() {
                return vm.query;
            }, function(value) {
                vm.users = [];

                getUsers();
            });
        });

        function clear() {
            vm.currentPage = 1;
            vm.users = [];
            vm.query = '';
        }

        function refresh() {
            vm.currentPage = 1;
            vm.users = [];

            getUsers(vm.query);
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function loadNextPage() {
            vm.currentPage++;
            getUsers();
        }

        function getUsers() {
            return users
                .search({
                    q:    vm.query,
                    page: vm.currentPage
                })
                .then(setUsersData)
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }

        function setUsersData(data) {
            console.log('got users');
            var user = data.users;

            vm.users.push.apply(vm.users, user);
            vm.page_count = data.page_count;

            return vm.users;
        }
    }
})();
