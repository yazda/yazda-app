(function() {
  'use strict';

  angular
    .module('yazda.features.usersSearch')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'loggedIn.users-search',
        config: {
          url:   '/users/search',
          views: {
            'profile-view': {
              templateUrl: 'app/features/usersSearch/usersSearch.html',
              controller:  'UsersCtrl as search'
            }
          }
        }
      }
    ];
  }
})();
