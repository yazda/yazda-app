/* jshint -W117, -W030 */
(function() {
    describe('login routes', function() {
        describe('state', function() {
            var view = 'app/features/login/login.htm';

            beforeEach(function() {
                module('yazda.features.login');
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();


            describe('for login route', function() {
                it('should navigate to /login', function() {
                    expect($state.href('login'))
                        .to.equal('/login');
                });

                it('should map /login route to login template', function() {
                    expect($state.get('login').templateUrl).to.equal(view);
                });

                it('login should work with $state.go', function() {
                    $state.go('login');

                    $rootScope.$apply();

                    expect($state.current).to.be('login');
                });
            });
        });
    });
}());
