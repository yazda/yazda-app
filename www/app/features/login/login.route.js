(function() {
  'use strict';

  angular
    .module('yazda.features.login')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'login',
        config: {
          url: '/login',
          templateUrl: 'app/features/login/login.html'
        }
      }
    ];
  }
})();
