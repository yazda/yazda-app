(function () {
    'use strict';

    describe('Login Controller', function () {
        var vm,
            $scope;

        beforeEach(function () {
            bard.appModule('yazda.features.login', 'yazda.yazda-api', function ($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function () {
                });
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                'oauthClient'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));
        });

        beforeEach(function () {
            $scope = $rootScope.$new();
            vm = $controller('LoginCtrl', {$scope: $scope});
            $rootScope.$apply();

            sinon.spy($rootScope, '$broadcast');
        });

        it('vm should exist', function () {
            expect(vm).to.be.ok();
        });

        describe('signIn(user)', function () {
            it('broadcasts a username and password', function () {
                var user = {
                    username: 'name',
                    password: 'password'
                };

                vm.signIn(user);
                $rootScope.$apply();

                expect($rootScope.$broadcast.calledWith('user.login'));
            });
        });
    });
})();
