(function() {
    angular
        .module('yazda.features.login')
        .controller('LoginCtrl', LoginCtrl);

    /* @ngInject */
    function LoginCtrl($rootScope, oauthClient) {
        var vm = this;

        vm.signIn = signIn;

        function signIn(user) {
            oauthClient.login(user.username, user.password);
        }
    }
})();


