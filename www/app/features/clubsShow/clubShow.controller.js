(function() {
    'use strict';

    angular
        .module('yazda.features.clubsShow')
        .controller('ClubShowCtrl', ClubShowCtrl);

    /* @ngInject */
    function ClubShowCtrl($stateParams, $state, $ionicHistory, clubs, BranchFactory, $ionicPlatform) {
        var vm = this;

        vm.activate = activate;
        vm.getClub = getClub;
        vm.editClub = editClub;
        vm.share = share;

        $ionicPlatform.ready(activate);

        function activate() {
            return getClub();
        }

        function editClub() {
            if($ionicHistory.currentStateName() === 'loggedIn.adventure-clubs-show') {
                $state.go('loggedIn.adventure-clubs-edit', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.profile-show' ||
                $ionicHistory.currentStateName() === 'loggedIn.profile-clubs-show') {
                $state.go('loggedIn.profile-clubs-edit', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.invite-clubs-show') {
                $state.go('loggedIn.invite-clubs-edit', {id: vm.data.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.club-clubs-show') {
                $state.go('loggedIn.club-clubs-edit', {id: vm.data.id});
            }
        }

        function getClub() {
            return clubs
                .show($stateParams.id)
                .then(setClubData);
        }

        function share() {
            BranchFactory.share('adventure-show', 'mobile');
        }

        function setClubData(data) {
            var club = data.club;

            vm.data = club;

            BranchFactory.createObj('club_' + club.id,
                club.name,
                club.description,
                club.avatar_url,
                false, {
                    'url':             'yazda://clubs/' + club.id,
                    '$og_title':       club.name,
                    '$og_description': club.description,
                    '$og_image_url':   club.avatar_url
                }).then(function() {
                BranchFactory.registerView();
            });

            return vm.data;
        }
    }
})();
