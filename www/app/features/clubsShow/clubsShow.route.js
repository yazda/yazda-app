(function() {
    'use strict';

    angular
        .module('yazda.features.clubsShow')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        function sharedConfig(viewName) {
            var returnObject = {url: '/clubs/:id'};

            returnObject.views = {};
            returnObject.views[viewName] = {
                templateUrl: 'app/features/clubsShow/clubsShow.html',
                controller:  'ClubShowCtrl as club'
            };

            return returnObject;
        }

        return [
            {
                state:  'loggedIn.club-clubs-show',
                config: sharedConfig('club-view')
            },
            {
                state:  'loggedIn.adventure-clubs-show',
                config: {
                    url:         '/adventures/:adventureId/clubs/:id',
                    views: {
                        'adventure-view': {
                            templateUrl: 'app/features/clubsShow/clubsShow.html',
                            controller:  'ClubShowCtrl as club'
                        }
                    }

                }
            },
            {
                state:  'loggedIn.invite-clubs-show',
                config: sharedConfig('invite-view')
            },
            {
                state:  'loggedIn.profile-clubs-show',
                config: sharedConfig('profile-view')
            }
        ];
    }
})();
