/* jshint -W117, -W030 */
(function() {
    describe('usersShow routes', function() {
        describe('state', function() {
            var view = 'app/features/usersShow/usersShow.htm';

            beforeEach(function() {
                module('yazda.features.usersShow', function($provide, $urlRouterProvider){
                    $provide.value('$ionicTemplateCache', function(){} );
                    $urlRouterProvider.deferIntercept();
                });
                bard.inject(
                    'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();

            describe('for usersShow route', function() {
                it('should navigate to /users/search', function() {
                    expect($state.href('loggedIn.invite-user-show'))
                        .to.equal('/users/:id');
                });

                it('should map /users/search route to login template', function() {
                    expect($state.get('loggedIn.invite-user-show').templateUrl).to.equal(view);
                });

                it('usersSearch should work with $state.go', function() {
                    $state.go('loggedIn.invite-user-show');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.invite-user-show');
                });
            });
            describe('for usersShow route', function() {
                it('should navigate to /users/search', function() {
                    expect($state.href('loggedIn.adventure-users-show'))
                        .to.equal('/users/:id');
                });

                it('should map /users/search route to login template', function() {
                    expect($state.get('loggedIn.adventure-users-show').templateUrl).to.equal(view);
                });

                it('usersSearch should work with $state.go', function() {
                    $state.go('loggedIn.adventure-users-show');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.adventure-users-show');
                });
            });
            describe('for usersShow route', function() {
                it('should navigate to /users/search', function() {
                    expect($state.href('loggedIn.profile-users-show'))
                        .to.equal('/users/:id');
                });

                it('should map /users/search route to login template', function() {
                    expect($state.get('loggedIn.profile-users-show').templateUrl).to.equal(view);
                });

                it('usersSearch should work with $state.go', function() {
                    $state.go('loggedIn.profile-users-show');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.profile-users-show');
                });
            });
            describe('for usersShow route', function() {
                it('should navigate to /users/search', function() {
                    expect($state.href('loggedIn.invite-users-show'))
                        .to.equal('/users/:id');
                });

                it('should map /users/search route to login template', function() {
                    expect($state.get('loggedIn.invite-users-show').templateUrl).to.equal(view);
                });

                it('usersSearch should work with $state.go', function() {
                    $state.go('loggedIn.invite-users-show');

                    $rootScope.$apply();

                    expect($state.current).to.be('loggedIn.invite-users-show');
                });
            });
        });
    });
}());
