(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresIndex')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'loggedIn.adventures-index',
                config: {
                    url: '/adventures',
                    cache: true,
                    views: {
                        'adventure-view': {
                            templateUrl: 'app/features/adventuresIndex/adventuresIndex.html',
                            controller: 'AdventuresCtrl as adventures'
                        }
                    },
                    isProtected: true
                }
            }
        ];
    }
})();
