(function () {
    'use strict';

    angular.module('yazda.features.adventuresIndex', [
        'yazda.core',
        'yazda.websockets'
    ]);

})();
