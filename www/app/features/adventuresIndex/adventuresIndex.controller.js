(function() {
    'use strict';

    angular
        .module('yazda.features.adventuresIndex')
        .controller('AdventuresCtrl', AdventuresCtrl);

    /* @ngInject */
    function AdventuresCtrl($scope, adventures, $cordovaGeolocation, $ionicPlatform, WebSockets, $ionicModal, $filter, $ionicScrollDelegate) {
        var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
        var vm = this;
        var DEFAULT_PAGE_SIZE_STEP = 25;
        var lat;
        var lon;
        var navToConv = false;

        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.data = [];
        vm.getAdventures = getAdventures;
        vm.refresh = refresh;
        vm.viewChat = viewChat;
        vm.viewConversations = viewConversations;
        vm.closeConversations = closeConversations;
        vm.unread = unread;
        vm.convMessages = {};
        vm.conversations = [];

        $scope.convUnread = convUnread;
        $scope.closeModal = closeModal;
        $scope.sendMessage = sendMessage;
        $scope.input = {message: ''};
        $scope.sendTo = [];
        $scope.externalScope = $scope;
        $scope.sanatizeUserNames = sanatizeUserNames;
        $scope.images = images;
        $scope.timeAgo = timeAgo;
        $scope.me = {id: window.localStorage.getItem('current_user_id')};
        $scope.backgroundStyle = backgroundStyle;

        $ionicPlatform.ready(activate);

        $scope.$on('socket:conversations', function(e, conversations) {
            vm.conversations = conversations.messages || [];
            viewScroll.scrollBottom();
        });

        $scope.$on('socket:conversations:add', function(e, conversations) {
            $scope.conversationId = conversations[0].conversationId;
            $scope.title = sanatizeUserNames(conversations[0]);

            vm.convMessages[$scope.conversationId] = conversations;
            $scope.messages = $filter('orderBy')(
                vm.convMessages[$scope.conversationId],
                {sent_at: true});

            viewScroll.scrollBottom();
        });

        $scope.$on('socket:direct_chat', function(e, msg) {
            if(navToConv) {
                $scope.conversationId = msg.conversationId;
                navToConv = false;
            }

            if(!vm.convMessages[msg.conversationId]) {
                vm.convMessages[msg.conversationId] = [];
            }

            vm.conversations.push(msg);
            vm.convMessages[msg.conversationId].push(msg);

            $scope.messages = $filter('orderBy')(
                vm.convMessages[$scope.conversationId],
                {sent_at: true});

            viewScroll.scrollBottom();
        });

        $scope.$on('$destroy', function() {
            $scope.modal.remove();
            $scope.conversationModal.remove();
        });

        function backgroundStyle(adventure) {
            if(adventure) {
                var club = adventure.club;

                if(club && club.banner_image_thumb_url) {
                    return 'box-shadow: inset 0 0 0 1000px' +
                        ' rgba(39,39,39,0.8) !important;background-position:' +
                        ' center;background-image:' +
                        ' url('+club.banner_image_thumb_url +');background-size: cover;';
                }
            }
        }

        function timeAgo(date) {
            return moment(date).fromNow();
        }

        function images(message) {
            return _.reject(message.sentTo,
                {id: Number($scope.me.id)});
        }

        function sanatizeUserNames(message) {
            return _.pluck(
                _.reject(message.sentTo,
                    {id: Number($scope.me.id)}),
                'name')
                .join(', ')
                .replace(/,([^,]*)$/, ' &$1');
        }

        function sendMessage(msg, conversationId) {
            if(!conversationId) {
                navToConv = true;
            }
            WebSockets.sendMessageToUsers({
                msg:            msg,
                sentTo:         $scope.sendTo,
                conversationId: conversationId
            });
            $scope.sendTo = [];
            $scope.input.message = '';
        }

        function convUnread(id) {
            return _.reduce(
                _.where(vm.conversations,
                    {conversationId: id}),
                function(num, obj) {
                    return num + obj.unread;
                }, 0);
        }

        function unread() {
            return _.reduce(vm.conversations, function(num, obj) {
                return num + obj.unread;
            }, 0);
        }

        function viewConversations() {
            $scope
                .conversationModal
                .show();
        }

        function closeConversations() {
            $scope
                .conversationModal
                .hide();
        }

        function viewChat(conversationId) {
            $scope.conversationId = conversationId;
            $scope.messages = _.where(vm.conversations, {conversationId: conversationId});
            $scope.title = $scope.messages.length ? sanatizeUserNames($scope.messages[0]) : '';

            WebSockets.getConversationMessages(conversationId);

            markRead();

            if(!conversationId) {
                vm.users = [];
                $scope.messages = [];
            }

            $scope
                .modal
                .show()
                .then(function() {
                });
        }

        function closeModal() {
            markRead();

            $scope
                .modal
                .hide()
                .then(function() {
                });
        }

        function markRead() {
            WebSockets.markConversationRead($scope.conversationId);

            _.each(_.where(vm.conversations,
                {conversationId: $scope.conversationId}),
                function(conv) {
                    conv.unread = 0;
                });
        }

        function refresh() {
            vm.currentPage = 1;
            vm.data = [];
            getAdventures();
        }

        function loadNextPage() {
            vm.currentPage++;
            getAdventures();
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function activate() {
            $ionicModal
                .fromTemplateUrl('app/components/chat/view-conversations.html', {
                    scope:     $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.conversationModal = modal;
                }
            );

            $ionicModal
                .fromTemplateUrl('app/components/chat/view-chat.html', {
                    scope:     $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.modal = modal;
                }
            );

            return $cordovaGeolocation.getCurrentPosition({
                    timeout:            2000,
                    enableHighAccuracy: false
                })
                .then(setLatAndLon)
                .finally(function() {
                    getAdventures();
                });
        }

        function setLatAndLon(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
        }

        function getAdventures() {
            var config = {
                page: vm.currentPage,
                lat:  lat,
                lon:  lon
            };

            return adventures.index(config)
                .then(function(data) {
                    var adventures = data.adventures;

                    vm.page_count = data.page_count;
                    vm.data.push.apply(vm.data, adventures);

                    return vm.data;
                })
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }
    }
})();
