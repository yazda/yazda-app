/*
/!* jshint -W117, -W030 *!/
(function() {
    describe('adventuresIndex routes', function() {
        describe('state', function() {
            var view = 'app/features/adventuresIndex/adventuresIndex.html';

            beforeEach(function() {
                module('yazda.features.adventuresIndex', function($provide, $urlRouterProvider){
                    $provide.value('$ionicTemplateCache', function(){} );
                    $urlRouterProvider.deferIntercept();
                });
                bard.inject(
                    //'routerHelper',
                    '$rootScope',
                    '$state',
                    '$templateCache'
                );
            });

            beforeEach(function() {
                $templateCache.put(view, '');
            });

            bard.verifyNoOutstandingHttpRequests();

            //TODO: not using ui-router? Tests need to be refactored?

            describe('for adventuresIndex.route', function() {
                it('should navigate to /adventures', function() {
                    expect($state.href('loggedIn.adventures-index'))
                        .to.eq('/adventures');
                });

                it('should map /adventures route to Adventures Index template', function() {
                    expect($state.get('loggedIn.adventures-index').templateUrl).to.equal(view);
                });

                it('loggedIn.adventures-index should work with $state.go', function() {
                    $state.go('loggedIn.adventures-index');

                    $rootScope.$apply();

                    expect($state.is('loggedIn.adventures-index')).to.be.true();
                });

                it('should set cache to false', function() {
                    expect().to.be.false();
                });

            });
        });
    });
}());
*/
