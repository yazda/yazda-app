(function() {
    'use strict';

    angular
        .module('yazda.features.friends')
        .run(appRun);

    appRun.$inject = ['routerHelper'];

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        function sharedConfig(viewName) {
            var returnObject = {url: '/users/:id/friends'};

            returnObject.views = {};
            returnObject.views[viewName] = {
                templateUrl: 'app/features/friends/friends.html',
                controller:  'FriendsCtrl as users'
            };

            return returnObject;
        }

        return [
            {
                state:  'loggedIn.profile-friends-index',
                config: sharedConfig('profile-view')
            },
            {
                state:  'loggedIn.adventure-friends-index',
                config: sharedConfig('adventure-view')
            },
            {
                state:  'loggedIn.invite-friends-index',
                config: sharedConfig('invite-view')
            },
            {
                state:  'loggedIn.club-friends-index',
                config: sharedConfig('club-view')
            }
        ];
    }
})();
