(function() {
    'use strict';

    describe('Friends Controller', function() {
        var vm,
            $scope;

        beforeEach(function() {
            bard.appModule('yazda.features.friends', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$controller',
                '$rootScope',
                '$stateParams',
                '$ionicHistory',
                '$state',
                'friends'
            );


            bard.mockService($state, mocks.StateMock($q));
            bard.mockService($ionicHistory, mocks.IonicHistoryMock());
            bard.mockService(friends, mocks.FriendsMock($q));
        });

        beforeEach(function() {
            $scope = $rootScope.$new();

            vm = $controller('FriendsCtrl', {$scope: $scope});

            $rootScope.$apply();

            sinon.spy($state, 'go');
            sinon.spy(friends, 'index');
            sinon.spy($scope, '$broadcast');
        });

        describe('Function viewUser', function() {
            var id = 'testId';

            describe('If $ionicHistory === "loggedIn.adventure-friends-index"', function() {

                it('Should call $state.go', function() {
                    $ionicHistory.currentStateName('loggedIn.adventure-friends-index');
                    vm.viewUser(id);
                    expect($state.current()).to.eq('loggedIn.adventure-users-show');
                });
            });

            describe('If $ionicHistory === "loggedIn.profile-friends-index"', function() {

                it('Should call $state.go', function() {
                    $ionicHistory.currentStateName('loggedIn.profile-friends-index');

                    vm.viewUser(id);

                    expect($state.current()).to.eq('loggedIn.profile-users-show');
                });
            });

            describe('If $ionicHistory === "loggedIn.invite-friends-index"', function() {

                it('Should call $state.go', function() {
                    $ionicHistory.currentStateName('loggedIn.invite-friends-index');

                    vm.viewUser(id);

                    expect($state.current()).to.eq('loggedIn.invite-users-index');
                });
            });

            describe('If !$ionicHistory.currentStateName() === \'loggedIn.adventure-friends-index\',' +
                + '\'loggedIn.profile-friends-index\',\'loggedIn.invite-friends-index\'', function() {

                it('Should stay the same',function() {
                    $state.go('denver baseball');

                    vm.viewUser(id);

                    expect($state.is('denver baseball')).to.be.true();


               });
            });
        });
        describe('Function refresh', function() {
            it('Should set vm.users to []', function() {
                vm.currentPage = 2;
                vm.users = ['TestUser', 'TestUser'];
                vm.refresh();
                $rootScope.$apply();
                expect(vm.users.length).to.eq(1);
            });
        });
        describe('Function moreData', function() {
            it('Should return true if currentPage < page_count', function() {
                vm.page_count = 2;
                expect(vm.moreData()).to.eq(true);
            });
            it('Should return false if currentPage !< page_count', function() {
                vm.page_count = 1;
                expect(vm.moreData()).to.eq(false);
            });
        });

        describe('Function loadNextPage', function() {
           it('Should increment currentPage by 1', function() {
              vm.loadNextPage();
               expect(vm.currentPage).to.eq(2);
           });
        });

        describe('Function getFriends', function() {
           it('Should call friends.index', function() {
              vm.getFriends();
               $rootScope.$apply();
               expect(friends.index.called).to.be.true();
           });
        });

        describe('Function setFriendData', function() {
           it('Should $broadcast', function() {
               vm.getFriends();
               $rootScope.$apply();
               expect($scope.$broadcast.calledWith('scroll.refreshComplete'))
               expect($scope.$broadcast.calledWith('scroll.infiniteScrollComplete'))
           });
        });
    });
})();
