(function() {
    'use strict';

    angular
        .module('yazda.features.friends')
        .controller('FriendsCtrl', FriendsCtrl);

    FriendsCtrl.$inject = ['$scope', 'friends', '$stateParams', '$ionicHistory', '$state','$ionicPlatform'];
    function FriendsCtrl($scope, friends, $stateParams, $ionicHistory, $state,$ionicPlatform) {
        var DEFAULT_PAGE_SIZE_STEP = 25;
        var vm = this;

        vm.getFriends = getFriends;
        vm.users = [];
        vm.currentPage = 1;
        vm.loadNextPage = loadNextPage;
        vm.moreData = moreData;
        vm.refresh = refresh;
        vm.viewUser = viewUser;

        $ionicPlatform.ready(getFriends);

        function viewUser(id) {

            if($ionicHistory.currentStateName() === 'loggedIn.adventure-friends-index') {
                $state.go('loggedIn.adventure-users-show', {id: id});

            } else if($ionicHistory.currentStateName() === 'loggedIn.profile-friends-index') {
                $state.go('loggedIn.profile-users-show', {id: id});

            } else if($ionicHistory.currentStateName() === 'loggedIn.invite-friends-index') {
                $state.go('loggedIn.invite-users-index', {id: id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.club-friends-index') {
                $state.go('loggedIn.club-users-show', {id: id});
            }
        }

        function refresh() {
            vm.currentPage = 1;
            vm.users = [];

            getFriends();
        }

        function moreData() {
            return vm.currentPage < vm.page_count;
        }

        function loadNextPage() {
            vm.currentPage++;
            getFriends();
        }

        function getFriends(query) {
            return friends.index($stateParams.id, {
                    page: vm.currentPage
                })
                .then(setFriendData)
                .finally(function() {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
        }

        function setFriendData(data) {
            var user = data.users;

            vm.users.push.apply(vm.users, user);
            vm.page_count = data.page_count;

            return vm.users;
        }
    }
})();
