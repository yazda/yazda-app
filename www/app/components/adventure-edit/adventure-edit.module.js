(function() {
  'use strict';

  angular.module('yazda.components.adventureEdit', [
      'ngMap',
      'ngCordova'
  ]);

})();
