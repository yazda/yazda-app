(function () {
    'use strict';

    describe('Directive: adventureEdit', function () {
        var html,
            element,
            vm;

        beforeEach(function () {
            bard.appModule('yazda.components.adventureEdit', 'yazda.yazda-api');
            bard.inject(
                '$httpBackend',
                '$compile',
                '$q',
                '$rootScope',
                '$templateCache',
                'adventures',
                'GeoCoder',
                '$ionicHistory',
                '$cordovaGeolocation'
            );

            bard.mockService(adventures, mocks.AdventuresMock($q));
            bard.mockService(GeoCoder, mocks.GeoCoderMock($q));
            bard.mockService($ionicHistory, mocks.IonicHistoryMock($q));
            bard.mockService($cordovaGeolocation, mocks.CordovaGeoLocationMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<adventure-edit></adventure-edit>');
            $rootScope = $rootScope.$new();

            $templateCache.put('app/components/adventure-edit/adventure-edit.html', '');

            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            vm = element.controller('adventureNew');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('should', function() {
            expect(element).to.be.defined;
            expect(vm).to.be.defined;
        });

        describe('For: $rootScope.$on mapInitialized', function() {
            it('Should set vm.map', function() {
                var map = {
                    map: 'testMap'
                };

                $rootScope.$emit('mapInitialized', map);
                $rootScope.$apply();

                expect(vm.map).to.eql(map);
            });
        });

        describe('For: function activate()', function() {
            it('Should: addEventListener', function() {
                var event = document.createEvent('Event');

                event.initEvent('deviceready', true, true);

                // Dispatch the event.
                document.dispatchEvent(event);


                //$rootScope.emit('deviceready');
                $rootScope.$apply();

                expect(vm.map_lat).to.equal(100);
                expect(vm.map_lon).to.equal(200);
            });
        });

        describe('For: placeChanged()', function() {
            beforeEach(function() {
                vm.address = "TestAddress";
                vm.map = {
                    fitBounds: function(viewPort) {
                        return 'TestBounds';
                    },
                    setCenter: function(location) {
                        return 'Centered';
                    },
                    setZoom: function(zoom) {
                        return zoom;
                    }
                }
                $rootScope.$apply();

                sinon.spy(vm.map, 'fitBounds');
                sinon.spy(vm.map, 'setZoom');
            });

            it('Should: call vm.map.fitBounds', function() {
                vm.placeChanged();
                $rootScope.$apply();

                expect(vm.displayMarker).to.be.true();
                expect(vm.map.fitBounds.called).to.be.true();
            });

            it('Should: call vm.map.setZoom & vm.map.setCenter', function() {
                GeoCoder.geocode = function() {
                    return $q.when([
                        {
                            geometry: {
                                location: {
                                    lat: function() {
                                        return 100;
                                    },
                                    lng: function() {
                                        return 200;
                                    }
                                },
                                viewport: undefined
                            }
                        }
                    ])
                };
                vm.placeChanged();
                $rootScope.$apply();
                expect(vm.map.setZoom.called).to.be.true();
            });

            //it('Should: set vm.lon = result[0].geometry.location.lng(), ' +
            //    'and set vm.lat = result[0].geometry.location.lat();', function() {
            //
            //    vm.placeChanged();
            //    $rootScope.$apply();
            //
            //    expect(vm.lon).to.eql(100);
            //    expect(vm.lat).to.eql(200);
            //});
            //
            //describe('For: createAdventure()', function() {
            //    //TODO: do we need to test these?
            //    it('Should: set adventure.user_ids = _.map(adventure.users, \'id\');', function() {
            //        delete adventure.users;
            //        delete adventure.vm;
            //        delete adventure.map;
            //
            //        vm.createAdventure();
            //        $rootScope.$apply();
            //
            //        expect(adventure.users).to.be.true();
            //        expect(adventure.vm).to.be.true();
            //        expect(adventure.map).to.be.true();
            //    });
            //
            //    it('Should: set adventure.private = !adventure.public', function() {
            //        delete adventure.public;
            //
            //        vm.createAdventure();
            //
            //        expect(adventure.public).to.be.true();
            //    });
            //
            //    it('Should: set adventure.lat = !adventure.map_lat', function() {
            //        delete adventure.displayMarker;
            //
            //        vm.createAdventure();
            //
            //        expect(adventure.displayMarker).to.be.true();
            //    });
            //
            //    it('Should: set adventure.lon = !adventure.map_lon', function() {
            //        delete adventure.displayMarker;
            //
            //        vm.createAdventure();
            //
            //        expect(adventure.displayMarker).to.be.true();
            //    });
            //});
        });

        describe('For: createAdventure()', function() {
            beforeEach(function() {
                vm.users = [
                    {
                        id: 'Test01'
                    },
                    {
                        id: 'Test02'
                    }
                ];

                $rootScope.$apply();

                sinon.spy(adventures, 'create');
            });

            it('Should call adventures.create()', function() {
                vm.createAdventure();
                $rootScope.$apply();

                expect(adventures.create.called).to.be.true();
            });
        });
    });
})();
