(function() {
    'use strict';

    angular
        .module('yazda.components.adventureEdit')
        .directive('adventureEdit', adventureItem);

    function adventureItem() {
        return {
            templateUrl:  'app/components/adventure-edit/adventure-edit.html',
            restrict:     'EA',
            scope:        {
                adventure: '='
            },
            controller:   AdventureEditController,
            controllerAs: 'ctrl'
        };
    }

    function AdventureEditController($scope, adventures, $ionicHistory, $ionicPopup, AnalyticsFactory, GeoCoder, account, $ionicPlatform) {
        var vm = this;
        var map;

        $scope.cancelAdventure = cancelAdventure;
        $scope.disableTap = disableTap;
        vm.placeChanged = placeChanged;
        vm.setMarker = setMarker;

        $ionicPlatform.ready(function() {
            account.me().then(function(data) {
                vm.me = data.user;
            });
        });

        $scope.$on('mapInitialized', mapInit);

        function setMarker(e) {
            GeoCoder.geocode({'location': e.latLng})
                .then(function(result, status) {
                    $scope.adventure.lat = result[0].geometry.location.lat();
                    $scope.adventure.lon = result[0].geometry.location.lng();
                    $scope.adventure.address = result[0].formatted_address;
                });
        }

        function placeChanged() {
            GeoCoder.geocode({address: $scope.adventure.address})
                .then(function(result) {
                    $scope.adventure.lat = result[0].geometry.location.lat();
                    $scope.adventure.lon = result[0].geometry.location.lng();
                });
        }

        function mapInit(event, map) {
            vm.map = map;
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');
            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');
            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('edit-places').blur();
            });
        }

        function cancelAdventure(id) {
            $ionicPopup.show({
                template: '<p>Are you sure you want to cancel this event. You will not be able to reopen it</p>',
                scope:    $scope,
                buttons:  [
                    {text: 'Cancel'},
                    {
                        text:  '<b>Cancel Adventure</b>',
                        type:  'button-assertive',
                        onTap: function(e) {
                            adventures
                                .destroy(id)
                                .then(function() {
                                    AnalyticsFactory
                                        .track('adventures',
                                            'delete-adventure',
                                            $scope.adventure);
                                    $ionicHistory.clearCache().then(function() {
                                        $ionicHistory.goBack();
                                    });
                                });

                        }
                    }
                ]
            });
        }
    }
})();
