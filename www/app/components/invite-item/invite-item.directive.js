(function() {
    'use strict';

    angular
        .module('yazda.components.inviteItem')
        .directive('inviteItem', inviteItem);

    function inviteItem() {
        return {
            restrict:    'E',
            scope:       {
                invite:   '=',
                callback: '&'
            },
            controller:  InviteItemController,
            templateUrl: 'app/components/invite-item/invite-item.html'
        };
    }

    InviteItemController.$inject = ['$scope'];

    function InviteItemController($scope) {
        var now = moment();

        $scope.startTime = startTime;

        function startTime(time) {
            if(moment(time).isBefore(now)) {
                return 'Now';
            } else {
                return moment(time).format('h:mm A');
            }
        }
    }
})();
