(function () {
    'use strict';

    describe('Component: invite-item', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.inviteItem');
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache'
            );

        });

        beforeEach(function () {
            var html = angular.element('<invite-item></invite-item>');

            $templateCache.put('app/components/invite-item/invite-item.html', '');
            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Opens the invite', function () {
            expect(element).to.be.ok();
        });

        describe('Function: fromNow(time)', function () {
            it('Should return moment(time).fromNow', function () {
                var time = '11:00',
                    output = scope.fromNow();

                expect(output).to.eq('a few seconds ago');
            });
        });

        describe('fromNow()', function () {
            it('Should return a few seconds ago', function () {
                var output = scope.fromNow();

                expect(output).to.eq('a few seconds ago');
            });

            it('Should return a day ago when given yesterdays date', function () {
                var time = moment().subtract(1, 'days'),
                    output = scope.fromNow(time);

                expect(output).to.eq('a day ago');
            });

            it('Should return a week ago when given a week old date', function () {
                var time = moment().subtract(7, 'days'),
                    output = scope.fromNow(time);

                expect(output).to.eq('7 days ago');
            });
        });
    });
})();
