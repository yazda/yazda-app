(function() {
    'use strict';

    angular
        .module('yazda.components.clubEdit')
        .directive('clubEdit', clubEdit);

    function clubEdit() {
        return {
            templateUrl:      'app/components/club-edit/club-edit.html',
            restrict:         'EA',
            controller:       ClubEditController,
            controllerAs:     'ctrl',
            bindToController: true
        };
    }

    ClubEditController.$inject = ['clubs', '$stateParams', '$ionicHistory', '$cordovaActionSheet', '$cordovaCamera', 'account','$ionicPlatform'];

    function ClubEditController(clubs, $stateParams, $ionicHistory, $cordovaActionSheet, $cordovaCamera, account,$ionicPlatform) {
        var vm = this;

        $ionicPlatform.ready(getClub);

        //TODO delete club
        vm.disableTap = disableTap;
        vm.updateClub = updateClub;
        vm.chooseWayToUploadPhoto = chooseWayToUploadPhoto;
        vm.background = background;

        function background() {
            if(vm.club && vm.club.banner_image_thumb_url) {
                return 'url( ' + vm.club.banner_image_thumb_url + ')';
            }
        }

        function updateClub() {
            return clubs.update(vm.club)
                .then(function() {
                    return account
                        .me(true)
                        .finally(function() {
                            $ionicHistory
                                .clearCache()
                                .then(function() {
                                    $ionicHistory.goBack();
                                });
                        });

                });
        }

        function chooseWayToUploadPhoto(width, height, type) {
            var config = {
                quality:            100,
                destinationType:    Camera.DestinationType.DATA_URL,
                allowEdit:          true,
                encodingType:       Camera.EncodingType.PNG,
                targetWidth:        Number(width),
                targetHeight:       Number(height),
                popoverOptions:     CameraPopoverOptions,
                saveToPhotoAlbum:   true,
                correctOrientation: true
            };

            $cordovaActionSheet.show({
                    title:                     'How do you want to pick your picture',
                    buttonLabels:              ['Camera', 'Photo Library'],
                    addCancelButtonWithLabel:  'Cancel',
                    androidEnableCancelButton: true
                })
                .then(function(index) {
                    if(index === 1) {
                        config.sourceType = Camera.PictureSourceType.CAMERA;
                    } else if(index === 2) {
                        config.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
                    }

                    if(index !== 3) {
                        $cordovaCamera.getPicture(config)
                            .then(function(data) {
                                if(type === 'avatar') {
                                    clubs
                                        .updateProfileImage(vm.club.id, data)
                                        .then(function() {
                                            getClub();
                                        });
                                } else if(type === 'banner') {
                                    clubs.updateBannerImage(vm.club.id, data)
                                        .then(function() {
                                            getClub();
                                        });
                                }

                            });
                    }
                });
        }

        function getClub() {
            return clubs
                .show($stateParams.id)
                .then(setClubData);
        }

        function setClubData(data) {
            var club = data.club;

            vm.club = club;

            return vm.club;
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');
            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');
            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('club-places').blur();
            });
        }
    }
})();
