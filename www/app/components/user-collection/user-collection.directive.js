(function() {
    'use strict';

    angular
        .module('yazda.components.userCollection')
        .directive('userCollection', userCollection);

    function userCollection() {
        return {
            restrict:    'EA',
            scope:       {
                invites: '=',
                users:   '=',
                owner:   '='
            },
            templateUrl: 'app/components/user-collection/user-collection.html'
        };
    }
})();
