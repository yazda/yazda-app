(function () {
    'use strict';

    describe('Component: userCollection', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.userCollection');
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$templateCache'
            );
        });

        beforeEach(function () {
            $rootScope = $rootScope.$new();
            var html = angular.element('<user-collection></user-collection>');
            $templateCache.put('app/components/user-collection/user-collection.html', '');
            element = $compile(html)($rootScope);
            $rootScope.$digest(element);
            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open user-collection', function() {
            //TODO test that invites/owner/users can be passed in via attr's
            //expect(scope.TestInvites).to.eq('TestInvites');
        });
    });
})();
