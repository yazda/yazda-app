(function() {
    'use strict';

    angular
        .module('yazda.components.adventureRespondTo')
        .directive('adventureRespondTo', adventureRespondTo);

    function adventureRespondTo() {
        return {
            restrict:    'E',
            scope:       {
                adventure: '=',
                success:   '&'
            },
            controller:  AdventureRespondToController,
            templateUrl: 'app/components/adventure-respond-to/adventure-respond-to.html'
        };
    }

    AdventureRespondToController.$inject = ['$scope', 'invites',
        'AnalyticsFactory'];

    function AdventureRespondToController($scope, invites, AnalyticsFactory) {
        $scope.isAttending = isAttending;
        $scope.isNotAttending = isNotAttending;
        $scope.hasPassedOrOwner = hasPassedOrOwner;
        $scope.accept = accept;
        $scope.reject = reject;

        function reject(adventure) {
            invites.reject(adventure.id)
                .then(function() {
                    AnalyticsFactory
                        .track('adventures', 'reject-invite', adventure);

                    $scope.adventure.is_attending = false;
                    $scope.adventure.has_responded = true;

                    if(typeof $scope.success === 'function') {
                        $scope.success();
                    }
                });
        }

        function accept(adventure) {
            invites.accept(adventure.id)
                .then(function() {
                    AnalyticsFactory
                        .track('adventures', 'accept-invite', adventure);

                    $scope.adventure.is_attending = true;
                    $scope.adventure.has_responded = true;

                    if(typeof $scope.success === 'function') {
                        $scope.success();
                    }
                });
        }

        function hasPassedOrOwner(adventure) {
            return adventure && (adventure.is_owner || moment(adventure.start_time) < moment());
        }

        function isAttending(adventure) {
            return adventure && (adventure.is_attending && adventure.has_responded);
        }

        function isNotAttending(adventure) {
            return adventure && (adventure.has_responded && !adventure.is_attending);
        }
    }
})();
