(function () {
    'use strict';

    describe('Component: adventure-respond-to', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.adventureRespondTo', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                'invites'
            );

            bard.mockService(invites, mocks.InvitesMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<adventure-respond-to></adventure-respond-to>');

            $templateCache.put('app/components/adventure-respond-to/adventure-respond-to.html', '');
            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Opens the adventure list', function () {
            expect(element).to.be.ok();
        });

        describe('For: reject(adventure)', function () {
            it('adds .is_attending = false to scope.adventure', function () {
                scope.adventure = {is_attending: true};

                scope.reject({id: 1});
                scope.$apply();

                expect(scope.adventure.is_attending).to.be.false();
            });

            it('adds .has_responded = true to scope.adventure', function () {
                scope.adventure = {has_responded: true};

                scope.reject({id: 1});
                scope.$apply();

                expect(scope.adventure.has_responded).to.be.true()
            });
        });

        describe('For: accept(adventure)', function () {
            it('adds .is_attending = true to scope.adventure', function () {
                scope.adventure = {is_attending: true};

                scope.accept({id: 1});
                scope.$apply();

                expect(scope.adventure.is_attending).to.be.true();
            });

            it('adds .has_responded = true to scope.adventure', function () {
                scope.adventure = {has_responded: true};

                scope.accept({id: 1});
                scope.$apply();

                expect(scope.adventure.has_responded).to.be.true()
            });
        });

        describe('hasPassedOrOwner(adventure)', function() {
            it('returns true if one of conditions is true', function () {
                var adventure = {
                    is_owner: false,
                    start_time: moment().subtract(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.true();
            });

            it('returns true if one of conditions is true', function () {
                var adventure = {
                    is_owner: true,
                    start_time: moment().subtract(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.true();
            });

            it('returns false if adventure properties are false', function () {
                var adventure = {
                    is_owner: false,
                    start_time: moment().add(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.false();
            });

            it('returns false if adventure is undefined', function () {
                var adventure = false;

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.false();
            });
        });

        describe('isAttending(adventure)', function() {
           it('returns true if all conditions are true', function() {
               var adventure = {
                   is_attending: true,
                   has_responded: true
               },
                   result = scope.isAttending(adventure);

               expect(result).to.be.true();
            });

            it('returns false if one condition is false', function() {
                var adventure = {
                        is_attending: true,
                        has_responded: false
                    },
                    result = scope.isAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if all conditions are false', function() {
                var adventure = {
                        is_attending: false,
                        has_responded: false
                    },
                    result = scope.isAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if adventure is undefined', function() {
                var adventure = false,
                    result = scope.isAttending(adventure);

                expect(result).to.be.false();
            });
        });

        describe('isNotAttending(adventure)', function() {
            it('returns true if adventure.has_responded and !is_attending', function() {
                var adventure = {
                        has_responded: true,
                        is_attending: false
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.true();
            });

            it('returns false if one condition is false', function() {
                var adventure = {
                        has_responded: false,
                        is_attending: false
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if all conditions are false', function() {
                var adventure = {
                        has_responded: false,
                        is_attending: true
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if adventure is undefined', function() {
                var adventure = false,
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.false();
            });
        });

    });

})();
