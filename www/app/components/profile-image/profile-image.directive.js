(function() {
    'use strict';

    angular
        .module('yazda.components.profileImage')
        .directive('profileImage', profileImage);

    function profileImage() {
        return {
            templateUrl:      'app/components/profile-image/profile-image.html',
            restrict:         'EA',
            controller:       ProfileImageCtrl,
            controllerAs:     'ctrl',
            bindToController: true,
            scope:            {
                user: '=',
                size: '@'
            }
        };
    }

    function ProfileImageCtrl() {
        var vm = this;
        vm.profile_image = profile_image;
        vm.css_class = css_class;

        function profile_image() {
            if(vm.user) {
                if(vm.size === 'profile') {
                    return vm.user.profile_image_url;
                } else {
                    return vm.user.profile_image_thumb_url;
                }
            }
        }

        function css_class() {
            if(vm.user) {
                if(vm.size === 'profile') {
                    return 'lg';
                } else {
                    return 'sm';
                }
            }
        }
    }
})();
