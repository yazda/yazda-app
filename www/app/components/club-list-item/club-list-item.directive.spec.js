(function() {
    'use strict';

    describe('Directive: adventureListItem', function() {
        var scope,
            element;

        beforeEach(function() {
            bard.appModule('yazda.components.adventureListItem');
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache'
            );
        });

        beforeEach(function () {
            var html = angular.element('<adventure-list-item></adventure-list-item>');

            $templateCache.put('app/components/adventure-list-item/adventure-list-item.html', '');
            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Opens the adventure list', function () {
            expect(element).to.be.ok();
        });

        describe('filterPendings(newVal)', function() {
            it('Set: when adventure.attendings changes then scope.attending = newVal', function() {
                var newVal = 'abcdefg';

                scope.adventure = {attendings: newVal};
                scope.$apply();

                expect(scope.attending).to.be.eq('abcd');
            });
        });

        describe('startTime(time)', function() {
            describe('If: moment(time).isBefore(now)', function() {
                it('Should: return \'Now\'', function() {
                    var output = scope.startTime('10/01/2015');

                    expect(output).to.eq('Now');
                });

                it('Else: !moment(time).isBefore(now)', function() {
                    var tomorrow = moment().add(7, 'days'),
                        output = scope.startTime(tomorrow);

                    expect(output).to.eq(tomorrow.format('h:mm A'));
                });
            });
        });
    });
})();
