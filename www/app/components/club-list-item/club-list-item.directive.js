(function() {
    'use strict';

    angular
        .module('yazda.components.clubListItem')
        .directive('clubListItem', clubListItem);

    function clubListItem() {
        return {
            restrict:    'E',
            scope:       {
                club: '='
            },
            templateUrl: 'app/components/club-list-item/club-list-item.html'
        };
    }
})();
