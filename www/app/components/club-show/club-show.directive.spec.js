(function() {
    'use strict';

    describe('Directive: userShow', function() {
        var element,
            html;

        beforeEach(function() {
            bard.appModule('yazda.components.userShow', 'yazda.yazda-api', 'yazda.controllers', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function() {
                });
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$httpBackend',
                '$compile',
                '$q',
                '$rootScope',
                '$controller'
            );

        });

        beforeEach(function() {
            var html = angular.element('<user-show></user-show>');

            $rootScope = $rootScope.$new();

            $httpBackend.when('GET', 'app/components/user-show/user-show.html').respond('');
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: expect element to be ok', function() {
            expect(element).to.be.ok();
            $httpBackend.flush();
        });
    });
})();
