(function() {
    'use strict';

    angular
        .module('yazda.components.clubShow')
        .directive('clubShow', user);

    function user() {
        return {
            restrict:    'EA',
            templateUrl: 'app/components/club-show/club-show.html',
            controller:  clubCtrl,
            scope:       {
                club: '='
            }
        };
    }

    clubCtrl.$inject = ['$scope', '$ionicHistory', '$state', '$stateParams', 'clubs'];

    function clubCtrl($scope, $ionicHistory, $state, $stateParams, clubs) {
        $scope.background = background;
        $scope.viewMembers = viewMembers;
        $scope.join = join;
        $scope.leave = leave;
        $scope.openWaiver = openWaiver;

        setTimeout(function() {
            $scope.$watch(function() {
                return $scope.club.adventure_invite || $scope.club.chat;
            }, function(value) {
                if($scope.club.is_member){
                    clubs
                        .updateNotification($scope.club.id,
                            {
                                adventure_invite: $scope.club.adventure_invite,
                                //chat:             $scope.club.chat TODO
                            });
                }
            });
        }, 500);

        function join(id) {
            clubs.join(id)
                .then(function(response) {
                    $scope.club = response.club;
                });
        }

        function leave(id) {
            clubs.leave(id)
                .then(function(response) {
                    $scope.club.is_member = false;
                });
        }

        function openWaiver() {
            window.open($scope.club.waiver_url, '_blank', 'location=yes');
        }

        function viewMembers() {
            if($ionicHistory.currentStateName() === 'loggedIn.adventure-clubs-show') {
                $state.go('loggedIn.adventure-club-members-index', {
                    id:          $stateParams.id,
                    adventureId: $stateParams.adventureId
                });
            } else if($ionicHistory.currentStateName() === 'loggedIn.profile-show' ||
                $ionicHistory.currentStateName() === 'loggedIn.profile-clubs-show') {
                $state.go('loggedIn.profile-club-members-index', {id: $stateParams.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.invite-clubs-show') {
                $state.go('loggedIn.invite-club-members-index', {id: $stateParams.id});
            } else if($ionicHistory.currentStateName() === 'loggedIn.club-clubs-show') {
                $state.go('loggedIn.club-club-members-index', {id: $stateParams.id});
            }
        }

        function background(club) {
            if(club && club.banner_image_thumb_url) {
                return 'url( ' + club.banner_image_thumb_url + ')';
            }
        }
    }
})();
