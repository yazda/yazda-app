(function(){
    'use strict';

    angular
        .module('yazda.components.userItem')
        .directive('userItem', UserItemDirective);

    function UserItemDirective() {
        return {
            restrict: 'EA',
            templateUrl: 'app/components/user-item/user-item.html',
            link: link,
            scope: {
                user: '=',
                show: '@',
                checked: '&'
            }
        };

        function link($scope) {
            $scope.friendDisplay = friendDisplay;

            function friendDisplay() {
                if($scope.user.following) {
                    return  '<i class="ion-ios-people"></i> Friends';
                } else if($scope.user.role) {
                    return  '<i class="ion-ios-people"></i> ' + _.capitalize($scope.user.role);
                }
            }
        }
    }
})();
