(function() {
    'use strict';

    describe('Directive: userItem', function() {
        var element,
            scope,
            html;

        beforeEach(function() {
            bard.appModule('yazda.components.userItem', 'yazda.yazda-api', 'yazda.controllers', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function() {
                });
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$httpBackend',
                '$compile',
                '$q',
                '$rootScope',
                '$templateCache',
                '$controller'
            );

        });

        beforeEach(function() {
            var html = angular.element('<user-item></user-item>');

            $rootScope = $rootScope.$new();

            $templateCache.put('app/components/user-item/user-item.html', '');
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: expect element to be ok', function() {
            expect(element).to.be.defined;
        });
    });
})();
