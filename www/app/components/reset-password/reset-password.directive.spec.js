(function() {
    'use strict';

    describe('Directive: resetPassword', function() {
        var scope,
            element;

        beforeEach(function() {

            bard.appModule('yazda.components.resetPassword', 'yazda.yazda-api', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function() {
                });
                $urlRouterProvider.deferIntercept();
            });

            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$ionicPopup',
                '$ionicPlatform',
                'oauthClient'
            );

            bard.mockService($ionicPopup, mocks.IonicPopupMock($q));
            bard.mockService($ionicPlatform, mocks.IonicPlatformMock());
            bard.mockService(oauthClient, mocks.OauthClientMock($q));
        });

        beforeEach(function() {
            var html = angular.element('<reset-password></reset-password>');

            scope = $rootScope.$new();

            element = $compile(html)(scope);

            scope.$digest(element);

            sinon.spy($ionicPlatform, 'ready');
            sinon.spy($ionicPopup, 'show');
        });

        bard.verifyNoOutstandingHttpRequests();

        describe('When $ionicPlatform.ready', function() {

            it('Should set function showForgotPassword to the scope', function() {
                expect(typeof scope.showForgotPassword).to.eq('function');
            });

            describe('Function showForgotPassword', function() {

                it('Should call $ionicPopup.show', function() {
                    scope.showForgotPassword();
                    $rootScope.$apply();
                    expect($ionicPopup.show.called).to.be.true();
                });

                it('Should set the pop up to show', function() {
                    scope.showForgotPassword();
                    $rootScope.$apply();
                    expect($ionicPopup.read('showPopUp').title).to.eq('Forgot your password?');
                });

                describe('onTap: function(e)', function() {
                    describe('if scope.data.email', function() {
                        it('Should run forgotPasswordResponse(scope.data.email)', function() {
                            scope.showForgotPassword();
                            $rootScope.$apply();
                            console.log($ionicPopup.show());





                        });
                    });

                });
            });
        });
    });
})();
