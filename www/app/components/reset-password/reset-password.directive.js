(function() {
    'use strict';

    angular
        .module('yazda.components.resetPassword')
        .directive('resetPassword', resetPassword);

    resetPassword.$inject = ['$ionicPopup', '$ionicPlatform', 'oauthClient',
        'AnalyticsFactory'];
    function resetPassword($ionicPopup, $ionicPlatform, oauthClient,
                           AnalyticsFactory) {
        return {
            link:     link,
            restrict: 'EA',
            template: '<button class="button button-clear button-energized" ng-click="showForgotPassword()">Forgot password?</button>'
        };

        function link(scope, element, attrs) {

            $ionicPlatform.ready(function() {
                scope.showForgotPassword = showForgotPassword;
                scope.data = {};

                function showForgotPassword() {

                    $ionicPopup.show({
                        template: '<input type="email" ng-model="data.email" />',
                        title:    'Forgot your password?',
                        subTitle: 'Enter your email address to request a password reset.',
                        scope:    scope,
                        buttons:  [
                            {text: 'Cancel'},
                            {
                                text:  '<b>Send Request</b>',
                                type:  'button-energized',
                                onTap: function(e) {
                                    if(!scope.data.email) {
                                        e.preventDefault();
                                    } else {
                                        forgotPasswordResponse(scope.data.email);
                                    }
                                }
                            }
                        ]
                    });

                    //TODO: test that the $ionicPopup can call this function
                    function forgotPasswordResponse(email) {
                        oauthClient.forgotPassword(email)
                            .finally(function() {
                                $ionicPopup
                                    .alert({
                                        title:    'Password Reset Sent',
                                        template: 'Please check your email to reset your password.',
                                        okType:   'button-energized'
                                    });
                            });
                    }
                }
            });
        }
    }
})();
