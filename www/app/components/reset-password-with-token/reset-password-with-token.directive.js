(function(){
    'use strict';

    angular
        .module('yazda.components.resetPasswordWithToken')
        .directive('resetPasswordWithToken', resetPasswordWithToken);

    function resetPasswordWithToken() {
        return {
            restrict: 'EA',
            controller: ForgotPasswordController,
            controllerAs: 'user',
            bindToController: true,
            templateUrl: 'app/components/reset-password-with-token/reset-password-with-token.html'
        };

    }

    ForgotPasswordController.$inject = ['$state', '$stateParams', 'oauthClient', '$ionicPopup'];

    function ForgotPasswordController($state, $stateParams, oauthClient, $ionicPopup) {
        var vm = this;

        vm.token = $stateParams.reset_password_token;
        vm.changePassword = changePassword;

        function changePassword(user) {
            oauthClient.changeForgottenPassword(vm.token, user.password, user.passwordConfirmation)
                .then(function() {
                    $ionicPopup.alert({
                            title:    'Password Changed Successfully',
                            template: 'Please login with your new password',
                            okType:   'button-energized'
                        }).then(function() {
                            $state.go('login');
                        });
                });
        }
    }
})();
