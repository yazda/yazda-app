(function () {
    'use strict';

    describe('Directive: resetPasswordWithToken', function () {
        var vm,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.resetPasswordWithToken', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                '$state',
                '$stateParams',
                'oauthClient',
                '$ionicPopup'
            );

            bard.mockService($state, mocks.StateMock($q));
            bard.mockService($stateParams, mocks.StateParamsMock($q));
            bard.mockService(oauthClient, mocks.OauthClientMock($q));
            bard.mockService($ionicPopup, mocks.IonicPopupMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<reset-password-with-token></reset-password-with-token>');

            $templateCache.put('app/components/reset-password-with-token/reset-password-with-token.html', '');

            $rootScope = $rootScope.$new();

            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            vm = element.controller('resetPasswordWithToken');

            sinon.spy(oauthClient, 'changeForgottenPassword');
            sinon.spy($state, 'go');
        });

        bard.verifyNoOutstandingHttpRequests();

        describe('changePassword(user)', function () {

            var user;

            beforeEach(function() {
                vm.token = "TestToken";
                user = {
                    password: 'TestPassword',
                    passwordConfirmation: true
                };

                vm.changePassword(user);

                $rootScope.$apply();
            });

            it('calls oauthClient.changeForgottenPassword()', function () {
                expect(oauthClient.changeForgottenPassword.calledWith(
                    vm.token,
                    user.password,
                    user.passwordConfirmation
                )).to.be.true();
            });
            it('calls / sets an ionicPopup.alert', function() {
               expect($ionicPopup.read('ionicPopupAlert')).to.eql({
                   title:    'Password Changed Successfully',
                   template: 'Please login with your new password',
                   okType:   'button-energized'
               });
            });
            it('calls routes to state: "login"', function() {
               expect($state.go.calledWith('login')).to.be.true();
            });
        });
    });
})();
