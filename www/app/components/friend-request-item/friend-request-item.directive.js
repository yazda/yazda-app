(function() {
    'use strict';

    angular
        .module('yazda.components.friendRequestItem')
        .directive('friendRequestItem', friendRequestItem);

    function friendRequestItem() {
        return {
            restrict:    'E',
            replace:     true,
            scope:       {
                invite:  '=',
                success: '&'
            },
            controller:  FriendRequestItemController,
            templateUrl: 'app/components/friend-request-item/friend-request-item.html'
        };
    }

    FriendRequestItemController.$inject = ['$scope', 'friendships',
        'AnalyticsFactory'];

    function FriendRequestItemController($scope, friendships) {
        var now = moment();

        $scope.fromNow = fromNow;
        $scope.accept = accept;
        $scope.reject = reject;

        function fromNow(time) {
            return moment(time).fromNow();
        }

        function accept(id) {
            friendships.accept(id)
                .then(function() {
                    if(typeof $scope.success === 'function') {
                        $scope.success();
                    }
                });
        }

        function reject(id) {
            friendships.reject(id)
                .then(function() {
                    if(typeof $scope.success === 'function') {
                        $scope.success();
                    }
                });
        }
    }

})();
