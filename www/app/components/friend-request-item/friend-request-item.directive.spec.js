(function () {
    'use strict';

    describe('Directive: friendRequestItem', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.friendRequestItem', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                '$httpBackend',
                'friendships'
            );

            bard.mockService(friendships, mocks.FriendShipsMock($q));
        });


        beforeEach(function () {
            var html = angular.element('<friend-request-item invite="MockInvite" success="Mock"></friend-request-item>');

            $templateCache.put('app/components/friend-request-item/friend-request-item.html', '<div></div>');

            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);
            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: open friendRequestItem directive', function() {
            expect(scope).to.be.ok();
            expect(element).to.be.ok();
        });

        describe('Function: fromNow(time)', function() {
            it('returns amount of time from var time to now', function() {
                expect(scope.fromNow(Date.now())).to.eq('a few seconds ago');
            });
        });

        describe('Function: accept / reject', function() {

            beforeEach(function() {
                scope.success = function() {
                    return;
                };
                $rootScope.$apply();
                sinon.spy(scope, 'success');
            });

            it('calls friendships.accept(id)', function(){
                scope.accept('TestId');
                $rootScope.$apply();
                expect(scope.success.called).to.be.true();
            });

            it('calls friendships.reject(id)', function() {
                scope.reject('testId');
                $rootScope.$apply();
                expect(scope.success.called).to.be.true();
            });
        });
    });
})();
