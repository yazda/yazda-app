(function() {
    'use strict';

    angular
        .module('yazda.components.registerForm')
        .directive('registerForm', registerForm);

    function registerForm() {
        return {
            templateUrl:  'app/components/register-form/register-form.html',
            restrict:     'EA',
            controller:   RegisterCtrl,
            controllerAs: 'register'
            //bindToController: true
        };
    }

    RegisterCtrl.$inject = ['$rootScope', 'oauthClient', '$ionicPlatform'];

    function RegisterCtrl($rootScope, oauth, $ionicPlatform) {
        var vm = this;

        vm.user = {};
        $ionicPlatform.ready(function() {
            vm.register = register;
        });

        function register(user) {
            oauth
                .register(user);
        }
    }
})();
