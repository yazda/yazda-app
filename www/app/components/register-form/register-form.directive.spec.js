(function () {
    'use strict';

    describe('Directive: Components: registerForm', function () {
        var vm,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.registerForm', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$templateCache',
                '$controller',
                'oauthClient'
            );
            bard.mockService(oauthClient, mocks.OauthClientMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<register-form></register-form>');

            $rootScope = $rootScope.$new();
            $templateCache.put('app/components/register-form/register-form.html', '');
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            vm = element.controller('registerForm');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Opens the register form', function () {
            expect(element).to.be.ok();
        });

        describe('For: register()', function(){
            it('Calls oauth.register', function () {
                var user = {
                    email: 'TestEmail@test.com',
                    password: 1234
                };
                vm.register(user);

                $rootScope.$apply();

                expect(oauthClient.register.calledWith(user)).to.be.true();
            });
        });
    });
})();
