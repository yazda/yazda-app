(function() {
    'use strict';

    angular
        .module('yazda.components.adventureListItem')
        .directive('adventureListItem', adventureListItem);

    function adventureListItem() {
        return {
            restrict:    'E',
            scope:       {
                adventure: '='
            },
            controller:  AdventureListItemController,
            templateUrl: 'app/components/adventure-list-item/adventure-list-item.html'
        };
    }

    AdventureListItemController.$inject = ['$scope'];

    function AdventureListItemController($scope) {
        var now = moment();

        $scope.spotsLeft = spotsLeft;
        $scope.startTime = startTime;
        $scope.$watch('adventure.attendings | limitTo : 4', filterPendings, true);

        function spotsLeft() {
            var limit = $scope.adventure.reservation_limit;
            var attendingCount = $scope.adventure.attendings.length;

            if(limit >= attendingCount) {
                return limit - attendingCount + ' Spots Left!';
            }
        }

        function filterPendings(newVal) {
            $scope.attending = newVal;
        }

        function startTime(time) {
            if(moment(time).isBefore(now)) {
                return 'Now';
            } else {
                return moment(time).format('h:mm A');
            }
        }
    }
})();
