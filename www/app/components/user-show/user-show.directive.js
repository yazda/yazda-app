(function(){
    'use strict';

    angular
        .module('yazda.components.userShow')
        .directive('userShow', user);

    function user() {
        return {
            restrict: 'EA',
            templateUrl: 'app/components/user-show/user-show.html',
            controller: 'UserCtrl',
            controllerAs: 'user',
            bindToController: true,
            scope: {
                profile: '='
            }
        };
    }
})();
