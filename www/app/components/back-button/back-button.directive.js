(function() {
    'use strict';

    angular
        .module('yazda.components.backButton')
        .directive('backButton', BackButtonDirective)
        .controller('BackButtonDirectiveController', BackButtonDirectiveController);

    function BackButtonDirective() {
        return {
            restrict: 'E',
            replace: true,
            controller: BackButtonDirectiveController,
            template: [
                '<ion-nav-buttons> ',
                '<button class="button back-button mlz-back-button buttons button-clear header-item" ng-if="state.show" ng-click="goBack()">',
                '<i class="icon ion-ios-arrow-back"></i>',
                ' Back',
                '</button>',
                '</ion-nav-buttons>'
            ].join('')
        };
    }

    BackButtonDirectiveController.$inject = ['$scope', '$state', '$ionicHistory', '$ionicViewSwitcher'];
    function BackButtonDirectiveController($scope, $state, $ionicHistory, $ionicViewSwitcher) {

        var ctrl = this;

        /**
         * Checks to see if there is a view to navigate back to
         * if we have no current back view.
         *
         * @param  {string} state The current view state
         * @return {boolean}      If there is a view we should navigate back to
         */
        this.canGoBack = function (state) {

            if (!state) {
                var history = $ionicHistory.currentStateName();
                state = $state.get(history).url;
            }

            var states = state.split('/');

            if (states.length > 1 && !$ionicHistory.backView())  {
                states.pop();
                ctrl.toState = states.join('/');
                return true;
            }
            return false;
        };

        $scope.state = {
            show: true,
            previousState: ''
        };

        $scope.goBack = function () {

            // check we can go back first
            if (!ctrl.canGoBack()) {
                return false;
            }

            // Switch this animation around so it looks like we're navigating backwards
            $ionicViewSwitcher.nextDirection('back');

            // Stop it from caching this view as one to return to
            $ionicHistory.nextViewOptions({
                historyRoot: true,
                disableBack: true
            });

            // Switch to our new previous state
            //TODO lookup state instead of this hack
            //$state.go(ctrl.toState, $state.params);
            window.location.hash = ctrl.toState;
        };

        $scope.state.show = ctrl.canGoBack();

        $scope.$on('$stateChangeStart', function (e, to) {
            $scope.state.show = ctrl.canGoBack(to.name);
        });
    }

})();
