(function () {
    'use strict';

    describe('Directive: Back-Button', function () {
        var vm,
            $scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.backButton', 'yazda.core', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$state',
                '$ionicHistory',
                '$ionicViewSwitcher'
            );

            bard.mockService($state, mocks.StateMock($q));
            bard.mockService($ionicViewSwitcher, mocks.IonicViewSwitcherMock($q));
            bard.mockService($ionicHistory, mocks.IonicHistoryMock());
        });

        beforeEach(function () {
            $scope = $rootScope.$new();

            var html = angular.element('<ion-nav-bar><back-button></back-button></ion-nav-bar>');
            element = $compile(html)($scope);

            $scope.$digest(element);

            vm = $controller('BackButtonDirectiveController', {$scope: $scope});

            //sinon.spy($ionicViewSwitcher, 'nextDirection');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: ', function () {

            expect(element).to.be.ok();
        });

        describe('canGoBack()', function () {

            describe('If:  !state', function () {

                it('Set state = $state.get(history).url', function () {
                    var state = 'Mock/Url';

                    vm.canGoBack(state);

                    expect(history.length).to.be.eq(1);
                    expect(state).to.be.eq('Mock/Url');

                });

            });

            describe('If state', function () {

                describe('if (states.length > 1 && !$ionicHistory.backView())', function () {
                    it('Should remove last item in the states', function () {
                        var state = 'Mock/Url';
                        vm.canGoBack(state);

                        expect(vm.toState).to.be.eq('Mock');
                        expect(vm.canGoBack()).to.be.true();

                    });

                    it('Else returns false', function () {

                        expect(vm.canGoBack('Mock')).to.be.false();
                    });
                });
            });


        });

        describe('$scope.goBack', function () {
            it('if !ctrl.canGoBack() within $scope.goBack', function() {

                $state.get = function(history) {
                    return {
                        url: 'Mock'
                    };
                };
                //$rootScope.$apply();

                expect($scope.goBack()).to.be.false();
            });

            it('if ctrl.canGoBack() within $scope.goBack', function() {
                //$state.get = function(history) {
                //    return {
                //        url: 'Mock/Url'
                //    };
                //};
                $scope.goBack();
                $rootScope.$apply();
                expect($ionicViewSwitcher.nextDirection.calledWith('back')).to.be.true();
            });

            it('set ctrl.canGoBack() to $scope.state.show', function () {
                $rootScope.$broadcast('$stateChangeStart', {name: 'Test/state/'})
                $rootScope.$apply();
                expect($scope.state.show).to.be.true();
            });
        });

    });
})();
