(function() {
    'use strict';

    angular
        .module('yazda.components.adventureShow')
        .directive('adventureShow', adventureItem);

    function adventureItem() {
        return {
            restrict:    'E',
            scope:       {
                adventure: '=data'
            },
            controller:  AdventureShowController,
            templateUrl: 'app/components/adventure-show/adventure-show.html'
        };
    }

    AdventureShowController.$inject = ['$scope', '$cordovaCalendar',
        '$cordovaLaunchNavigator', 'invites', 'AnalyticsFactory'];

    function AdventureShowController($scope, $cordovaCalendar,
                                     $cordovaLaunchNavigator, invites,
                                     AnalyticsFactory) {
        var now = moment();

        $scope.addToCalendar = addToCalendar;
        $scope.openInMaps = openInMaps;
        $scope.formatTime = formatTime;
        $scope.isAttending = isAttending;
        $scope.isNotAttending = isNotAttending;
        $scope.toCaps = toCaps;
        $scope.hasPassedOrOwner = hasPassedOrOwner;
        $scope.accept = accept;
        $scope.reject = reject;
        $scope.backgroundStyle = backgroundStyle;
        $scope.openWaiver = openWaiver;
        $scope.spotsLeft = spotsLeft;

        function spotsLeft() {
            var limit = $scope.adventure.reservation_limit;
            var attendingCount = $scope.adventure.attendings.length;

            if(limit >= attendingCount) {
                return limit - attendingCount;
            } 
        }

        function openWaiver() {
            window.open($scope.adventure.club.waiver_url, '_blank','location=yes');
        }

        function backgroundStyle(adventure) {
            if(adventure) {
                var club = adventure.club;

                if(club && club.banner_image_thumb_url) {
                    return 'box-shadow: inset 0 0 0 1000px' +
                        ' rgba(39,39,39,0.8) !important;background-position:' +
                        ' center;background-image:' +
                        ' url(' + club.banner_image_thumb_url + ');background-size: cover;';
                }
            }
        }

        function addToCalendar(adventure) {
            var data = {
                title:     'Yazda - ' + adventure.name,
                location:  adventure.address,
                notes:     adventure.description,
                startDate: adventure.start_time,
                endDate:   adventure.end_time
            };

            $cordovaCalendar.createEventInteractively(data);
        }

        function openInMaps(adventure) {
            $cordovaLaunchNavigator
                .navigate(adventure.address, null, {
                    transportMode: 'driving'
                });
        }

        function reject(adventure) {
            invites.reject(adventure.id)
                .then(function() {
                    AnalyticsFactory
                        .track('adventures', 'accept-invite', adventure);

                    $scope.adventure.is_attending = false;
                    $scope.adventure.has_responded = true;
                });
        }

        function accept(adventure) {
            invites.accept(adventure.id)
                .then(function() {
                    AnalyticsFactory
                        .track('adventures', 'reject-invite', adventure);

                    $scope.adventure.is_attending = true;
                    $scope.adventure.has_responded = true;
                });
        }

        function hasPassedOrOwner(adventure) {
            return adventure.is_owner || moment(adventure.start_time) < moment();
        }

        function toCaps(word) {
            return (word) ? word.replace('_', ' ') : '';
        }

        function isAttending(adventure) {
            return adventure.is_attending && adventure.has_responded;
        }

        function isNotAttending(adventure) {
            return adventure.has_responded && !adventure.is_attending;
        }

        function formatTime(time) {
            return moment(time).format('MMM D [at] h:mm A');
        }
    }
})
();
