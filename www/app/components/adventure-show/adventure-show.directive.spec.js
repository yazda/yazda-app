(function() {
    'use strict';

    describe('Directive: adventureShow', function() {
        var vm,
            element,
            scope;

        beforeEach(function() {
            bard.appModule('yazda.components.adventureShow', 'yazda.yazda-api');
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                '$cordovaCalendar',
                'invites'
            );

            bard.mockService(invites, mocks.InvitesMock($q));
            bard.mockService($cordovaCalendar, mocks.CordovaCalendarMock($q));
        });

        beforeEach(function() {
            var html = angular.element('<adventure-show></adventure-show>');

            $templateCache.put('app/components/adventure-show/adventure-show.html', '');
            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open adventure-show', function() {
            expect(element).to.be.ok();
        });

        describe('Function addToCalendar()', function() {

            it('createEventInteractively()', function() {
                var adventure = {
                    name: 'Name',
                    address: '123 Easy St',
                    description: 'Adv Desc',
                    start_time: '10:00 AM',
                    end_time: '01:00 PM'
                };

                scope.addToCalendar(adventure);
                scope.$apply();
            });
        });

        describe('For: reject(adventure)', function() {
            it('adds .is_attending = false to scope.adventure', function() {
                scope.adventure = {is_attending: true};

                scope.reject({id: 1});
                scope.$apply();

                expect(scope.adventure.is_attending).to.be.false();
            });

            it('adds .has_responded = true to scope.adventure', function() {
                scope.adventure = {has_responded: true};

                scope.reject({id: 1});
                scope.$apply();

                expect(scope.adventure.has_responded).to.be.true()
            });
        });

        describe('For: accept(adventure)', function() {
            it('adds .is_attending = true to scope.adventure', function() {
                scope.adventure = {is_attending: true};

                scope.accept({id: 1});
                scope.$apply();

                expect(scope.adventure.is_attending).to.be.true();
            });

            it('adds .has_responded = true to scope.adventure', function() {
                scope.adventure = {has_responded: true};

                scope.accept({id: 1});
                scope.$apply();

                expect(scope.adventure.has_responded).to.be.true()
            });
        });

        describe('hasPassedOrOwner(adventure)', function() {
            it('returns true if one of conditions is true', function() {
                var adventure = {
                    is_owner: false,
                    start_time: moment().subtract(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.true();
            });

            it('returns true if one of conditions is true', function() {
                var adventure = {
                    is_owner: true,
                    start_time: moment().subtract(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.true();
            });

            it('returns false if adventure properties are false', function() {
                var adventure = {
                    is_owner: false,
                    start_time: moment().add(1, 'days')
                };

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.false();
            });

            it('returns false if adventure is undefined', function() {
                var adventure = false;

                var result = scope.hasPassedOrOwner(adventure);

                expect(result).to.be.false();
            });
        });

        describe('isAttending(adventure)', function() {
            it('returns true if all conditions are true', function() {
                var adventure = {
                        is_attending: true,
                        has_responded: true
                    },
                    result = scope.isAttending(adventure);

                expect(result).to.be.true();
            });

            it('returns false if one condition is false', function() {
                var adventure = {
                        is_attending: true,
                        has_responded: false
                    },
                    result = scope.isAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if all conditions are false', function() {
                var adventure = {
                        is_attending: false,
                        has_responded: false
                    },
                    result = scope.isAttending(adventure);

                expect(result).to.be.false();
            });
        });

        describe('isNotAttending(adventure)', function() {
            it('returns true if adventure.has_responded and !is_attending', function() {
                var adventure = {
                        has_responded: true,
                        is_attending: false
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.true();
            });

            it('returns false if one condition is false', function() {
                var adventure = {
                        has_responded: false,
                        is_attending: false
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.false();
            });

            it('returns false if all conditions are false', function() {
                var adventure = {
                        has_responded: false,
                        is_attending: true
                    },
                    result = scope.isNotAttending(adventure);

                expect(result).to.be.false();
            });
        });

        describe('toCaps(word)', function() {

            it('word.replace()', function() {
                var word = 'This_is_a_test',
                    output = scope.toCaps(word);

                expect(output).to.eq('This is_a_test');
            });
        });

        describe('formatTime(time)', function() {

            it('format time', function() {
                var time = moment(),
                    output = scope.formatTime(time);

                expect(output).to.eq(time.format('MMM D [at] h:mm A'));
            });
        });
    });
})();
