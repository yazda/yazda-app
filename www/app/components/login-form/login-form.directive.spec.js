(function () {
    'use strict';

    describe('Directive: loginForm', function () {
        var vm,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.loginForm', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                'oauthClient'
            );

            bard.mockService(oauthClient, mocks.OauthClientMock($q));

            sinon.spy($rootScope, '$broadcast');
            sinon.spy(oauthClient, 'login');
        });

        beforeEach(function () {
            var html = angular.element('<login-form></login-form>');

            $rootScope = $rootScope.$new();

            $templateCache.put('app/components/login-form/login-form.html', '');

            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            vm = element.controller('loginForm');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: ', function() {
            expect(vm).to.be.ok();
        });

        describe('signIn()', function() {
            it('should login user, then broadcast message', function() {
                var user = {username: 'username', password: 'password'};

                vm.signIn(user);
                $rootScope.$apply();
                expect(oauthClient.login.called).to.be.true();
                expect($rootScope.$broadcast.calledWith('user.login')).to.be.true();
            });
        });
    });
})();
