(function() {
    'use strict';

    angular
        .module('yazda.components.loginForm')
        .directive('loginForm', loginForm);

    function loginForm() {
        return {
            templateUrl:      'app/components/login-form/login-form.html',
            restrict:         'EA',
            controller:       LoginCtrl,
            controllerAs:     'login',
            bindToController: true
        };
    }

    LoginCtrl.$inject = ['$ionicHistory', 'oauthClient', '$ionicPlatform'];

    function LoginCtrl($ionicHistory, oauth, $ionicPlatform) {
        var vm = this;

        $ionicPlatform.ready(function() {
            vm.signIn = signIn;
        });

        function signIn(user) {
            oauth
                .login(user.username, user.password)
                .then(function() {
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();

                    user.password = '';
                });
        }
    }
})();
