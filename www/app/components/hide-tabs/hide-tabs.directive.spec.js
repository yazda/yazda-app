(function () {
    'use strict';

    describe('Directive: hideTabs ', function () {

        var $scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.hideTabs', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$rootScope',
                '$controller'
            );
        });
        beforeEach(function () {
            var html = angular.element('<div hide-tabs></div>');

            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);
        });

        bard.verifyNoOutstandingHttpRequests();

        describe('For hideTabs()', function() {
            it('sets hideTabs to true on $rootScope', function() {
                expect($rootScope.hideTabs).to.be.true();
            });

            it('sets hideTabs to false $on $destroy', function() {
                $rootScope.$broadcast('$destroy');
                $rootScope.$apply();

                expect($rootScope.hideTabs).to.be.false();
            });
        });
    });
})();
