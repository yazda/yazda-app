(function() {
    'use strict';

    angular
        .module('yazda.components.hideTabs')
        .directive('hideTabs', hideTabs);

    hideTabs.$inject = ['$rootScope'];
    function hideTabs($rootScope) {
        return {
            link:     link,
            restrict: 'A'
        };

        function link(scope, element, attrs) {
            $rootScope.hideTabs = true;

            scope.$on('$destroy', function() {
                $rootScope.hideTabs = false;
            });
        }
    }
})();
