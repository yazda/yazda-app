(function() {
    'use strict';

    angular
        .module('yazda.components.adventureType')
        .directive('adventureType', adventureType);

    function adventureType() {
        return {
            restrict: 'E',
            scope:    {
                type: '='
            },
            templateUrl: 'app/components/adventure-type/adventure-type.html'
        };
    }
})();
