(function () {
    'use strict';

    describe('Directive: adventure-type', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.adventureType');
            bard.inject(
                '$compile',
                '$rootScope',
                '$controller',
                '$templateCache'
            );
        });

        beforeEach(function () {
            var html = angular.element('<adventure-type type="MockType"></adventure-type>');

            $templateCache.put('app/components/adventure-type/adventure-type.html', '');
            $rootScope = $rootScope.$new();
            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            scope = element.isolateScope();
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should: ', function() {
            expect(element).to.be.ok();
        });
    });
})();
