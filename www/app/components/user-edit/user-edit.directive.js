(function() {
    'use strict';

    angular
        .module('yazda.components.userEdit')
        .directive('userEdit', user);

    function user() {
        return {
            restrict:         'EA',
            templateUrl:      'app/components/user-edit/user-edit.html',
            controller:       AccountController,
            controllerAs:     'user',
            bindToController: true
        };
    }

    AccountController.$inject = ['account', '$ionicHistory', '$cordovaActionSheet', '$cordovaCamera','$ionicPlatform'];

    function AccountController(account, $ionicHistory, $cordovaActionSheet, $cordovaCamera,$ionicPlatform) {
        var vm = this;

        vm.getMe = getMe;
        vm.updateSettings = updateSettings;
        vm.chooseWayToUploadPhoto = chooseWayToUploadPhoto;
        vm.disableTap = disableTap;
        vm.background = background;

        $ionicPlatform.ready(activate);

        function background() {
            if(vm.data && vm.data.banner_image_thumb_url) {
                return 'url( ' + vm.data.banner_image_thumb_url + ')';
            }
        }

        function chooseWayToUploadPhoto(width, height, type) {
            var config = {
                quality:            100,
                destinationType:    Camera.DestinationType.DATA_URL,
                allowEdit:          true,
                encodingType:       Camera.EncodingType.PNG,
                targetWidth:        Number(width),
                targetHeight:       Number(height),
                popoverOptions:     CameraPopoverOptions,
                saveToPhotoAlbum:   true,
                correctOrientation: true
            };

            $cordovaActionSheet.show({
                    title:                     'How do you want to pick your picture',
                    buttonLabels:              ['Camera', 'Photo Library'],
                    addCancelButtonWithLabel:  'Cancel',
                    androidEnableCancelButton: true
                })
                .then(function(index) {
                    if(index === 1) {
                        config.sourceType = Camera.PictureSourceType.CAMERA;
                    } else if(index === 2) {
                        config.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
                    }

                    if(index !== 3) {
                        $cordovaCamera.getPicture(config)
                            .then(function(data) {
                                if(type === 'avatar') {
                                    account.updateProfileImage(data)
                                        .then(function() {
                                            getMe();
                                        });
                                } else if(type === 'banner') {
                                    account.updateBannerImage(data)
                                        .then(function() {
                                            getMe();
                                        });
                                }

                            });
                    }
                });
        }

        function activate() {
            return getMe();
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');

            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');

            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('places').blur();
            });
        }

        function updateSettings() {
            return account.updateSettings(vm.data)
                .then(function() {
                    $ionicHistory.clearCache().then(function() {
                        $ionicHistory.goBack();
                    });
                });
        }

        function getMe() {
            return account.me(true)
                .then(setUserData);
        }

        function setUserData(data) {
            var user = data.user;

            vm.data = user;

            return vm.data;
        }
    }
})();
