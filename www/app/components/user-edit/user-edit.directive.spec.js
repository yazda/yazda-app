(function () {
    'use strict';

    describe('Directive: userEdit', function () {
        var vm,
            element,
            _Camera = {
                DestinationType: {
                    DATA_URL: 'TEST_URL'
                }
            };

        beforeEach(function () {
            bard.appModule('yazda.components.userEdit', 'ngCordovaMocks', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){});
                $provide.value('Camera', _Camera);
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                '$ionicHistory',
                '$cordovaActionSheet',
                '$cordovaCamera',
                'account'
            );

            bard.mockService($ionicHistory, mocks.IonicHistoryMock($q));
            bard.mockService(account, mocks.AccountMock($q));

            bard.mockService($cordovaActionSheet, mocks.CordovaActionSheetMock($q));
            bard.mockService($cordovaCamera, mocks.CordovaCameraMock($q));

        });

        beforeEach(function () {
            var html = angular.element('<user-edit></user-edit>');

            $templateCache.put('app/components/user-edit/user-edit.html', '');

            $rootScope = $rootScope.$new();

            element = $compile(html)($rootScope);

            $rootScope.$digest(element);

            vm = element.controller('userEdit');

            $rootScope.$apply();
        });

        beforeEach(function() {
            sinon.spy(account, 'me');
            sinon.spy(account, 'updateSettings');
            sinon.spy($ionicHistory, 'goBack');
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open userEdit', function () {
            expect(vm).to.be.ok();
            expect(element).to.be.ok();
        });

        //it('Should call activate', function() {
        //    vm = element.controller('userEdit');
        //    //$rootScope.$apply();
        //    expect(account.me.called).to.be.true();
        //});

        describe('Function chooseWayToUploadPhoto', function() {

            it('Should call $cordovaActionSheet.show', function() {
                vm.chooseWayToUploadPhoto();

                $rootScope.$apply();

                expect('')
            });
        });

        describe('Function getMe', function() {
            beforeEach(function() {
                vm.getMe();
                $rootScope.$apply();
            });
            it('Should call account.me', function() {
                expect(account.me.called).to.be.true();
            });
            describe('Function setUserData', function() {
                it('Should set vm.data', function() {
                    expect(vm.data).to.eql({
                        id: 'TestID'
                    });
                });
            });
        });

        describe('Function updateSettings', function() {
            beforeEach(function() {
                vm.updateSettings();
                $rootScope.$apply();
            });
            it('Should call account.updateSettings', function() {
                expect(account.updateSettings.calledWith({
                    id: 'TestID'
                })).to.be.true();
            });
            it('Should call $ionicHistory.goBack', function() {
                expect($ionicHistory.goBack.called).to.be.true();
            });
        });
    });
})();
