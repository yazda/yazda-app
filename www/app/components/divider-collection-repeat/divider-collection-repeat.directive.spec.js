(function () {
    'use strict';

    describe('Directive: dividerCollectionRepeat', function () {
        var scope,
            element;

        beforeEach(function () {
            bard.appModule('yazda.components.dividerCollectionRepeat', 'yazda.yazda-api', function($provide, $urlRouterProvider){
                $provide.value('$ionicTemplateCache', function(){} );
                $urlRouterProvider.deferIntercept();
            });
            bard.inject(
                '$q',
                '$compile',
                '$parse',
                '$rootScope',
                '$templateCache',
                'invites'
            );

            bard.mockService(invites, mocks.InvitesMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<div divider-collection-repeat itemHeight="100"></div>');

            scope = $rootScope.$new();

            element = $compile(html)($rootScope);

            $rootScope.$digest(element);
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open divider-collection-repeat', function(){
            expect(element).to.be.ok();
        });
    });

})();
