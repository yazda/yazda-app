(function() {
    angular.module('yazda.components.dividerCollectionRepeat')
        .directive('dividerCollectionRepeat', DividerCollectionRepeat);

    DividerCollectionRepeat.$inject = ['$parse'];

    function DividerCollectionRepeat($parse) {
        return {
            restrict: 'A',
            priority: 1001,
            compile:  compile
        };

        function compile(element, attr) {
            var item = attr.collectionRepeat.substr(0, attr.collectionRepeat.indexOf(' '));
            var height = attr.itemHeight || '188';

            attr.$set('itemHeight', item + '.isDivider ? 37 : ' + height);

            element.children().attr('ng-hide', item + '.isDivider');
            element.prepend(
                '<div class="item item-divider ng-hide" ng-show="'+item+'.isDivider" ng-bind="'+item+'.divider"></div>'
            );
        }
    }

})();
