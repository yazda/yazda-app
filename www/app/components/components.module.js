(function() {
    'use strict';

    angular.module('yazda.components', [
        'yazda.components.adventureEdit',
        'yazda.components.adventureListItem',
        'yazda.components.adventureRespondTo',
        'yazda.components.adventureShow',
        'yazda.components.adventureType',
        'yazda.components.backButton',
        'yazda.components.biClick',
        'yazda.components.clubCreate',
        'yazda.components.clubEdit',
        'yazda.components.clubListItem',
        'yazda.components.clubShow',
        'yazda.components.dividerCollectionRepeat',
        'yazda.components.friendRequestItem',
        'yazda.components.hideTabs',
        'yazda.components.inviteItem',
        'yazda.components.loginForm',
        'yazda.components.profileImage',
        'yazda.components.registerForm',
        'yazda.components.resetPassword',
        'yazda.components.resetPasswordWithToken',
        'yazda.components.socialLogin',
        'yazda.components.userCollection',
        'yazda.components.userItem',
        'yazda.components.userShow',
        'yazda.components.userEdit',
        'yazda.components.usersListModal'
    ]);
})();



