(function() {
    'use strict';

    angular
        .module('yazda.components.socialLogin')
        .directive('socialLogin', ['oauthClient', 'oauthConstants', '$cordovaOauth', '$ionicPlatform', socialLogin]);

    function socialLogin(oauthClient, oauthConstants, $cordovaOauth, $ionicPlatform) {
        var directive = {
            replace:     true,
            link:        link,
            templateUrl: 'app/components/social-login/social-login.html',
            restrict:    'EA'
        };

        return directive;

        function link(scope, element, attrs) {
            $ionicPlatform.ready(function() {
                scope.facebook = facebook;
                scope.strava = strava;
            });

            function facebook() {
                $cordovaOauth
                    .facebook(oauthConstants.facebookAppId, ['email', 'public_profile', 'user_friends'])
                    .then(function(result) {
                        oauthClient
                            .loginWithFacebook(result, attrs.broadcast);
                    });
            }

            function strava() {
                $cordovaOauth
                    .strava(oauthConstants.stravaClientId, oauthConstants.stravaClientSecret, ['public'])
                    .then(function(result) {
                        oauthClient
                            .loginWithStrava(result, attrs.broadcast);
                    });
            }
        }
    }
})();
