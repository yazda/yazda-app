(function() {
    'use strict';

    angular.module('yazda.components.socialLogin', [
        'ngCordova',
        'yazda.yazda-api'
    ]);

})();
