(function () {
    'use strict';

    //TODO: figure out how to test $scope.externalScope
    describe.skip('Directive: usersListModal', function () {
        var element,
            $scope;

        beforeEach(function () {
            bard.appModule('yazda.components.usersListModal', 'yazda.yazda-api', function($provide, $urlRouterProvider) {
                $provide.value('$ionicTemplateCache', function() {
                });
                $urlRouterProvider.deferIntercept();
            });

            bard.inject(
                '$compile',
                '$q',
                '$rootScope',
                '$controller',
                '$templateCache',
                '$ionicModal',
                'users',
                'friends'
            );

            bard.mockService($ionicModal, mocks.IonicModalMock($q));
            bard.mockService(users, mocks.UsersMock($q));
            bard.mockService(friends, mocks.FriendsMock($q));
        });

        beforeEach(function () {
            var html = angular.element('<users-list-modal></users-list-modal>');

            $scope = $rootScope.$new();

            element = $compile(html)($scope);

            $scope.$digest(element);
        });

        bard.verifyNoOutstandingHttpRequests();

        it('Should open users-list', function() {
            console.log($scope.externalScope);
        })
    });
})();
