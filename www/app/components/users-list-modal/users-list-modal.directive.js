(function() {
    'use strict';

    angular
        .module('yazda.components.usersListModal')
        .directive('usersListModal', usersList);

    usersList.$inject = ['$ionicModal', 'users', 'friends','$ionicPlatform'];
    function usersList($ionicModal, users, friends,$ionicPlatform) {
        return {
            restrict:    'E',
            scope:       {
                externalScope: "="
            },
            replace:     true,
            transclude:  true,
            templateUrl: 'app/components/chat/chat-search.html',
            link:        link
        };

        function link($scope, $element, $attrs) {
            $scope.externalScope.currentPage = 0;
            $scope.externalScope.openModal = openModal;
            $scope.externalScope.closeList = closeList;
            $scope.externalScope.getUsers = getUsers;
            $scope.externalScope.addToUsers = addToUsers;
            $scope.externalScope.refresh = refresh;
            $scope.externalScope.moreData = moreData;
            $scope.externalScope.loadNextPage = loadNextPage;
            $scope.externalScope.checked = checked;
            $scope.externalScope.users = [];
            $scope.externalScope.query = {text: ''};

            var currentPage = 0;
            var page_count = 0;

            $ionicPlatform.ready(activate);
            //TODO piecemeal so it works for otherthings besides just selecting users

            function refresh() {
                currentPage = 0;
                $scope.externalScope.users = [];

                getUsers();
            }

            function activate() {
                getUsers();
                $scope.$watch(function() {
                    return $scope.externalScope.query.text;
                }, function(value) {
                    $scope.externalScope.users = [];
                    getUsers();
                });
            }

            function getUsers() {
                var id = window.localStorage.getItem('current_user_id');

                return friends
                    .index(id,
                        {
                            page: currentPage,
                            q:    $scope.externalScope.query.text
                        })
                    .then(function(response) {
                        page_count = response.page_count;
                        $scope.externalScope.users = response.users;
                    })
                    .finally(function() {
                        $scope
                            .$broadcast('scroll.refreshComplete');
                        $scope
                            .$broadcast('scroll.infiniteScrollComplete');
                    });
            }

            function moreData() {
                return currentPage < page_count;
            }

            function loadNextPage() {
                currentPage++;
                getUsers();
            }

            function checked(user) {
                if(!user){
                    return false;
                }

                var index = _.findIndex($scope.externalScope.sendTo, {id: user.id});

                return index !== -1;
            }

            function addToUsers(user) {
                var index = _.findIndex($scope
                        .externalScope
                        .sendTo,
                    {id: user.id});

                if(index === -1) {
                    $scope
                        .externalScope
                        .sendTo
                        .push(user);
                } else {
                    _.pullAt(
                        $scope
                            .externalScope
                            .sendTo,
                        index
                    );
                }
            }

            function openModal() {
                $scope.modal.show();
            }

            function closeList() {
                $scope.modal.hide();
            }

            $ionicModal.fromTemplateUrl('app/components/users-list-modal/users-list-modal.html', {
                scope:     $scope.externalScope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });
        }
    }
})();
