(function() {
    'use strict';

    angular
        .module('yazda.components.clubCreate')
        .directive('clubCreate', clubCreate);

    function clubCreate() {
        return {
            templateUrl:      'app/components/club-create/club-create.html',
            restrict:         'EA',
            controller:       ClubCreateController,
            controllerAs:     'ctrl',
            bindToController: true
        };
    }

    ClubCreateController.$inject = ['clubs', '$ionicHistory', 'account', '$state'];

    function ClubCreateController(clubs, $ionicHistory, account, $state) {
        var vm = this;

        vm.disableTap = disableTap;
        vm.createClub = createClub;

        function createClub() {
            return clubs
                .create(vm.club)
                .then(function(club) {
                    vm.club = {};

                    return account
                        .me(true)
                        .finally(function() {
                            $ionicHistory.nextViewOptions({
                                historyRoot: true,
                                disableBack: true
                            });

                            $ionicHistory
                                .clearCache()
                                .then(function() {
                                    $state.go('loggedIn.clubs-index')
                                        .then(function() {
                                            $state.go('loggedIn.club-clubs-show', {id: club.club.id});
                                        });
                                });
                        });

                });
        }

        function disableTap() {
            var container = document.getElementsByClassName('pac-container');
            // disable ionic data tab
            angular.element(container).attr('data-tap-disabled', 'true');
            // leave input field if google-address-entry is selected
            angular.element(container).on("click", function() {
                document.getElementById('club-places').blur();
            });
        }
    }
})();
