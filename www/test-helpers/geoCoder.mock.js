var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.GeoCoderMock = function($q) {
        return {
            geocode: geocode
        };

        function geocode(address) {
            return $q.when([
                    {
                        geometry: {
                            location: {
                                lat: function() {
                                    return 100;
                                },
                                lng: function() {
                                    return 200;
                                }
                            },
                            viewport: 300
                        }

                    }
                ]);
        }
    }
})(mocks);
