var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.IonicModalMock = function($q){
        return {
            index: getIndex
        };

        function getIndex(id) {
            return $q.when({
                function: 'getIndex',
                params: id
            })
        }


    }
})(mocks);
