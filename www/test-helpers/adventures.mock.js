var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.AdventuresMock = function($q){
        var localData = [
            {id: 1, name: 'adv 1'},
            {id: 2, name: 'adv 2'},
            {id: 3, name: 'adv 3'}
        ];

        return {
            index:   getAdventures,
            create:  createAdventure,
            show:    showAdventure,
            update:  updateAdventure,
            destroy: destroyAdventure,
            data: localData
        };

        function getAdventures(options) {
            return $q.when({
                adventures: localData,
                page_count: 4
            });

        }

        function createAdventure(obj) {
            return $q.when(
                {
                    function: 'createAdventure',
                    params: obj
                });
        }

        function showAdventure(id) {
            return $q.when({
                function: 'showAdventure',
                params: id
            });

        }

        function updateAdventure(obj) {
            return $q.when({
                function: 'updateAdventure',
                params: obj
            });

        }

        function destroyAdventure(id) {
            return $q.when({
                function: '',
                params: id
            });

        }
    }
})(mocks);
