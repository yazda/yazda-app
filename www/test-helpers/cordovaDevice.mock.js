var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.CordovaDeviceMock = function($q) {

    return {
      getModel: getModel
    };

    function getModel() {
      return {
        modalName: 'testModal'
      };
    }
  }
})(mocks);
