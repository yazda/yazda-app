var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.OauthMock = function($q) {

        return {
            login: login
        };

        function login() {
            return $q.when({});
        }
    };
})(mocks);
