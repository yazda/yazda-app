var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.DevicesMock = function($q){
    return {
      create: createDevice
    };

    function createDevice(obj) {
      return $q.when({
        function: 'createDevice',
        params: obj
      })
    }
  }
})(mocks);
