var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.CordovaBadgeMock = function($q) {

    return {
      hasPermission: hasPermission,
      increase: increase,
      get: get
    };

    function hasPermission() {
      return $q.when(true);
    }
    function increase() {
      return $q.when({status: 200});
    }
    function get() {
      return $q.when('1000');
    }
  };
})(mocks);
