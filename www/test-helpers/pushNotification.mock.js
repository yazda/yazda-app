var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.PushNotificationMock = function($q) {
    return {
      init: init
    };

    function init(config) {
      return {
        on: function(event, cb) {
          switch(event){
            case 'notification':
              return cb({
                additionalData: {
                  notification_type: event
                }
              })
              break;
            case 'registration':
              return cb({
                registrationId: 'TestId'
              })
              break;
          }
        }
      }
    }
  };
})(mocks);
