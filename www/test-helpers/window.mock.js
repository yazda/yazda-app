var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.WindowMock = function($q){
        return {
            open: open
        };

        function open() {
            return {}
        }

    }
})(mocks);
