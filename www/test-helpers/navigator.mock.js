var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.NavigatorMock = function($q){
        return {
            startApp: startApp
        };

        function startApp() {
            return {
                function: 'startApp',
                check: check,
                start: start
            }
        }

    }
})(mocks);
