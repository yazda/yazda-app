var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.IonicPlatformMock = function() {

        return {
            ready: ready
        };

        function ready(cb) {
            if(typeof cb === 'function') {
                return cb();
            } else {
                return {
                    status: 200
                }
            }
        }
    }
})(mocks);
