var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.IonicPopupMock = function($q) {
        var localData = {};
        return {
            show: show,
            alert: alert,
            read: read
        };

        function read(item) {
            return localData[item];
        }

        function show(item) {
            localData['showPopUp'] = item;
            return $q.when({status:200});
        }

        function alert(alert) {
            localData['ionicPopupAlert'] = alert;
            return $q.when({status:200});
        }
    }
})(mocks);

