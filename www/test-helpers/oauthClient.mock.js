var mocks = mocks || {};

(function(mocks) {
    'use strict';

    var loggedIn = true,
        resultStatus;

    mocks.OauthClientMock = function($q) {

        resultStatus = 200;

        return {
            changeForgottenPassword: changeForgottenPassword,
            isLoggedIn: isLoggedIn,
            setLoggedIn: setLoggedIn,
            login: login,
            getSettings: getSettings,
            post: post,
            patch: patch,
            get: get,
            destroy: destroy
        };

        function destroy(url, obj) {
            return $q.when({
                status: resultStatus
            })
        }

        function post(url, obj) {
            return $q.when({
                status: resultStatus
            });
        };

        function patch(url, obj) {
            return $q.when({
                status: resultStatus
            });
        };

        function get(url, obj) {
            return $q.when({
                status: resultStatus
            });
        };

        function getSettings() {
            return $q.when({
                status: 200
            })
        }

        function changeForgottenPassword() {
            return $q.when ({});
        }

        function isLoggedIn() {
            return loggedIn;
        }

        function login() {
            return $q.when({});
        }

        function setLoggedIn(status) {
            loggedIn = status;
        }

        function signIn(user) {
            return $q.when({});
        }

    };
})(mocks);
