var mocks = mocks || {};

(function (mocks) {
    'use strict';

    mocks.Base = function ($q) {
        var timesThroughLoop = 0;
        var error_code = 'NO_MORE_USERS';
        var error = 'ERROR';
        var failError = {
            status: 400,
            result: {
                status: 400
            },
            data: {
                TEST: error,
            },
            error_code: error_code
        };

        return {
            failure: function () {
                console.log('!!!!! BASE MOCK FAIL !!!!!');
                var deferred = $q.defer();
                deferred.reject({
                    status: 400,
                    result: {
                        status: 400
                    },
                    data: {
                        TEST: error
                    },
                    error_code: error_code,
                    statusText: 'test error status'
                });
                return deferred.promise;
            },
            loop: function () {
                var deferred = $q.defer();

                timesThroughLoop += 1;
                if (timesThroughLoop) {
                    deferred.resolve();
                }
                return deferred.promise;
            },
            resetLoop: function () {
                timesThroughLoop = 0;
            },

            setErrorCode: function (err) {
                error_code = err;
            }
        };
    };
})(mocks);
