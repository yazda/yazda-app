var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.UsersMock = function($q) {
        return {
            show: showUser,
            search: searchUsers
        };

        function showUser(id) {
            return $q.when({
                user: {
                    id: '123'
                }
            });
        }

        function searchUsers(options) {
            return $q.when({
                function: 'searchUsers',
                params: options
            })
        }
    }
})(mocks);
