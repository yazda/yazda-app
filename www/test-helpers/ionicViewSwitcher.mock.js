var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.IonicViewSwitcherMock = function($q) {
        var nextDirection = '';
        return {
            nextDirection: nextDirection
        };

        function nextDirection(direction) {
            nextDirection = direction;
            return $q.when({status: 200});
        }
    }
})(mocks);

