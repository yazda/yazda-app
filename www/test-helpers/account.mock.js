var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.AccountMock = function($q){
        return {
            getSettings: getSettings,
            updateSettings: updateSettings,
            me: me,
            updateProfileImage: updateProfileImage
        };

        function me(oauth) {
            return $q.when({
                user: {
                    id: 'TestID'
                }
            });

    }
        function getSettings() {
            return $q.when({function: 'getSettings'});

        }

        function updateSettings(obj){
            return $q.when({
                function: 'updateSettings',
                params: obj
            });
        }

        function updateProfileImage(obj) {
            return $q.when({
                function: 'updateProfileImage',
                params: obj
            });
        }
    }
})(mocks);
