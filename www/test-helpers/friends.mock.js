var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.FriendsMock = function($q){
        return {
            index: getIndex,
            id: 1
        };
        function getIndex(id) {

            return $q.when({
                function: 'getIndex',
                index: {
                    id: 7
                },
                users: ['TestUser']
            })
        }

        function getFriends() {
            return $q.when({
                index: 'getIndex',
                params: id
            })
        }

        function moreData() {
            return {
                page_count: 2
            }
        }

        function setFriendData(data) {
            return {
                data: {
                    users: 'person',
                    page_count: 3
                }
            }
        }

    }
})(mocks);
