var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.FollowersMock = function($q){
    return {
      index: getIndex
    };

    function getIndex(id) {
      return $q.when({
        users: ['TEST_USER_01', 'TEST_USER_02']
      })
    }


  }
})(mocks);
