var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.CordovaCameraMock = function() {
        return {
            getPicture: getPicture
        };

        function getPicture() {
        }
    };
})(mocks);
