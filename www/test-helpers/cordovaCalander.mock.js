var mocks = mocks || {};

(function(mocks) {
  'use strict';

  mocks.CordovaCalendarMock = function($q) {
      return {
          createEventInteractively: createEventInteractively
      };

      function createEventInteractively() {
          return $q.when({});
      }
  };
})(mocks);
