var mocks = mocks || {};

(function(mocks) {
  'use strict';

  var currentState = '';
  var currentParams = {};

  mocks.StateMock = function($q) {
    return {
      current: current,
      go: go,
      href: href,
      get: get,
      is: is,
      reload: reload,
      transitionTo: transitionTo,
      lastParams: lastParams
    };

    function get(history) {
      return {
        url: 'Mock/Url'
      };
    }

    function transitionTo(newState) {
      currentState = newState;
      return $q.when({});
    }

    function is(query) {
        if(query) {
            return query === currentState;
        } else {
            return currentState;
        }

    }

    function go(newState, params) {
      currentState = newState;
      currentParams = params;
      return $q.when({});
    }

    function reload() {
      return {};
    }

    function current() {
      return currentState;
    }

    function href() {
      return 'hrefMock';
    }

    function lastParams() {
      return currentParams;
    }
  };
})(mocks);
