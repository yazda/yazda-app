var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.CordovaGeoLocationMock = function($q) {

        return {
            getCurrentPosition: getCurrentPosition
        };

        function getCurrentPosition() {
            return $q.when({
                coords: {
                    latitude: 100,
                    longitude: 200
                }
            });
        }
    }
})(mocks);
