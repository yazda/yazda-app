var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.AdventureItemMock = function($q) {

        return {
        };

        function getData() {
            return {
                moment: '2:00 A',
                start: '0:00',
                end: '5:00',
                time: '30:00'
            }
        };
    };
})(mocks);
