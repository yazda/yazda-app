var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.InvitesMock = function($q){
        var localData = [
            {id: 1, name: 'inv 1'},
            {id: 2, name: 'inv 2'},
            {id: 3, name: 'inv 3'}
        ];

        return {
            pending: getPending,
            accept:  accept,
            reject:  reject,
            data: localData
        };

        function getPending() {
            return $q.when({
                invites: localData,
                page_count: 5
            })
        }

        function accept() {
            return $q.when({});
        }

        function reject() {
            return $q.when({});

        }
    }
})(mocks);
