/*globals extendNS, streetSweepAlert*/
/* jshint -W117 */
var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.StateParamsMock = function() {
        var param = 0;
        return {
            id: id,
            setId: setId,
            reset_password_token: resetPassword
        };

        function id() {
            return param;
        }

        function setId(id) {
            param = id;
        }

        function resetPassword() {
            return 'Token';
        }
    };
})(mocks);
