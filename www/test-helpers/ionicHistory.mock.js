var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.IonicHistoryMock = function() {
        var self = this,
            currentState;

        return {
            goBack: goBack,
            currentStateName: currentStateName,
            backView: backView
        };

        function backView() {

        }

        function goBack() {
        }

        function currentStateName(state) {
            if(state) {
                currentState = state;
            } else {
                return currentState;
            }
        }
    }
})(mocks);

