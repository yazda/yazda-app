var mocks = mocks || {};

(function(mocks) {
    'use strict';

    mocks.FriendShipsMock = function($q){
        return {
            incoming: getIncoming,
            outgoing: getOutgoing,
            create:   createFriendship,
            accept:   acceptFriendship,
            reject:   rejectFriendship
        };

        function getIncoming(page) {
            return $q.when({
                friendships: [
                    {user: 'one'},
                    {user: 'two'},
                    {user: 'three'}
                ],
                page_count: page.page
            })
        }

        function getOutgoing(obj) {
            return $q.when({
                function: 'getOutgoing',
                params: obj
            });
        }

        function createFriendship(userId) {
            return $q.when({
                function: 'createFriendship',
                params: userId
            });
        }

        function acceptFriendship(id) {
            return $q.when({
                status: 200
            });
        }

        function rejectFriendship(id) {
            return $q.when({
                status: 200
            });
        }
    }
})(mocks);
