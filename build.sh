#!/usr/bin/env bash

# increment version numbers
perl -i -pe 's/\bandroid-versionCode="[^"]*\K\b(\d+)(?=[^"]*")/$1+1/e' config.xml
perl -i -pe 's/\bios-CFBundleVersion="[^"]*\K\b(\d+)(?=[^"]*")/$1+1/e' config.xml

# use prod keys
perl -i -pe 's/\bkey_[^"]*\b(?=[^"]*")/key_live_fomHN5t1mbHUXc62i5mfDngisCmk8Tnt/e' config.xml
ionic plugin rm io.branch.sdk
ionic plugin add https://github.com/BranchMetrics/Cordova-Ionic-PhoneGap-Deferred-Deep-Linking-SDK.git --variable BRANCH_KEY=key_live_fomHN5t1mbHUXc62i5mfDngisCmk8Tnt --variable URI_SCHEME=yazda

cordova plugin rm org.apache.cordova.console

# Setup for android

gulp replace --env production-android
gulp js

ionic build android --release

# Setup for iOS

gulp replace --env production-ios
gulp js

ionic build --release ios

pushd platforms/ios

xcodebuild -scheme "Yazda" -configuration Release clean archive

popd

gulp replace

# use test keys
perl -i -pe 's/\bkey_[^"]*\b(?=[^"]*")/key_test_bmlIK0C3ahGT5n7Xm5jpypkbwAacXSma/e' config.xml
ionic plugin rm io.branch.sdk
ionic plugin add https://github.com/BranchMetrics/Cordova-Ionic-PhoneGap-Deferred-Deep-Linking-SDK.git --variable BRANCH_KEY=key_test_bmlIK0C3ahGT5n7Xm5jpypkbwAacXSma --variable URI_SCHEME=yazda

